# Find Sphinx

include(FindPackageHandleStandardArgs)

macro(_find_sphinx)
  find_program(
    Sphinx_EXECUTABLE
    NAMES sphinx-build
    PATHS ${Sphinx_DIR}
          $ENV{HOME}/.local/bin
          /usr/bin
          /bin
    DOC "Path to sphinx-build"
  )

  if(NOT Sphinx_EXECUTABLE)
    set(_FAILED
        "Unable to find 'sphinx-build' or an extension. Try setting the 'Sphinx_DIR' environment variable"
    )
    return()
  endif()

  add_executable(Sphinx::Build IMPORTED)
  set_property(
    TARGET Sphinx::Build
    PROPERTY IMPORTED_LOCATION "${_Sphinx_EXECUTABLE}"
  )
endmacro()

macro(_find_breathe)
  find_program(
    Breathe_EXECUTABLE
    NAMES breathe-apidoc
    PATHS ${Breathe_DIR}
          $ENV{HOME}/.local/bin
          /usr/bin
          /bin
    DOC "Path to breathe-apidoc"
  )

  if(Breathe_EXECUTABLE)
    add_executable(Breathe::Apidoc IMPORTED)
    set_property(
      TARGET Breathe::Apidoc
      PROPERTY IMPORTED_LOCATION "${Breathe_EXECUTABLE}"
    )
    set(Sphinx_Breathe_FOUND
        TRUE
    )
  else()
    set(_FAILED "${_FAILED}, did not find 'breathe'")
  endif()
endmacro()

macro(_stop)
  find_package_handle_standard_args(
    Sphinx
    REQUIRED_VARS Sphinx_EXECUTABLE
    HANDLE_COMPONENTS REASON_FAILURE_MESSAGE ${_FAILED}
  )
  unset(Sphinx_EXECUTABLE)
endmacro()

# Ensure Python was found
if(NOT TARGET Python3::Interpreter)
  find_package(
    Python3
    COMPONENTS Interpreter
  )
  if(NOT TARGET Python3::Interpreter)
    set(_FAILED
        "Sphinx needs Python!"
    )
    _stop()
    return()
  endif()
endif()

# Find Sphinx
_find_sphinx()

# Find Components
if("Breathe" IN_LIST Sphinx_FIND_COMPONENTS)
  _find_breathe()
endif()

# Find sphinx-contrib
set(_SPHINX_CONTRIB
    ${Sphinx_FIND_COMPONENTS}
)
list(FILTER
     _SPHINX_CONTRIB
     EXCLUDE
     REGEX
     "Breathe"
)
foreach(
  _EXT IN
  LISTS _SPHINX_CONTRIB
)
  message(TRACE "Looking for sphinx component ${_EXT} with '${Python3_EXECUTABLE} -c \"import sphinxcontrib.${_EXT}\"'")
  execute_process(
    COMMAND ${Python3_EXECUTABLE} -c "import sphinxcontrib.${_EXT}"
    RESULT_VARIABLE _PROC_RESULT
    OUTPUT_QUIET ERROR_QUIET
  )

  if(NOT _PROC_RESULT)
    set(Sphinx_${_EXT}_FOUND
        TRUE
    )
  else()
    message(
      WARNING "Couldn't find the Sphinx component ${_EXT} (${_PROC_RESULT})"
    )
    set(_FAILED "${_FAILED}; did not find '${_EXT}'")
  endif()
endforeach()
unset(_EXT)
unset(_EXIT_CODE)
unset(_SPHINX_CONTRIB)

_stop()
