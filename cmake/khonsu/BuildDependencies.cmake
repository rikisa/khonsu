# Fetch all dependencies

# Build dependencies
find_package(PNG++)
set_package_properties(
  PNG++ PROPERTIES
  DESCRIPTION "C++ bindings for libpng"
  TYPE REQUIRED
)

find_package(Lua 5.4)
set_package_properties(
  Lua PROPERTIES
  DESCRIPTION "Lua script language"
  TYPE REQUIRED
)

find_package(sol2)
set_package_properties(
  sol2 PROPERTIES
  DESCRIPTION "C++ bindings for Lua"
  TYPE REQUIRED
)

find_package(glfw3)
set_package_properties(
  glfw3 PROPERTIES
  DESCRIPTION "OpenGL contexts, window, input and event handling"
  TYPE REQUIRED
)

find_package(fmt)
set_package_properties(
  fmt PROPERTIES
  DESCRIPTION "String formatting library"
  TYPE REQUIRED
)

find_package(spdlog)
set_package_properties(
  spdlog PROPERTIES
  DESCRIPTION "Runtime logging"
  TYPE REQUIRED
)

find_package(cxxopts)
set_package_properties(
  cxxopts PROPERTIES
  DESCRIPTION "Commandline parser"
  TYPE REQUIRED
)

find_package(Notcurses++)
set_package_properties(
  Notcurses++ PROPERTIES
  DESCRIPTION "Terminal TUI library - with bling"
  TYPE REQUIRED
)
