# Find coverage instrumentations and create targets

include(FindPackageHandleStandardArgs)

if(NOT (${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Linux"))
  message(FATAL_ERROR "Codecoverage is only supported on linux")
endif()

find_package(
  Python3
  COMPONENTS Interpreter
  QUIET
)
# needed for report generation
if(NOT Python3_FOUND)
  message(FATAL_ERROR "KHONSU_CODECOV is ON but python3 has not been found!")
endif()


if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")

  find_program(
    LLVM_COV_PATH llvm-cov
    NAMES llvm-cov-16
  )

  find_program(
    LLVM_PROFDATA_PATH llvm-profdata
    NAMES llvm-profdata-16
  )

  add_custom_target(
    CoverageReport
    COMMAND
      ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/util/create-cov-report.py
      "--cov" ${LLVM_COV_PATH} "--pdat" ${LLVM_PROFDATA_PATH} "--dir"
      "${CMAKE_BINARY_DIR}/covreport" "--prof" "${CMAKE_BINARY_DIR}"
      "--binmanifest" "${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES}" "--profpat" "\"testcov_.*.profraw\""
      "--ignore" "\"(test_|fake|mock).*\""
  )

  find_package_handle_standard_args(
    llvm-cov
    REQUIRED_VARS LLVM_PROFDATA_PATH LLVM_COV_PATH
  )
  mark_as_advanced(LLVM_COV_PATH)
  mark_as_advanced(LLVM_PROFDATA_PATH)

elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
  find_program(
    GCOV_PATH gcovr
    NAMES gcovr.py
  )
  if(NOT GCOV_PATH)
    message(FATAL_ERROR "KHONSU_CODECOV is ON but gcovr has not been found!")
  endif()

  add_custom_target(
    CoverageReport
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMAND ${CMAKE_COMMAND} -E make_directory
            ${CMAKE_BINARY_DIR}/covreport/html
    COMMAND ${GCOV_PATH} -r ${CMAKE_SOURCE_DIR} --exclude-directories CMakeFiles
            --filter ${CMAKE_SOURCE_DIR}/src --txt
    COMMAND
      ${GCOV_PATH} -r ${CMAKE_SOURCE_DIR} --exclude-directories CMakeFiles
      --filter ${CMAKE_SOURCE_DIR}/src --html-details --html-self-contained
      --output ${CMAKE_BINARY_DIR}/covreport/html/index.html
    COMMAND
      ${GCOV_PATH} -r ${CMAKE_SOURCE_DIR} --exclude-directories CMakeFiles
      --filter ${CMAKE_SOURCE_DIR}/src --cobertura --output
      ${CMAKE_BINARY_DIR}/covreport/coverage.xml
  )

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    gcovr
    REQUIRED_VARS GCOV_PATH
  )
mark_as_advanced(GCOV_PATH)
else()
  message(
    FATAL_ERROR
      "Only gcc and clang are supported for coverage reporting, but got ${CMAKE_CXX_COMPILER_ID} instead"
  )
endif()
