# Testing dependencies
find_package(Catch2 3)
set_package_properties(
  Catch2 PROPERTIES
  DESCRIPTION "Unit testing framework"
  TYPE OPTIONAL
)

find_package(trompeloeil)
set_package_properties(
  trompeloeil PROPERTIES
  DESCRIPTION "Mocking framework"
  TYPE OPTIONAL
)

find_package(Catch2 3)
set_package_properties(
  Catch2 PROPERTIES
  DESCRIPTION "Unit testing framework"
  TYPE OPTIONAL
)

# Docs dependencies
find_package(Doxygen)
set_package_properties(
  Doxygen PROPERTIES
  DESCRIPTION "Documentation generation"
  TYPE OPTIONAL
)

find_package(
  Sphinx
  COMPONENTS mermaid plantuml Breathe
)
set_package_properties(
  Sphinx PROPERTIES
  DESCRIPTION "Code and project documentation"
  TYPE OPTIONAL
)

# Versioning
find_package(Git)
set_package_properties(
  Git PROPERTIES
  DESCRIPTION "Distributed version control system"
  TYPE OPTIONAL
)
