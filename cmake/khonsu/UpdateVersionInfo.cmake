execute_process(
  COMMAND ${GIT_EXECUTABLE} describe --always --tags --long
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_STRIP_TRAILING_WHITESPACE
  OUTPUT_VARIABLE KHONSU_GIT_DESC
  RESULT_VARIABLE _result)

if(_result)
  set(KHONSU_GIT_DESC "dev")
  message(
    WARNING "Failed to invoke git exectuable to obtain revision information!")
endif()

string(REGEX MATCH "([0-9]+.[0-9]+-[0-9]+)-(.*)" DESC_MATCH_FULL
             "${KHONSU_GIT_DESC}")

if(DESC_MATCH_FULL)
  set(KHONSU_VERSION
      "${CMAKE_MATCH_1}")
  set(KHONSU_REVISION
      "${CMAKE_MATCH_2}")
else()
  set(KHONSU_VERSION
      "dev")
  set(KHONSU_REVISION
      "${KHONSU_GIT_DESC}")
endif()

if(EXISTS ${KHONSU_GIT_TAG_FILE})
  file(READ ${KHONSU_GIT_TAG_FILE} _PREF_REV)
  # if revision changed, touch input -> reconfigures -> outdate versioninfo
  if(NOT "${_PREF_REV}" STREQUAL "${KHONSU_GIT_DESC}")
    message(TRACE "Sha1 tag changed ${_PREF_REV} -> ${KHONSU_REVISION}")
    file(TOUCH ${KHONSU_VERINFO_INPUT})
    file(WRITE ${KHONSU_GIT_TAG_FILE}
         "${KHONSU_GIT_DESC}")
  else()
    message(TRACE "No changes in source control")
  endif()
else()
  message(TRACE "Creating sha1 tag <- ${KHONSU_REVISION}")
  file(WRITE ${KHONSU_GIT_TAG_FILE}
       "${KHONSU_REVISION}")
endif()

configure_file(${KHONSU_VERINFO_INPUT} ${KHONSU_VERINFO_OUTPUT} @ONLY)

set(KHONSU_VERSIONINFO_UPDATE_SCRIPT ${CMAKE_CURRENT_LIST_FILE})
