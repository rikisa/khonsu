# Convenience function to create libs and assorted aliases Prefixes with khonsu
# if not already done. creates an aliase
function(khonsu_add_lib _lib_name)

  set(options
      INTERFACELIB)
  set(oneValueArgs)
  set(multiValueArgs
      SOURCES PUBLIC PRIVATE INTERFACE INCLUDE)
  cmake_parse_arguments(_libadd "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})

  if(_libadd_INTERFACELIB)
    set(_lib_type
        INTERFACE)
  endif()

  message(DEBUG "Preparing lib with lib name: ${_lib_name}")

  # Remove any khonsu prefix
  string(
    REGEX
    REPLACE "^[Kk]honsu" "" _lib_name ${_lib_name})

  # Add uniformly
  set(_lib_full_name
      Khonsu${_lib_name})

  message(TRACE
          "Using full cmake target name: ${_lib_full_name} from '${_lib_name}'")

  add_library(
    ${_lib_full_name}
    ${_lib_type})

  if(NOT _libadd_INTERFACELIB)
    target_sources(
      ${_lib_full_name}
      PRIVATE ${_libadd_SOURCES})
  endif()

  if(_libadd_PUBLIC)
    target_link_libraries(
      ${_lib_full_name}
      PUBLIC ${_libadd_PUBLIC})
  endif()
  if(_libadd_PRIVATE)
    target_link_libraries(
      ${_lib_full_name}
      PRIVATE ${_libadd_PRIVATE})
  endif()
  if(_libadd_INTERFACE)
    target_link_libraries(
      ${_lib_full_name}
      INTERFACE ${_libadd_INTERFACE})
  endif()

  target_include_directories(
    ${_lib_full_name}
    PUBLIC ${_libadd_INCLUDE})

  add_library(Khonsu::${_lib_name} ALIAS ${_lib_full_name})

endfunction()

# Testing Utilies and Macros

set(KHONSU_TEST_BINARIES_IN
    "test_binaries.manifest.in")
set(KHONSU_TEST_BINARIES
    "test_binaries_$<CONFIG>.manifest")

# Force update disovered test for each configuration
file(REMOVE ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES_IN})
file(REMOVE ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES})

# Wrapper for smoke test generation
function(khonsu_add_smoketest _test_name _target_name _test_command _test_args)

  if(NOT TARGET ${_target_name})
    message(
      WARNING
        "Trying to add smoketest ${_test_name} for ${_target_name}, but it is not an proper target!"
    )
    return()
  endif()

  # For LLVM we need some book keeping and environment set
  set(_profraw_name
      "testcov_${_test_name}-%5m.profraw")

  get_property(
    IS_MULTI_CONF GLOBAL
    PROPERTY GENERATOR_IS_MULTI_CONFIG)
  if(IS_MULTI_CONF)
    message(TRACE "Adding test ${_test_name} -> ${_test_command} for all configurations")
    add_test(
      NAME "${_test_name}"
      COMMAND "${_test_command}" "${_test_args}"
      CONFIGURATIONS Debug Coverage Release RelWithDebInfo)
  else()
    message(TRACE "Adding test ${_test_name} -> ${_test_command}")
    add_test(
      NAME "${_test_name}"
      COMMAND "${_test_command}" "${_test_args}")
  endif()

  set_property(
    TEST "${_test_name}"
    PROPERTY ENVIRONMENT "LLVM_PROFILE_FILE=${_profraw_name}")

  file(READ "${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES_IN}" _manifest_content)

  string(FIND "${_manifest_content}" ${_target_name} _already_added)

  if(_already_added GREATER_EQUAL 0)
    return()
  endif()

  file(APPEND ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES_IN}
       "$<TARGET_FILE:${_target_name}>\n")

endfunction()

# Invoke test discovery of testing framework and set environment, mostly for
# coverage
function(khonsu_discover_tests _target_name)

  # For LLVM we need some book keeping and environment set
  set(_profraw_name
      "testcov_${_target_name}-%5m.profraw")

  catch_discover_tests(
    ${_target_name} PROPERTIES ENVIRONMENT
    "$<$<CXX_COMPILER_ID:Clang>:LLVM_PROFILE_FILE=${_profraw_name}>")

  file(APPEND ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES_IN}
       "$<TARGET_FILE:${_target_name}>\n")

endfunction()

# Create as manifest file for all tests discovered by khonsu_discover_tests
function(khonsu_create_test_manifest)

  if(EXISTS ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES})
    return()
  endif()

  file(GENERATE
       OUTPUT ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES}
       INPUT ${CMAKE_BINARY_DIR}/${KHONSU_TEST_BINARIES_IN})

endfunction()

# Convenience function to create tests
function(khonsu_add_test _test_name)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs
      SOURCES LIBRARIES)
  cmake_parse_arguments(_testadd "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})

  add_executable(${_test_name})
  target_sources(
    ${_test_name}
    PRIVATE ${_testadd_SOURCES})
  target_link_libraries(
    ${_test_name}
    PRIVATE Catch2::Catch2WithMain ${_testadd_LIBRARIES})
  khonsu_discover_tests(${_test_name})
endfunction()
