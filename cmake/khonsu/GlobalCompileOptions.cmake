set(is_clang "$<CXX_COMPILER_ID:Clang>")
set(is_gcc "$<CXX_COMPILER_ID:GNU>")
set(is_gcc_like "$<CXX_COMPILER_ID:GNU,Clang>")

set(is_cxx "$<COMPILE_LANGUAGE:CXX>")

set(with_cov_flags "$<CONFIG:Coverage>")

set(with_debug_flags "$<CONFIG:Coverage,Debug>")

set(with_release_flags "$<CONFIG:Release>")

set(with_rel_with_deb_flags "$<CONFIG:RelWithDebInfo>")

set(CMAKE_CXX_FLAGS_RELEASE
    ""
    CACHE STRING "Overwriting CMakes default flags" FORCE)

  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO
    ""
    CACHE STRING "Overwriting CMakes default flags" FORCE)

list(APPEND
     cxx_comp_flags
     # Debug/Release
     "$<$<AND:${is_gcc_like},${with_rel_with_deb_flags}>:-O2;-g;-Wall;-Wextra;-Wpedantic;-Werror>"
     "$<$<AND:${is_gcc_like},${with_release_flags}>:-O3;-DNDEBUG;-Wall;-Wextra;-Wpedantic;-Werror>"
     "$<$<AND:${is_gcc_like},${with_debug_flags}>:-O0;-Wall;-Wextra;-Wpedantic>"
     # Coverage
     "$<$<AND:${is_clang},${with_cov_flags}>:-fprofile-instr-generate;-fcoverage-mapping>"
     "$<$<AND:${is_gcc},${with_cov_flags}>:-fprofile-arcs;-ftest-coverage;-fprofile-abs-path>"
)

list(APPEND
     cxx_link_flags
     "$<$<AND:${is_clang},${with_cov_flags}>:-fprofile-instr-generate;-fcoverage-mapping>"
     "$<$<AND:${is_gcc},${with_cov_flags}>:-fprofile-arcs>")

add_compile_options("$<${is_cxx}:${cxx_comp_flags}>")
add_link_options("$<${is_cxx}:${cxx_link_flags}>")

if(NOT CMAKE_BUILD_TYPE)
  message(
    WARNING
      "No release type specified, assuming 'Debug'. To suppress the warning, reconfigure with:\n\t-DCMAKE_BUILD_TYPE={Debug|Release|Coverage}"
  )
  set(CMAKE_BUILD_TYPE Debug
      CACHE STRING "Supported build types: Debug Release Coverage." FORCE)
endif()

set(BUILD_SHARED_LIBS ${KHONSU_BUILD_SHARED_LIBS})
