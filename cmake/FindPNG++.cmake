include(CMakeFindDependencyMacro)
include(FindPackageHandleStandardArgs)

find_dependency(ZLIB)
set_package_properties(
  ZLIB PROPERTIES
  DESCRIPTION
    "Portable, general-purpose, lossless data-compression library for used by libpng"
  TYPE REQUIRED
)

if(ZLIB_FOUND)

  find_dependency(PNG)
  set_package_properties(
    PNG PROPERTIES
    DESCRIPTION
      "libpng, the official PNG reference library - supports almost all PNG features"
    TYPE REQUIRED
  )

  find_path(
    PNG++_INCLUDE_DIR
    png.hpp
    HINTS /usr/local/include/png++ /usr/include/png++
  )

  add_library(PNG++ INTERFACE)
  target_link_libraries(PNG++ INTERFACE PNG::PNG ZLIB::ZLIB)
  target_sources(PNG++ INTERFACE ${PNG++_INCLUDE_DIR}/png.hpp)
  target_include_directories(PNG++ INTERFACE ${PNG++_INCLUDE_DIR})

  add_library(PNG++::PNG++ ALIAS PNG++)

  set(PNG++_FOUND TRUE)

endif()

find_package_handle_standard_args(
  PNG++
  REQUIRED_VARS PNG++_FOUND ZLIB_FOUND PNG++_INCLUDE_DIR
)
mark_as_advanced(PNG++_INCLUDE_DIRS)
