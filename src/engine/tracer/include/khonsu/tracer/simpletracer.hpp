#pragma once

#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace tracer {

class SimpleTracer : public engine::part::BaseTracer {
  public:
    void use_logsink( engine::part::BaseLogRef logsink ) override;
    void use_eventqueue( engine::part::BaseEventQueueRef equeue ) override;

    // Exposed to controller
    void write_image( rendering::ImageRef image ) override;
    void set_camera( rendering::BaseCameraRef camera ) override;
    void trace_scene( rendering::SceneRef scene ) override;

    // factory functions
    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig const & ) -> ::engine::part::BaseTracerRef;

    struct ConfigOpts {
        static auto config_name() -> std::string { return { s_config_name.cbegin(), s_config_name.cend() }; }

      private:
        static std::string_view constexpr s_config_name = "tracer.simple";
    };

  private:
    rendering::BaseCameraRef _camera;
    rendering::ImageRef _image;
};

} // namespace tracer
