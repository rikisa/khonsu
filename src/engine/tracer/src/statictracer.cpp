#include <cstddef>
#include <memory>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/rendering/basecamera.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/rendering/scene.hpp"
#include "khonsu/tracer/simpletracer.hpp"

using linalg::FloatType;
using linalg::Intersection;
using linalg::Ray;
using linalg::Vector;
using material::BaseMaterial;
using material::Color;

namespace tracer {

void
SimpleTracer::use_logsink( engine::part::BaseLogRef logsink )
{
  (void)logsink;
}

void
SimpleTracer::use_eventqueue( engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

// factory functions
auto
SimpleTracer::create_config() -> ::engine::PartConfig
{
  return { SimpleTracer::ConfigOpts::config_name() };
}

void
SimpleTracer::write_image( rendering::ImageRef image )
{
  this->_image = image;
}

void
SimpleTracer::trace_scene( rendering::SceneRef scene )
{
  Ray ray{};
  Intersection intersect{};

  std::size_t constexpr max_ray_depth = 10;

  Color::elem_type const color_scale = ( 1.0F / static_cast<Color::elem_type>( this->_camera->get_rays_per_pixel() ) );

  // static Color node1{ { 144.0 / 255.0, 172.0 / 255.0, 249.0 / 255.0 } };
  // static Color node2{ { 0.80, 0.98, 0.71 } };
  static Color node1{ { 0.569, 0.787, 1.000 } };
  static Color node2{ { 0.749, 0.758, 0.951 } };
  // static Color node1{ { 0.000, 0.000, 1.000 } };
  // static Color node2{ { 1.000, 0.000, 0.000 } };

  Color cur_color{ 0 };
  Color cur_attenuation{ 1 };
  for ( std::size_t index = 0; index < this->_camera->size(); ++index ) {
    // Get initial ray
    this->_camera->update_ray( ray, index );

    // static auto reflect_step = this->_camera->cols() / 5;
    // switch (auto phase = ray.image_col() / reflect_step) {
    //   case 0:
    //     absorb = 0.1;
    //     break;
    //   case 1:
    //     absorb = 0.3;
    //     break;
    //   case 2:
    //     absorb = 0.5;
    //     break;
    //   case 3:
    //     absorb = 0.7;
    //     break;
    //   default:
    //     absorb = 0.9;
    // }

    // static auto const max_col = static_cast<Color::elem_type>(this->_camera->cols());
    // absorb = static_cast<Color::elem_type>(ray.image_col()) / max_col);

    cur_color.fill( 0 );
    cur_attenuation.fill( 1 );
    for ( std::size_t cur_ray_depth = 0; cur_ray_depth < max_ray_depth; ++cur_ray_depth ) {
      if ( scene->intersect( ray, intersect ) ) {
        // NOTE Maybe attach all properties directly to the Object, and then access them here from the object -> less
        // indirection
        intersect.with_material().scatter_ray( ray, intersect.at_point(), intersect.with_normal() );
        cur_attenuation *= intersect.with_material().get_color();

      } else {
        auto row = static_cast<float>( ray.dir()[ 0 ] );
        double upper_y = 0.2;
        double lower_y = -0.2;
        double dist = lower_y - upper_y;
        auto t = 1 - ( ( row - upper_y ) / dist );
        t = t < 0.0 ? 0.0 : ( t > 1.0 ? 1.0 : t );

        auto gradient = ( node1 * t + node2 * ( 1 - t ) );
        cur_color = Color{ std::move( gradient ) };
        break;
      }
    }
    this->_image->add_color( ray.image_row(), ray.image_col(), cur_color * cur_attenuation * color_scale );
  }
}

void
SimpleTracer::set_camera( rendering::BaseCameraRef camera )
{
  this->_camera = camera;
};

auto
SimpleTracer::create_part( ::engine::PartConfig const & config ) -> ::engine::part::BaseTracerRef
{
  (void)config;
  return std::make_shared<SimpleTracer>();
}

}; // namespace tracer
