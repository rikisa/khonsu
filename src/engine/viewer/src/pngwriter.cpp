#include "khonsu/viewer/pngwriter.hpp"

#include <cstdint>
#include <exception>
#include <ranges>

#include "khonsu/part/partconfig.hpp"

#include "rgb_pixel.hpp"
#include <png++/png.hpp>

namespace viewer {

PNGWriter::PNGWriter( FsPath filepath ) : _filepath( std::move( filepath ) )
{
  if ( this->_filepath.extension() != this->_file_extension ) {
    this->_filepath = this->_filepath.parent_path().string() / this->_filepath.filename();
    this->_filepath.replace_extension( this->_file_extension );
  }
};

void
PNGWriter::use_logsink( engine::part::BaseLogRef sink )
{
  (void)sink;
}

void
PNGWriter::use_eventqueue( engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

void
PNGWriter::read_image( rendering::ImageRef image )
{
  this->_image = image;
}

void
PNGWriter::show()
{
  if ( this->_image == nullptr ) {
    // TODO(rikisa): log warning here
    std::cerr << "No image!";
    return;
  }

  // TODO(rikisa): log warning here if returns false
  (void)PNGWriter::save_as_png( this->_filepath, this->_image );
}

void
PNGWriter::hide()
{
}

auto
PNGWriter::save_as_png( FsPath fpath, ImageRef img ) -> bool
{
  // TODO(rikisa) Select png image type based in ImageRef bitness (curretnly nothing has less than 16 bit)
  using png_pixel_type = png::rgb_pixel_16;
  using cast_type = std::uint16_t;

  png::image<png_pixel_type> png_image( img->width(), img->height() );

  static auto constexpr max_value = std::numeric_limits<cast_type>::max();
  std::size_t index = 0;

  for ( auto pixel : *img ) {
    auto casted = std::span( pixel.begin(), pixel.end() ) |
        std::views::transform( []( auto chan ) { return static_cast<cast_type>( chan * max_value ); } );
    auto cur_row = index / img->width();
    auto cur_col = index % img->width();
    png_image[ cur_row ][ cur_col ] = png_pixel_type( casted[ 0 ], casted[ 1 ], casted[ 2 ] );
    // TODO(rikisa) cleanup
    // std::cout << std::format(
    //     "({}, {}) = <{}, {}, {}> -> <{}, {}, {}>\n", cur_row, cur_col, pixel[ 0 ], pixel[ 1 ], pixel[ 2 ], casted[ 0
    //     ], casted[ 1 ], casted[ 2 ]
    // );
    index += 1;
  }

  try {
    png_image.write( fpath );
    return true;
  } catch ( std::exception const & ) {
    return false;
  }
}

auto
PNGWriter::create_config() -> ::engine::PartConfig
{
  ::engine::PartConfig config{ PNGWriter::ConfigOpts::config_name() };

  config.add_parameter<std::string>( PNGWriter::ConfigOpts::file_out() );

  return config;
}

auto
PNGWriter::get_part_config() const -> ::engine::PartConfig
{
  ::engine::PartConfig config{ PNGWriter::ConfigOpts::config_name() };

  config.add_parameter<std::string>( PNGWriter::ConfigOpts::file_out(), this->_filepath );

  return config;
}

auto
PNGWriter::create_part( ::engine::PartConfig const & config ) -> ::engine::part::BaseViewerRef
{
  auto fname = config.get_as_or_throw<std::string>( PNGWriter::ConfigOpts::file_out() );

  // TODO(10)
  // std::cout << std::format( "Creating for {}={}\n", PNGWriter::ConfigOpts::file_out(), fname );

  return std::make_shared<PNGWriter>( fname );
}

}; // namespace viewer
