#pragma once

#include <filesystem>

#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace viewer {

class PNGWriter : public engine::part::BaseViewer {
    using ImageRef = rendering::ImageRef;
    using FsPath = std::filesystem::path;

  public:
    explicit PNGWriter( FsPath filepath );

    void use_logsink( engine::part::BaseLogRef logsink ) override;
    void use_eventqueue( engine::part::BaseEventQueueRef equeue ) override;

    // Controller exposed
    void read_image( ImageRef image ) override;
    void show() override;
    void hide() override;

    // factory functions
    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig const & ) -> ::engine::part::BaseViewerRef;

    [[nodiscard]] auto get_part_config() const -> ::engine::PartConfig override;

    // Config Options
    struct ConfigOpts {
      public:
        static auto config_name() -> std::string { return { s_config_name.cbegin(), s_config_name.cend() }; }

        static auto file_out() -> std::string { return { s_file_out.cbegin(), s_file_out.cend() }; }

      private:
        static std::string_view constexpr s_config_name = "viewer.pngwriter";
        static std::string_view constexpr s_file_out = "filename";
    };

  private:
    ImageRef _image = nullptr;
    FsPath _filepath{};

    std::string _file_extension = ".png";

    [[nodiscard]] static auto save_as_png( FsPath fpath, ImageRef img ) -> bool;
};

} // namespace viewer
