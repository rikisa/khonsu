#include "khonsu/partregistry.hpp"

#include <algorithm>
#include <memory>
#include <string>

#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace engine {

auto
PartRegistry::_create_part( std::string const & key, PartConfig const & part_config ) -> std::any
{
  auto part_it = this->_part_factories.find( key );
  if ( part_it == this->_part_factories.end() ) {
    return {};
  }

  auto maybe_part = part_it->second( part_config );

  return maybe_part;
}

void
PartRegistry::_register_part( std::string const & key, std::function<std::any( PartConfig const & )> part_fac )
{
  this->_part_factories.emplace( key, part_fac );
}

void
PartRegistry::_register_config( std::string const & key, std::function<PartConfig()> conf_fac )
{
  this->_config_factories.emplace( key, conf_fac );
}

auto
PartRegistry::create_config( std::string const & key ) -> std::optional<PartConfig>
{
  auto conf_it = this->_config_factories.find( key );
  if ( conf_it != this->_config_factories.end() ) {
    return conf_it->second();
  }

  return std::nullopt;
}

auto
PartRegistry::registered() -> std::set<std::string>
{
  std::set<std::string> ret{};
  std::for_each( this->_part_factories.cbegin(), this->_part_factories.cend(), [ &ret ]( auto itr ) {
    ret.emplace( itr.first );
  } );
  return ret;
}

} // namespace engine
