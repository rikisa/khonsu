#include "khonsu/khonsu.hpp"

#include <format>
#include <map>
#include <stdexcept>
#include <utility>

#include "khonsu/basepartregistry.hpp"
#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace engine {

Khonsu::Khonsu( std::shared_ptr<BasePartRegistry> registry ) : _registry( std::move( registry ) ){};

void
Khonsu::from_part_configs( PartConfig const & controller, PartConfig const & viewer, PartConfig const & tracer )
{
  // TODO(?) interface can be simplified, only pass config. config name == part name

  auto maybe_controller = this->_registry->create_part<part::BaseController>( controller.name(), controller );
  if ( !maybe_controller.has_value() ) {
    throw std::runtime_error( std::format( "Could not create controller from config '{}'", controller.name() ) );
  }
  this->_controller = maybe_controller.value();

  auto maybe_viewer = this->_registry->create_part<part::BaseViewer>( viewer.name(), viewer );
  if ( !maybe_viewer.has_value() ) {
    throw std::runtime_error( std::format( "Could not create viewer from config '{}'", viewer.name() ) );
  }
  this->_viewer = maybe_viewer.value();

  auto maybe_tracer = this->_registry->create_part<part::BaseTracer>( tracer.name(), tracer );
  if ( !maybe_tracer.has_value() ) {
    throw std::runtime_error( std::format( "Could not create tracer from config '{}'", tracer.name() ) );
  }
  this->_tracer = maybe_tracer.value();
}

auto
Khonsu::get_part_configs() -> std::map<std::string, PartConfig>
{
  std::map<std::string, PartConfig> ret{};

  for ( auto const & conf_name : this->_registry->registered() ) {
    auto maybe_config = this->_registry->create_config( conf_name );

    if ( !maybe_config.has_value() ) {
      // TODO(10) log a warning here
      continue;
    }

    ret.emplace( conf_name, maybe_config.value() );
  }

  return ret;
}

auto
Khonsu::start() noexcept -> bool
{
  if ( this->_controller == nullptr || this->_viewer == nullptr || this->_tracer == nullptr ) {
    return false;
  }

  this->_controller->use_tracer( this->_tracer );
  this->_controller->use_viewer( this->_viewer );

  try {
    // Controller is not a blocking call
    // TODO(#18) rafactor the part interfaces to reflect this
    this->_controller->init();
  } catch ( std::exception const & err ) {
    // TODO(#10) log error here
    return false;
  }

  // Viewer init might be a blocking call
  this->_viewer->init();

  return true;
}

} // namespace engine
