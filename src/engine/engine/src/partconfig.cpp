#include "khonsu/part/partconfig.hpp"

#include <exception>
#include <format>
#include <string>

namespace engine {

PartConfig::PartConfig( std::string part_name ) : _name( std::move( part_name ) ){};

template<ValidParameterType valuetype>
auto
PartConfig::add_parameter( std::string const & name ) -> Parameter &
{
  auto ret = this->_config.emplace( name, Parameter{ .name = name, .value = {}, .type_hint = typeid( valuetype ) } );
  return ret->second;
}

template<ValidParameterType valuetype>
auto
PartConfig::add_parameter( std::string const & name, valuetype default_value ) -> Parameter &
{
  auto & param = this->add_parameter<valuetype>( name );
  this->set_parameter( name, default_value );
  return param;
}

auto
PartConfig::get_parameter( std::string const & name ) const -> std::optional<Parameter const>
{
  auto ret = this->_config.find( name );

  if ( ret == this->_config.end() ) {
    return std::nullopt;
  }

  return ret->second;
}

auto
PartConfig::get_or_throw( std::string const & name ) const -> std::any
{
  auto param = this->_config.find( name );

  auto err_msg = std::format( "Config for '{}' needs parameter '{}' set", this->name(), name );
  if ( param == this->_config.end() ) {
    throw config_error( err_msg );
  }

  auto maybe_value = param->second.value;
  if ( !maybe_value.has_value() ) {
    throw config_error( err_msg );
  }

  return maybe_value;
}

template<ValidParameterType valuetype>
auto
PartConfig::set_parameter( std::string const & name, valuetype value ) -> bool
{
  auto maybe_param = this->get_parameter( name );
  if ( maybe_param.has_value() ) {
    this->_config.find( name )->second.value = value;
    ;
    return true;
  }
  return false;
}

auto
PartConfig::name() const -> std::string
{
  return _name;
}

auto
operator==( engine::PartConfig const & one, engine::PartConfig const & other ) -> bool
{
  return one.name() == other.name();
}

} // namespace engine

template auto engine::PartConfig::add_parameter<>( std::string const &, double ) -> engine::PartConfig::Parameter &;
template auto engine::PartConfig::add_parameter<>( std::string const &, int ) -> engine::PartConfig::Parameter &;
template auto engine::PartConfig::add_parameter<>( std::string const &, std::string )
    -> engine::PartConfig::Parameter &;

// GCC does not pick up the other template specializations at higher optimizations
template auto engine::PartConfig::add_parameter<double>( std::string const & ) -> engine::PartConfig::Parameter &;
template auto engine::PartConfig::add_parameter<int>( std::string const & ) -> engine::PartConfig::Parameter &;
template auto engine::PartConfig::add_parameter<std::string>( std::string const & ) -> engine::PartConfig::Parameter &;
template auto engine::PartConfig::set_parameter<>( std::string const &, double ) -> bool;
template auto engine::PartConfig::set_parameter<>( std::string const &, int ) -> bool;
template auto engine::PartConfig::set_parameter<>( std::string const &, std::string ) -> bool;
