#pragma once

#include <functional>
#include <set>
#include <unordered_map>

#include "khonsu/basepartregistry.hpp"
#include "khonsu/part/partconfig.hpp"

namespace engine {

class PartRegistry : public BasePartRegistry {
  public:
    auto create_config( std::string const & key ) -> std::optional<PartConfig> override;

    auto registered() -> std::set<std::string> override;

  private:
    void _register_part( std::string const & key, std::function<std::any( PartConfig const & )> part_fac ) override;
    void _register_config( std::string const & key, std::function<PartConfig()> conf_fac ) override;
    auto _create_part( std::string const & key, PartConfig const & part_config ) -> std::any override;

    std::unordered_map<std::string, std::function<std::any( engine::PartConfig const & )>> _part_factories;
    std::unordered_map<std::string, std::function<engine::PartConfig()>> _config_factories;
};

} // namespace engine
