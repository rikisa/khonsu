#pragma once

#include <concepts>
#include <functional>
#include <memory>
#include <set>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace engine {

namespace detail {
template<typename...>
struct GetPartBaseType : std::false_type {
    using BaseType = void;
};

template<typename parttype, typename maybe_base, typename... more>
struct GetPartBaseType<parttype, maybe_base, more...>
    : std::conditional_t<std::is_base_of_v<maybe_base, parttype>, std::true_type, GetPartBaseType<parttype, more...>> {
    using BaseType = std::conditional_t<
        std::is_base_of_v<maybe_base, parttype>,
        maybe_base,
        typename GetPartBaseType<parttype, more...>::BaseType>;
};

template<typename parttype>
struct DerivedFromBasePart : GetPartBaseType<parttype, part::BaseController, part::BaseTracer, part::BaseViewer> {};

} // namespace detail

template<typename parttype>
concept HasStaticFactories = requires( PartConfig const & conf ) {
  {
    parttype::create_config()
  } -> std::same_as<PartConfig>;

  {
    parttype::create_part( conf )
  } -> std::same_as<std::shared_ptr<typename detail::DerivedFromBasePart<parttype>::BaseType>>;
};


template<typename parttype>
concept IsBasePart = detail::DerivedFromBasePart<parttype>::value;

template<typename parttype>
concept ValidPartType = ( IsBasePart<parttype> && HasStaticFactories<parttype> );

class BasePartRegistry {
  public:
    virtual ~BasePartRegistry() = default;

    /*!
    @brief Register a Part for later ceretion.

    The types that can registered and created are constrained,
    The must extend BaseController, BaseTracer and BaseViewer.
    Additionally, they must provide static factory functions
    for creating the default config and instances.

    @tparam ValidPartType Constrained type
    @param key Identifier key used in creation
    */
    template<ValidPartType parttype>
    void register_part( std::string const & key )
    {
      this->_register_part( key, parttype::create_part );
      this->_register_config( key, parttype::create_config );
    }

    template<IsBasePart parttype>
    auto create_part( std::string const & key, PartConfig const & part_config )
        -> std::optional<std::shared_ptr<parttype>>
    {
      auto maybe_part = this->_create_part( key, part_config );
      if ( maybe_part.has_value() ) {
        return std::any_cast<std::shared_ptr<parttype>>( maybe_part );
      }
      return std::nullopt;
    }

    virtual auto create_config( std::string const & ) -> std::optional<PartConfig> = 0;

    virtual auto registered() -> std::set<std::string> = 0;

  private:
    virtual void _register_part( std::string const &, std::function<std::any( PartConfig const & )> ) = 0;
    virtual void _register_config( std::string const &, std::function<PartConfig()> ) = 0;
    virtual auto _create_part( std::string const &, PartConfig const & ) -> std::any = 0;
};

} // namespace engine
