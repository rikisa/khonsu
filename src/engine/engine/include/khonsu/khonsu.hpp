#pragma once

#include <map>
#include <memory>
#include <type_traits>

#include "khonsu/basepartregistry.hpp"
#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/baseviewer.hpp"

namespace engine {

class Khonsu {
  public:
    explicit Khonsu( std::shared_ptr<BasePartRegistry> registry );

    /*!
    @brief Assembles parts from config into engine with entry point

    @throws std::runtime_error If any of the PartConfigs is invalid

    @param controller Config used to create PartController
    @param viewer Config used to create PartViewer
    @param tracer Config used to create PartTracer
    */
    void from_part_configs( PartConfig const & controller, PartConfig const & viewer, PartConfig const & tracer );

    /*!
    @brief Get all configs available from the BasePartRegistry

    @return mapping from PartConfig.name() to the default, not set part config
    */
    auto get_part_configs() -> std::map<std::string, PartConfig>;

    /*!
    @brief Enters Controller main loop after assembling engine

    @return true if successfully started, else false
    */
    auto start() noexcept -> bool;

  private:
    std::shared_ptr<BasePartRegistry> _registry;

    std::shared_ptr<part::BaseTracer> _tracer = nullptr;
    std::shared_ptr<part::BaseViewer> _viewer = nullptr;
    std::shared_ptr<part::BaseController> _controller = nullptr;
};

} // namespace engine
