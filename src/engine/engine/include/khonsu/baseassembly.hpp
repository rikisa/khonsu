#pragma once

namespace engine {

class BaseAssembly {
  public:
    virtual ~BaseAssembly() = default;
};

} // namespace engine
