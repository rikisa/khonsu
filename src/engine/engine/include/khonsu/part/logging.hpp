#pragma once

#include <memory>

namespace engine::part {

class BaseLogger {
  public:
    virtual ~BaseLogger() = default;
};

using LoggerRef = std::shared_ptr<BaseLogger>;

} // namespace engine::part
