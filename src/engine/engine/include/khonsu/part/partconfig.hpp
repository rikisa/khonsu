#pragma once
#include <any>
#include <functional>
#include <optional>
#include <stdexcept>
#include <string>
#include <typeindex>
#include <unordered_map>

namespace engine {

class config_error : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
    using std::runtime_error::what;
};

template<typename valuetype>
concept ValidParameterType = std::disjunction<
    std::is_same<valuetype, int>,
    std::is_same<valuetype, double>,
    std::is_same<valuetype, std::string>>::value;

class PartConfig {
  public:
    struct Parameter {
        std::string name;
        std::any value;
        std::type_index type_hint;
    };

    using ParameterSet = std::unordered_multimap<std::string, Parameter>;

    PartConfig( std::string part_name ); // NOLINT(*-explicit-*) We want conversions

    template<ValidParameterType valuetype>
    auto add_parameter( std::string const & name ) -> Parameter &;

    template<ValidParameterType valuetype>
    auto add_parameter( std::string const & name, valuetype default_value ) -> Parameter &;

    auto get_parameter( std::string const & name ) const -> std::optional<Parameter const>;

    auto get_or_throw( std::string const & name ) const -> std::any;

    template<typename return_type>
    auto get_as_or_throw( std::string const & name ) const -> return_type
    {
      auto any_or_throw = get_or_throw( name );
      return std::any_cast<return_type>( any_or_throw );
    }

    template<ValidParameterType valuetype>
    auto set_parameter( std::string const & name, valuetype value ) -> bool;

    auto name() const -> std::string;

  private:
    std::string _name;
    ParameterSet _config;
};

auto operator==( engine::PartConfig const & one, engine::PartConfig const & other ) -> bool;

} // namespace engine
