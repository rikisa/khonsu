#pragma once

#include "khonsu/partforwards.hpp"
#include "khonsu/rendering/basecamera.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/rendering/scene.hpp"

namespace engine::part {

class BaseTracer {
  public:
    virtual ~BaseTracer() = default;

    virtual void use_logsink( BaseLogRef ) = 0;
    virtual void use_eventqueue( BaseEventQueueRef ) = 0;

    // Controller exposed

    /*!
    @brief Set image the traced pixel are written to.
    Must be set before trace_scene()

    @param image Pointer to the BaseImage instance
    */
    virtual void write_image( rendering::ImageRef ) = 0;
    /*!
    @brief Set camera used to generate Rays.
    Must be set before trace_scene(). Amount of rays is infered
    from image size.

    @param camera Pointer to the BaseCamera instance
    */
    virtual void set_camera( rendering::BaseCameraRef ) = 0;

    virtual void trace_scene( rendering::SceneRef ) = 0;
};


} // namespace engine::part
