#pragma once

#include "khonsu/part/partconfig.hpp"
#include "khonsu/partforwards.hpp"
#include "khonsu/rendering/image.hpp"

namespace engine::part {

class BaseViewer {
  public:
    virtual ~BaseViewer() = default;

    virtual void use_logsink( BaseLogRef ) = 0;
    virtual void use_eventqueue( BaseEventQueueRef ) = 0;

    /*!
    @brief Do all initialization.
    This is supposed for Viewer that need eg an OpenGL context constructed.
    init() is allowed to enter a main loop, but must then listen to the
    stop events, and throw an error upont init(), it the use_eventqueue()
    was never called.
    */
    virtual void init(){};
    // TODO(12) write a generic viewer test, that checks for the init() contract. Will need eventqueue first

    // Controller exposed
    virtual void read_image( rendering::ImageRef ) = 0;
    virtual void show() = 0;
    virtual void hide() = 0;

    [[nodiscard]] virtual auto get_part_config() const -> ::engine::PartConfig = 0;
};

} // namespace engine::part
