#pragma once

#include <functional>
#include <memory>

#include "khonsu/engine/common.hpp"

namespace engine::part {
enum class Event { Start, Stop };

class SynchronizerBase {

  public:
    virtual ~SynchronizerBase() = default;

    virtual void post_event( Event ) = 0;
    virtual void callback_on_event( Event, std::function<void()> ) = 0;

    template<class Connectee>
    void callback_member_on_event(
        engine::Event event, Connectee * obj, std::function<void( Connectee * )> const & member_func
    );

  private:
    LogSink log;
};

template<class Connectee>
void
SynchronizerBase::callback_member_on_event(
    engine::Event event, Connectee * obj, std::function<void( Connectee * )> const & member_func
) {

  this->callback_on_event( event, [ & ]() {
    if ( obj != nullptr ) {
      member_func( obj );
      return;
    }
    this->log->debug( "Could not call back, object is dead already" );
  } );
}

using SynchronizerPart = SynchronizerBase *;

} // namespace engine::part
