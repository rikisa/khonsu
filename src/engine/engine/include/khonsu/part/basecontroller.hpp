#pragma once

#include "khonsu/partforwards.hpp"

namespace engine::part {

class BaseController {
  public:
    virtual ~BaseController() = default;

    virtual void use_logsink( BaseLogRef ) = 0;
    virtual void use_eventqueue( BaseEventQueueRef ) = 0;

    virtual void use_viewer( BaseViewerRef ) = 0;
    virtual void use_tracer( BaseTracerRef ) = 0;

    /*!
    @brief Initialize and start controller.

    Can start a new thread and assume all passed refs are thread safe.
    If a thread is created, it must be stopped and joined() on stop event
    or latest upon desturction of controller. Can not block and must return
    control flow to calling site.
    */
    virtual void init() = 0;
};

} // namespace engine::part
