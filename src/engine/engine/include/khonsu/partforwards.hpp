#pragma once

#include <memory>

namespace engine::part {

class BaseViewer;
class BaseTracer;
class BaseController;

using BaseViewerRef = std::shared_ptr<BaseViewer>;
using BaseTracerRef = std::shared_ptr<BaseTracer>;
using BaseControllerRef = std::shared_ptr<BaseController>;


class BaseLog;
class BaseEventQueue;

using BaseLogRef = std::shared_ptr<BaseLog>;
using BaseEventQueueRef = std::shared_ptr<BaseEventQueue>;

} // namespace engine::part
