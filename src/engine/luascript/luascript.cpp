#include "khonsu/enginepart/luascript.hpp"

#include <memory>
#include <variant>

#include "khonsu/engine/common.hpp"
#include "khonsu/engine/part.hpp"

namespace engine::LuaScript {

Controller::Controller( PartConfig & config, LogSink & sink ) : log( sink )
{
  auto config_entry = config.get_entry<Opts::Filename>();

  this->script = config_entry.get_value_or<std::string>();

  if ( this->script.empty() ) {
    this->log->critical( "Need valid path to a lua script!" );
    throw part_config_failed( "Need valid path to a lua script!" );
  }
};

void
Controller::start()
{
  this->log->info( "Starting using script {}", this->script.string() );
};

void
Controller::stop() noexcept
{
  this->log->info( "Stopping." );
};

auto
create_part( PartConfig && conf, LogSink && log ) -> ControllerPart
{
  return std::make_shared<Controller>( conf, log );
}

} // namespace engine::LuaScript
