#include <filesystem>
#include <memory>

#include "khonsu/engine/common.hpp"
#include "khonsu/engine/controller.hpp"
#include "khonsu/engine/logging.hpp"
#include "khonsu/engine/part.hpp"
#include "khonsu/engine/renderer.hpp"
#include "khonsu/engine/viewer.hpp"

namespace engine::LuaScript {

enum class Opts { Filename };

class Config : public PartConfig {
  public:
    Config();
};

class Controller : public ControllerBase {
  public:
    ///
    /// @brief Create a new controller from config and log sink
    ///
    /// Ideally, the Controller should not be created directly, but rather
    /// created by the creat_part factory function.
    ///
    /// @param[in] config LuaScript configuration
    /// @param[in] logger Sink used for logging
    ///
    /// @throws part_config_failed If config is invalid
    ///
    Controller( PartConfig & /*config*/, LogSink & /*sink*/ );

    void start() override;
    void stop() noexcept override;

  private:
    LogSink log;
    std::filesystem::path script;
};

auto create_part( PartConfig && /*conf*/, LogSink && /*log*/ ) -> ControllerPart;

} // namespace engine::LuaScript
