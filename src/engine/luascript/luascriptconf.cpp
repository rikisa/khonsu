#include "khonsu/enginepart/luascript.hpp"

namespace engine::LuaScript {

Config::Config()
{
  this->add_entry<Opts::Filename>(
      { .option_char_cmd = "f",
        .option_long_cmd = "luascript",
        .option_description = "Luascript that defines the scene to parse",
        .type_hint = ValueType::String }
  );
}

} // namespace engine::LuaScript
