#include "khonsu/controller/staticcontroller.hpp"

#include <cmath>
#include <memory>
#include <numbers>
#include <stdexcept>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/objects/objects.hpp"
#include "khonsu/part/basetracer.hpp" // IWYU pragma: keep
#include "khonsu/part/baseviewer.hpp" // IWYU pragma: keep
#include "khonsu/rendering/basecamera.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/rendering/scene.hpp"
#include "khonsu/rendering/stochasticcamera.hpp"

namespace controller {

using linalg::FloatType;

void
StaticControl::use_logsink( engine::part::BaseLogRef logsink )
{
  (void)logsink;
}

void
StaticControl::use_eventqueue( engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

void
StaticControl::use_viewer( engine::part::BaseViewerRef viewer )
{
  this->_viewer = viewer;
}

void
StaticControl::use_tracer( engine::part::BaseTracerRef tracer )
{
  this->_tracer = tracer;
}

void
StaticControl::_validate_config()
{
  if ( this->_viewer == nullptr ) {
    throw std::logic_error( "use_viewer() not called before start()" );
  }
  if ( this->_tracer == nullptr ) {
    throw std::logic_error( "use_tracer() not called before start()" );
  }
}

void
StaticControl::init()
{
  this->_validate_config();

  std::size_t constexpr row_pixel = 400; // 480; //
  std::size_t constexpr col_pixel = 400; // 640; //
  // constexpr std::size_t col_pixel = 4048;
  // constexpr std::size_t row_pixel = 4048;

  FloatType constexpr field_of_view = std::numbers::pi_v<FloatType> * 0.5; // 90 deg

  // Here the main function body. Here e.g. lua context can be
  // created, viewer, tracer and core elements bound,
  // to allow for dynamic scene creation and tracing.
  // The static controller does this all staticly and is intendet to be
  // used for benchmarking purposes.

  // create image to be shared between read-only viewer and write-only tracer
  auto image = std::make_shared<rendering::Image>( row_pixel, col_pixel );
  this->_viewer->read_image( image );
  this->_tracer->write_image( image );

  // create camera that must match the
  // auto camera = std::make_shared<rendering::BaseCamera>();
  std::size_t constexpr pixel_samples = 100;
  auto camera = std::make_shared<rendering::StochasticCamera>( pixel_samples );

  camera->set_shape( image->height(), image->width(), field_of_view );
  this->_tracer->set_camera( camera );

  auto scene = this->_create_scene();

  // linalg::CoordBase origin{ linalg::Vector( { 0, 0, 1 } ) };
  // return std::make_shared<objects::Sphere>(
  //     linalg::CoordBase{}, material::Material{ material::Color{ { 0.6, 0.1, 0.3 } } }, 0.3
  // );
  // return std::make_shared<objects::DebugSphere>( origin, objects::DebugSphere::ColorMode::DistanceMix, 0.3 );
  using linalg::CoordBase;
  using linalg::Vector;
  using material::BaseMaterial;
  using material::Color;
  using material::Dielectric;
  using material::DiffuseLambertian;
  using material::DiffuseUniform;
  using material::Metal;
  using objects::DebugSphere;
  using objects::Sphere;

  // Set the Scene

  // Debug Spheres
  // scene->add_object(
  //     std::make_shared<DebugSphere>(
  //       CoordBase{ Vector( { 0, 0, 1 } ) }, DebugSphere::ColorMode::Normals, 0.3 )
  // );
  // scene->add_object(
  //     std::make_shared<DebugSphere>(
  //       CoordBase{ Vector( { 0.2, 0.7, 2 } ) }, DebugSphere::ColorMode::Normals, 0.1 )
  // );
  // scene->add_object( std::make_shared<DebugSphere>(
  //     CoordBase{ Vector( { 0.2, -0.8, 1.2 } ) }, DebugSphere::ColorMode::CheckBoard, 0.2
  // ) );

  // scene->add_object( std::make_shared<Sphere>(
  //     CoordBase{ Vector( { -0.3, -0.8, 1 } ) }, std::make_shared<DiffuseUniform>( Color{ { 0.6, 0.1, 0.3 } } ), 0.2
  // ) );

  // scene->add_object( std::make_shared<Sphere>(
  //     CoordBase{ Vector( { -0.3, 0.8, 1 } ) }, std::make_shared<DiffuseLambertian>( Color{ { 0.6, 0.1, 0.3 } } ), 0.2
  // ) );

  // RGB
  // auto center = std::make_shared<Sphere>(
  //     CoordBase{ Vector( { 0, 0, 1 } ) }, std::make_shared<DiffuseLambertian>( Color{ { 0.0, 0.0, 0.5 } } ), 0.5
  // );
  // auto left = std::make_shared<Sphere>(
  //     CoordBase{ Vector( { 0, -std::sqrt( 0.5 ), std::sqrt( 0.5 ) + 1 } ) },
  //     std::make_shared<DiffuseLambertian>( Color{ { 0.0, 0.5, 0.0 } } ), 0.5
  // );
  // auto right = std::make_shared<Sphere>(
  //     CoordBase{ Vector( { 0, std::sqrt( 0.5 ), std::sqrt( 0.5 ) + 1 } ) },
  //     std::make_shared<DiffuseUniform>( Color{ { 0.5, 0.0, 0.0 } } ), 0.5
  // );

  auto left = std::make_shared<Sphere>(
      CoordBase{ Vector( { -0.2, -0.6, 1 } ) }, std::make_shared<DiffuseLambertian>( Color{ { 0.0, 0.5, 0.0 } } ), 0.3
  );
  auto right = std::make_shared<Sphere>(
      CoordBase{ Vector( { -0.2, 0.6, 1.0 } ) }, std::make_shared<DiffuseUniform>( Color{ { 0.5, 0.0, 0.0 } } ), 0.3
  );

  auto center = std::make_shared<Sphere>(
      CoordBase{ Vector( { -0.2, 0, 1 } ) }, std::make_shared<Dielectric>( Color{ { 0.9, 0.9, 1.0 } }, 0.0, 0.9 ), 0.3
  );

  auto ground = std::make_shared<Sphere>(
      CoordBase{ Vector( { -1000.5, 0, 1 } ) }, std::make_shared<DiffuseLambertian>( Color{ { 0.4, 0.4, 0.4 } } ), 1000
  );

  auto front_left = std::make_shared<Sphere>(
      CoordBase{ Vector( { -0.3, -0.4, 0.5 } ) }, std::make_shared<Metal>( Color{ { 0.8, 0.5, 0.2 } }, 0.1 ), 0.2
  );
  auto front_right = std::make_shared<DebugSphere>(
      CoordBase{ Vector( { -0.3, 0.4, 0.5 } ) }, std::make_shared<DiffuseLambertian>( Color{ { 0.8, 0.5, 0.2 } } ), 0.2,
      DebugSphere::ColorMode::Normals
  );

  auto center_upper = std::make_shared<Sphere>(
      CoordBase{ Vector( { 0.4, 0.0, 1.0 } ) }, std::make_shared<Metal>( Color{ { 0.5, 0.45, 0.7 } } ), 0.3
  );
  auto left_upper = std::make_shared<Sphere>(
      CoordBase{ Vector( { 0.35, -0.6, 1.0 } ) }, std::make_shared<Dielectric>( Color{ { 1.0, 0.9, 0.9 } }, 0.0, 1.5 ),
      0.25
  );

  auto left_upper_inner = std::make_shared<Sphere>(
      CoordBase{ Vector( { 0.35, -0.6, 1.0 } ) }, std::make_shared<Metal>( Color{ { 0.5, 0.4, 0.35 } }, 0.05 ), 0.1
  );

  scene->add_object( std::move( center ) );
  scene->add_object( std::move( left ) );
  scene->add_object( std::move( right ) );
  scene->add_object( std::move( front_left ) );
  scene->add_object( std::move( front_right ) );
  scene->add_object( std::move( center_upper ) );
  scene->add_object( std::move( left_upper ) );
  scene->add_object( std::move( left_upper_inner ) );

  scene->add_object( std::move( ground ) );

  // Trace the scene, potentially concurrently
  this->_tracer->trace_scene( scene );

  // Gamma correcture
  for ( auto & px_it : *( image ) ) {
    for ( auto & val : px_it ) {
      val = std::pow( val, 1.0f / 2.2f );
    }
  }

  // Write/Show image
  this->_viewer->show();
}

auto
StaticControl::_create_scene() -> rendering::SceneRef
{
  return std::make_shared<rendering::Scene>();
}

// linalg::CoordBase origin{ linalg::Vector( { 0, 0, 1 } ) };
// return std::make_shared<objects::Sphere>(
//     linalg::CoordBase{}, material::Material{ material::Color{ { 0.6, 0.1, 0.3 } } }, 0.3
// );
// return std::make_shared<objects::DebugSphere>( origin, objects::DebugSphere::ColorMode::DistanceMix, 0.3 );

// factory functions
auto
StaticControl::create_config() -> ::engine::PartConfig
{
  return { StaticControl::ConfigOpts::config_name() };
}

auto
StaticControl::create_part( ::engine::PartConfig const & config ) -> ::engine::part::BaseControllerRef
{
  (void)config;
  return std::make_shared<StaticControl>();
}

}; // namespace controller
