#pragma once

#include <string>

#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/partconfig.hpp"
#include "khonsu/partforwards.hpp"
#include "khonsu/rendering/scene.hpp"

namespace controller {

class StaticControl : public engine::part::BaseController {
  public:
    void use_logsink( engine::part::BaseLogRef logsink ) override;
    void use_eventqueue( engine::part::BaseEventQueueRef equeue ) override;

    void use_viewer( engine::part::BaseViewerRef viewer ) override;
    void use_tracer( engine::part::BaseTracerRef tracer ) override;

    void init() override;

    // factory functions
    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig const & ) -> ::engine::part::BaseControllerRef;

    struct ConfigOpts {
        static auto config_name() -> std::string { return { s_config_name.cbegin(), s_config_name.cend() }; }

      private:
        static std::string_view constexpr s_config_name = "control.static";
    };

  private:
    engine::part::BaseTracerRef _tracer = nullptr;
    engine::part::BaseViewerRef _viewer = nullptr;

    // Throws if viewer and tracer are not set
    void _validate_config();

    auto _create_scene() -> rendering::SceneRef;
};

} // namespace controller
