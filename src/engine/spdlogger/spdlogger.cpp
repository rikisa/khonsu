#include "khonsu/enginepart/spdlogger.hpp"

#include "khonsu/engine/logging.hpp"
#include "khonsu/engine/part.hpp"
#include <spdlog/fmt/fmt.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace engine {

SpdLogger::SpdLogger( PartConfig & config )
{
  for ( auto & [ key, entry ] : config.get_config() ) {
    auto as_enum = static_cast<SpdLoggerOpts>( key );
    switch ( as_enum ) {
    case ( SpdLoggerOpts::Verbose ):
      this->set_verbosity( entry.get_value_or<int>() );
    }
  };
}

auto
SpdLogger::get_sink( std::string const & name ) -> LogSink
{
  auto ret = spdlog::stdout_color_mt( name );
  ret->set_pattern( "[%n] %t %^%L%$ %v" );
  ret->set_level( this->level );
  return ret;
}

void
SpdLogger::set_verbosity( int verbosity )
{
  if ( verbosity > 0 ) {
    this->level = spdlog::level::level_enum::debug;
  }
}

auto
SpdLogger::create_part( PartConfig conf ) -> LoggerPart
{
  return std::make_shared<SpdLogger>( conf );
};

} // namespace engine
