#include "khonsu/enginepart/spdlogger.hpp"

namespace engine {

SpdLogger::Config::Config()
{
  this->add_entry<SpdLoggerOpts::Verbose>(
      { .option_char_cmd = "v",
        .option_long_cmd = "verbose",
        .option_description = "Debug logging",
        .type_hint = ValueType::Count }
  );
}

} // namespace engine
