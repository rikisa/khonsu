#pragma once

#include <memory>

#include "khonsu/engine/logging.hpp"
#include "khonsu/engine/part.hpp"
#include <spdlog/common.h>

namespace engine {

enum class SpdLoggerOpts { Verbose };

class SpdLogger : public BaseLogger {
  public:
    explicit SpdLogger( PartConfig & /*config*/ );
    static auto create_part( PartConfig /*conf*/ ) -> LoggerPart;

    auto get_sink( std::string const & name ) -> LogSink override;

    struct Config;

  private:
    spdlog::level::level_enum level = spdlog::level::level_enum::info;

    void set_verbosity( int verbosity );
};

struct SpdLogger::Config : public PartConfig {
    Config();
};

} // namespace engine
