#pragma once

#include <filesystem>
#include <typeindex>
#include <unordered_map>

#include "khonsu/part/partconfig.hpp"

#include "cxxopts.hpp"

namespace app {

enum class ParseResult {
  Error, // During parsing an error occured
  Quit,  // After Parsing, there is nothing left to do
  OK     // Configuration for main programm parsed
};

class CommandlineParser {
  public:
    explicit CommandlineParser( std::string const & name, std::string const & help_txt = "" );

    /*!
    @brief Register parameter/fields from configs

    Build the Command line options from configs and a prefix. A field with <fieldname> in
    config can than be accessed in the command line via <prefix>.<fieldname>

    @param prefix String prepended before every field name
    @param config Config with fields and maybe default values
    */
    void register_config( std::string prefix, engine::PartConfig config );

    /*!
    @brief Return the config for a prefix

    @param prefix Returns the config for the provided prefix, iff at least one
                  CLI paramter with this prefix was provided
    */
    auto get_config( std::string prefix ) -> std::optional<engine::PartConfig>;

    /*!
    @brief Parse commandline from main argc and argv

    Return value either indicates a error happend and the app should
    end with an error code, or an options was parsed that indicates the
    app shoudl quit (e.g. --version), or OK.

    @return Enum that indicates parsing result
    */
    auto parse( int argc, char const ** argv ) -> ParseResult;

    /*!
    @brief Gets a positional value

    @return Positional value or empty string
    */
    auto get_positional( std::string positional ) const -> std::string;


  private:
    std::unordered_map<std::string, engine::PartConfig> _part_configs{};
    std::unordered_map<std::string, std::string> _positionals{};
    cxxopts::Options _cmdlopts;
};

} // namespace app
