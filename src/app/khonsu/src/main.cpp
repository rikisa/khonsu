#include <cstdlib>
#include <memory>
#include <ranges>
#include <span>
#include <vector>

#include "khonsu/app/commandlineparser.hpp"
#include "khonsu/controller/staticcontroller.hpp"
#include "khonsu/khonsu.hpp"
#include "khonsu/partregistry.hpp"
#include "khonsu/tracer/simpletracer.hpp"
#include "khonsu/viewer/pngwriter.hpp"

auto
main( int argc, char const ** argv ) -> int
{
  auto args_as_strings = std::span( argv, static_cast<size_t>( argc ) ) |
      std::views::transform( []( auto arg ) { return std::string( arg ); } );
  auto args = std::vector<std::string>( args_as_strings.begin(), args_as_strings.end() );

  // setup parser
  app::CommandlineParser parser{ args.at( 0 ), "Khonsu - a fancy little Raytracer" };

  // Register Parts
  auto registry = std::make_shared<engine::PartRegistry>();
  registry->register_part<viewer::PNGWriter>( viewer::PNGWriter::ConfigOpts::config_name() );
  registry->register_part<controller::StaticControl>( controller::StaticControl::ConfigOpts::config_name() );
  registry->register_part<tracer::SimpleTracer>( tracer::SimpleTracer::ConfigOpts::config_name() );

  // create khonsu top level interface
  engine::Khonsu khonsu{ registry };

  // TODO(#9) Tell parser which configs are there, so it can create commands
  //  instead here we just take the viewe config, set it to a hardcoded output
  //  and pass this to the khonsu interface
  //  conf_options = khonsu.get_configs();
  //  parser.set_config_opts(conf_options);

  switch ( parser.parse( argc, argv ) ) {
  case app::ParseResult::Error:
    return EXIT_FAILURE;
  case app::ParseResult::Quit:
    return EXIT_SUCCESS;
  case app::ParseResult::OK:
    break;
  }

  // TODO(#9) Use parsed configs
  //  config = parser.get_configs()
  //  khonsu.from_part_configs(BaseViers, BaseXYZ, ...)

  auto all_configs = khonsu.get_part_configs();

  auto & viewer_conf = all_configs.at( viewer::PNGWriter::ConfigOpts::config_name() );
  viewer_conf.set_parameter<std::string>( viewer::PNGWriter::ConfigOpts::file_out(), parser.get_positional( "out" ) );

  khonsu.from_part_configs(
      all_configs.at( controller::StaticControl::ConfigOpts::config_name() ),
      all_configs.at( viewer::PNGWriter::ConfigOpts::config_name() ),
      all_configs.at( tracer::SimpleTracer::ConfigOpts::config_name() )
  );

  bool ret = khonsu.start();

  return ret ? EXIT_SUCCESS : EXIT_FAILURE;
}
