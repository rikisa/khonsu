#include "khonsu/app/commandlineparser.hpp"

#include <algorithm>
#include <filesystem>
#include <format>
#include <iostream>

#include "khonsu/app/versioninfo.hpp"

namespace opt = cxxopts;
namespace fs = std::filesystem;

namespace app {

namespace detail {
void
/*!
@brief Print all version information
*/
print_version()
{
  std::cout << std::format(
      "Khonsu version: {}\n"
      "      revision: {}\n"
      "   Lua version: {}\n",
      VerisionInfo::khonsu_version, VerisionInfo::khonsu_revision, VerisionInfo::lua
  );
}
} // namespace detail

CommandlineParser::CommandlineParser( std::string const & name, std::string const & help_txt ) :
    _cmdlopts{ fs::absolute( fs::path( name ) ).filename(), help_txt }
{
  _cmdlopts.add_options()( "version", "Print version and quit" )( "h,help", "Print help and quit" )(
      "out", "Output png file", cxxopts::value<std::string>()->default_value( "out.png" )
  );
  _cmdlopts.parse_positional( { "out" } );
}

auto
CommandlineParser::parse( int argc, char const ** argv ) -> ParseResult
{
  opt::ParseResult result;
  try {
    result = _cmdlopts.parse( argc, argv );
  } catch ( opt::exceptions::no_such_option & err ) {
    std::cout << err.what() << '\n' << _cmdlopts.help();
    return ParseResult::Error;
  } catch ( opt::exceptions::missing_argument & err ) {
    std::cout << err.what() << '\n' << _cmdlopts.help();
    return ParseResult::Error;
  }

  // version and help
  if ( result.count( "version" ) > 0 ) {
    detail::print_version();
    return ParseResult::Quit;
  }
  if ( result.count( "help" ) > 0 ) {
    std::cout << _cmdlopts.help();
    return ParseResult::Quit;
  }

  this->_positionals.emplace( "out", result[ "out" ].as<std::string>() );

  return ParseResult::OK;
}

auto
CommandlineParser::get_positional( std::string positional ) const -> std::string
{
  auto result = std::find_if( this->_positionals.begin(), this->_positionals.end(), [ & ]( auto const & val ) {
    return val.first == positional;
  } );
  if ( result == this->_positionals.end() ) {
    return {};
  }
  return result->second;
}

} // namespace app
