#pragma once

#include <concepts> // IWYU pragma: keep
#include <cstddef>  // IWYU pragma: keep
#include <cstdint>

namespace material {
namespace detail {

// would be different with half precision types
template<typename channel_type>
struct StorageType {
    using type = channel_type;
};

} // namespace detail

template<unsigned int num, typename type>
struct ColorInfo {
    using channel_type = type;
    using storage_type = typename detail::StorageType<channel_type>::type;
    using index_type = uint_fast16_t;

    static index_type constexpr channel_num = num;
    static index_type constexpr bits_per_channel = sizeof( channel_type );
    static index_type constexpr bits = sizeof( channel_type ) * num;
};


template<class maybe_color>
concept SupportedColor = requires {
  requires std::integral<typename maybe_color::channel_type> || std::floating_point<typename maybe_color::channel_type>;
  requires maybe_color::channel_num >= 1;
};

using RgbUint16bpc = ColorInfo<3U, uint16_t>;
using RgbFloat32bpc = ColorInfo<3U, float>;

} // namespace material
