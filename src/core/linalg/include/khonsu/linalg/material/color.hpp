#pragma once

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material/colorinfo.hpp"

/* Array class for color space
 * implements all functions for
 * color manipulation
 */
namespace material {

template<SupportedColor color_info>
class GenericColor : public linalg::ArrayN<typename color_info::channel_type, color_info::channel_num> {
  public:
    using array_type = linalg::ArrayN<typename color_info::channel_type, color_info::channel_num>;

    GenericColor() = default;
    GenericColor( array_type && ); // NOLINT(*-constructor)
    explicit GenericColor( array_type const & );

    using linalg::ArrayN<typename color_info::channel_type, color_info::channel_num>::ArrayN;
    using linalg::ArrayN<typename color_info::channel_type, color_info::channel_num>::operator+;

    auto blend( GenericColor<color_info> const & other ) -> GenericColor<color_info>;

    static auto from_vector( linalg::Vector ) -> GenericColor<color_info>;
};

using Color = GenericColor<RgbFloat32bpc>;

} // namespace material
