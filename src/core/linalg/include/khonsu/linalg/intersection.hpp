#pragma once

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"

namespace linalg {

class Intersection {
  protected:
    using Material = material::BaseMaterial;

  public:
    void set_at( Ray const &, FloatType at_t );
    void set_normal( Vector && norm );
    void set_material( Material const * material );

    [[nodiscard]] auto at_t() const -> FloatType;
    [[nodiscard]] auto at_point() const -> Vector const &;
    [[nodiscard]] auto with_normal() const -> Vector const &;
    [[nodiscard]] auto with_material() const -> Material const &;

  private:
    FloatType _at_t = {};
    Vector _at_point = {};
    Vector _with_normal = {};
    Material const * _with_material = nullptr;
};

} // namespace linalg
