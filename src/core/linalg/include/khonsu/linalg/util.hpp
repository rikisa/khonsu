#pragma once

#include <cstdint>
#include <random>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/types.hpp"

namespace linalg::util {

class XorShift {
  public:
    using StateType = std::uint64_t;

    explicit XorShift( StateType seed );
    auto next_state() -> StateType;
    auto next_value() -> FloatType;

  private:
    StateType _state;
    static FloatType constexpr _divisor = 1.0 / 18446744073709551616.0;
};

class SplitMix {
  public:
    using StateType = std::uint64_t;

    explicit SplitMix( StateType seed );
    auto next_state() -> StateType;
    auto next_value() -> FloatType;

  private:
    StateType _state;
    static FloatType constexpr _divisor = 1.0 / 18446744073709551616.0;
};

class MersenneTwister : public std::mt19937_64 {
  public:
    using StateType = std::uint64_t;

    using std::mt19937_64::mt19937_64;

    auto next_state() -> StateType;
    auto next_value() -> FloatType;

  private:
    static FloatType constexpr _divisor = 1.0 / 18446744073709551616.0;
};

using DefaultPrng = MersenneTwister;

template<class rnd_engine = DefaultPrng>
class StochasicSampler {
  public:
    StochasicSampler();

    auto get_random_dir() -> Vector;
    auto get_random_value() -> FloatType;
    auto get_random_value( FloatType lower, FloatType upper ) -> FloatType;
};

} // namespace linalg::util
