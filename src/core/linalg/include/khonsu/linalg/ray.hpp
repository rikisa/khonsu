#pragma once

#include <cstddef>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/types.hpp"

namespace linalg {


class Ray {
  public:
    Ray() = default;
    Ray( Vector pos, Vector dir );

    /*!
    @brief Sets the position of the ray in image plane pixel coordinates

    */
    void set_image_pos( std::size_t row, std::size_t col );
    [[nodiscard]] auto image_row() const -> std::size_t;
    [[nodiscard]] auto image_col() const -> std::size_t;

    /*!
    @brief Additively merge in color

    @deprecated Will be removes, color is separated from ray

    */
    void add_color( material::Color const & );

    /*!
    @brief Current color of the ray

    @deprecated Will be removes, color is separated from ray

    */
    [[nodiscard]] auto color() const -> material::Color;

    /*!
    @brief Resets color to RGB(0, 0, 0)

    @deprecated Will be removes, color is separated from ray

    */
    void reset_color();

    /*!
    @brief Move ray origin, keeping its direction.

    //TODO Remove
    @deprecated Leads to errenous ray state, will be removed

    @param pos Position the ray is moved to
    */
    void move( Vector pos );

    /*!
    @brief Move ray origin and change its orientation

    Will *always* normalize the dir

    @param pos Position the ray is moved to
    @param dir New direction of ray
    */
    void move( Vector pos, Vector dir );

    /*!
    @brief Get point at distance from pos along dir

    Computes Ray::pos() + dist * Ray::dir()

    @param dist Distance from pos
    @returns vector
    */
    [[nodiscard]] auto at_distance( FloatType dist ) const -> Vector;

    /*!
    @brief Point ray direction while keeping its position.

    Changes only ray.dir(), but not ray.pos(); - dir() will be normalized

    @param to_pos Point to to_pos so that ray.pos() + t * ray.dir() = to_pos
    */
    void point_at( Vector to_pos );

    /*!
    @brief Position provided via last point_at() call

    */
    [[nodiscard]] auto ref_point() const -> linalg::Vector;

    [[nodiscard]] auto pos() const -> Vector const &;
    [[nodiscard]] auto dir() const -> Vector const &;


  private:
    Vector _pos{};
    Vector _dir{};

    FloatType _image_row = 0;
    FloatType _image_col = 0;

    material::Color _color{};

    Vector _points_at{};
};

} // namespace linalg
