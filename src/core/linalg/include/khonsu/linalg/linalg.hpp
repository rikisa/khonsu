#pragma once

#include <array>
#include <cstddef>
#include <iostream>

#include "khonsu/linalg/types.hpp"

namespace linalg {

template<typename val_type, std::size_t dim>
class ArrayN {
  public:
    using elem_type = val_type;
    using array_type = std::array<val_type, dim>;
    using iterator = std::array<val_type, dim>::iterator;
    using const_iterator = std::array<val_type, dim>::const_iterator;

    static constexpr size_t dimension = dim;

    ArrayN() = default;
    explicit ArrayN( val_type const & );
    explicit ArrayN( std::array<val_type, dim> const & );

    auto operator[]( std::size_t ) const -> val_type;
    auto operator[]( std::size_t ) -> val_type &;
    auto operator=( typename ArrayN<val_type, dim>::array_type const & ) -> ArrayN<val_type, dim> &;

    void operator-=( ArrayN<val_type, dim> const & );
    void operator-=( val_type const & );
    void operator+=( ArrayN<val_type, dim> const & );
    void operator+=( val_type const & );
    void operator*=( ArrayN<val_type, dim> const & );
    void operator*=( val_type const & );
    void operator/=( ArrayN<val_type, dim> const & );
    void operator/=( val_type const & );

    // infix
    auto operator-( ArrayN<val_type, dim> const & ) const -> ArrayN<val_type, dim>;
    auto operator-( val_type const & ) const -> ArrayN<val_type, dim>;
    auto operator+( ArrayN<val_type, dim> const & ) const -> ArrayN<val_type, dim>;
    auto operator+( val_type const & ) const -> ArrayN<val_type, dim>;
    auto operator*( ArrayN<val_type, dim> const & ) const -> ArrayN<val_type, dim>;
    auto operator*( val_type const & ) const -> ArrayN<val_type, dim>;
    auto operator/( ArrayN<val_type, dim> const & ) const -> ArrayN<val_type, dim>;
    auto operator/( val_type const & ) const -> ArrayN<val_type, dim>;

    auto operator==( ArrayN<val_type, dim> const & ) const -> bool;
    auto operator!=( ArrayN<val_type, dim> const & ) const -> bool;

    auto cget() const -> std::array<val_type, dim> const &;

    // conversions
    explicit operator std::array<val_type, dim>() const;

    auto begin() -> iterator;
    auto end() -> iterator;

    auto cbegin() const -> const_iterator;
    auto cend() const -> const_iterator;


    auto cross( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
      requires( dim == 3 );

    // inplace normalize to L2 norm,
    // that eucledian length is 1
    auto normalize() -> val_type;

    // the L2 norm
    auto norm() const -> val_type;

    // the sum of all squared elemenrs
    auto sum_of_squares() const -> val_type;

    // element wise sum
    auto sum() const -> val_type;

    // dot product
    auto dot( ArrayN<val_type, dim> const & ) const -> val_type;

    // cosine of angle between two vectors
    auto cos( ArrayN<val_type, dim> const & ) const -> val_type;

    // String representations
    [[nodiscard]] auto repr() const -> std::string;

    // fill with value
    void fill( val_type const & other );

  protected:
    auto _get() -> std::array<val_type, dim> &;

  private:
    std::array<val_type, dim> _values;
};

template<class vec>
auto
cross( vec const && one, vec const && other ) -> vec
  requires( vec::dimension == 3 )
{
  return one.cross( other );
}

/* Matrix class for object coordinate base
 * Defines Orientation of objects in space
 */
template<class vec>
class GenericCoordBase {
  public:
    auto ydir() const -> vec const & { return this->_ydir; }; // rows, 1st dim

    auto xdir() const -> vec const & { return this->_xdir; }; // cols, 2nd dim

    auto zdir() const -> vec const & { return this->_zdir; }; // depth, 3rd dim

    auto pos() const -> vec const & { return this->_pos; }; // position of origin

    void move( vec pos );

    void flip_ydir();
    void flip_xdir();
    void flip_zdir();

    explicit GenericCoordBase( vec const & pos = vec( 0 ) );
    GenericCoordBase( vec const & upward, vec const & forward, vec const & pos = vec( 0 ) );

    using elem_type = vec;

  private:
    void _normalize();
    vec _ydir; // rows, 1st dim
    vec _xdir; // cols, 2nd dim
    vec _zdir; // depth, 3rd dim
    vec _pos;  // position of origin
};

using FArray3 = ArrayN<float, 3>;
using FArray4 = ArrayN<float, 4>;
using DArray3 = ArrayN<FloatType, 3>;
using DArray4 = ArrayN<FloatType, 4>;

using Vector = ArrayN<FloatType, 3>;
using CoordBase = GenericCoordBase<Vector>;

// infix operators
template<typename val_type, size_t dim>
auto operator-( val_type const &, ArrayN<val_type, dim> const & ) -> ArrayN<val_type, dim>;
template<typename val_type, size_t dim>
auto operator+( val_type const &, ArrayN<val_type, dim> const & ) -> ArrayN<val_type, dim>;
template<typename val_type, size_t dim>
auto operator*( val_type const &, ArrayN<val_type, dim> const & ) -> ArrayN<val_type, dim>;
template<typename val_type, size_t dim>
auto operator/( val_type const &, ArrayN<val_type, dim> const & ) -> ArrayN<val_type, dim>;


} // namespace linalg

auto operator<<( std::ostream & out, linalg::Vector const & vec ) -> std::ostream &;
