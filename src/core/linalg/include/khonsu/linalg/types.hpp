#pragma once

#include <limits>

namespace linalg {

using FloatType = double;
auto constexpr eps = std::numeric_limits<FloatType>::epsilon();
auto constexpr min_dist = 1e-10;
auto constexpr infinity = std::numeric_limits<FloatType>::infinity();

} // namespace linalg
