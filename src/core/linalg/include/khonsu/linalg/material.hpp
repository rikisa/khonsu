#pragma once

#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"

namespace material {

class BaseMaterial {
  protected:
    using Ray = linalg::Ray;
    using Vector = linalg::Vector;
    using FloatType = linalg::FloatType;

  public:
    BaseMaterial();
    explicit BaseMaterial( Color color );

    virtual ~BaseMaterial();

    void set_color( Color color );
    [[nodiscard]] auto get_color() const -> Color;
    // TODO(#23) Forward declare Intersect and move use intersect instead of scatter_pos and scatter_normal
    virtual void scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const;

  private:
    Color _color{};
};

class DiffuseUniform : public BaseMaterial {
  public:
    using BaseMaterial::BaseMaterial;

    void scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const override;
};

class DiffuseLambertian : public BaseMaterial {
  public:
    using BaseMaterial::BaseMaterial;

    void scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const override;
};

class Metal : public BaseMaterial {
  public:
    Metal();
    explicit Metal( Color color, FloatType fuzz = 0 );

    void scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const override;

  private:
    FloatType _fuzz = 0.0;
};

class Dielectric : public BaseMaterial {
  public:
    Dielectric();
    explicit Dielectric( Color color, FloatType fuzz = 0, FloatType refractive = 1 );

    void scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const override;

  private:
    FloatType _fuzz = 0.0;
    FloatType _refractive = 1.0;
};

} // namespace material
