#include "khonsu/linalg/linalg.hpp"

#include <array>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <sstream>

namespace linalg::detail {
template<typename val_type, size_t dim>
constexpr auto
init_arr( val_type val ) -> std::array<val_type, dim>
{
  auto ret = std::array<val_type, dim>();
  ret.fill( val );
  return ret;
}
} // namespace linalg::detail

namespace linalg {
template<typename val_type, size_t dim>
ArrayN<val_type, dim>::ArrayN( val_type const & values ) : _values( detail::init_arr<val_type, dim>( values ) ){};

template<typename val_type, size_t dim>
ArrayN<val_type, dim>::ArrayN( std::array<val_type, dim> const & values ) : _values( values ){};

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator[]( size_t index ) const -> val_type
{
  return this->_values[ index ];
};

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator[]( size_t index ) -> val_type &
{
  return this->_values[ index ];
};

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator=( ArrayN<val_type, dim>::array_type const & other ) -> ArrayN<val_type, dim> &
{
  for ( auto it = other.begin(); it != other.end(); ++it ) {
    this->_values[ it - other.begin() ] = *it;
  }
  return *this;
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::fill( val_type const & value )
{
  this->_values.fill( value );
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator-=( ArrayN<val_type, dim> const & other )
{
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->_values[ it - other.cbegin() ] = this->_values[ it - other.cbegin() ] - *it;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator-=( val_type const & scalar )
{
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it -= scalar;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator+=( ArrayN<val_type, dim> const & other )
{
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->_values[ it - other.cbegin() ] = this->_values[ it - other.cbegin() ] + *it;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator+=( val_type const & scalar )
{
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it += scalar;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator*=( ArrayN<val_type, dim> const & other )
{
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->_values[ it - other.cbegin() ] = this->_values[ it - other.cbegin() ] * *it;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator*=( val_type const & scalar )
{
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it *= scalar;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator/=( ArrayN<val_type, dim> const & other )
{
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->_values[ it - other.cbegin() ] = this->_values[ it - other.cbegin() ] / *it;
  }
}

template<typename val_type, size_t dim>
void
ArrayN<val_type, dim>::operator/=( val_type const & scalar )
{
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it /= scalar;
  }
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator-( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret -= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator-( val_type const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret -= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator+( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret += other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator+( val_type const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret += other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator*( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret *= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator*( val_type const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret *= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator/( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret /= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator/( val_type const & other ) const -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( *this );
  ret /= other;
  return ret;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator==( ArrayN<val_type, dim> const & other ) const -> bool
{
  for ( size_t i = 0; i < dim; ++i ) {
    if ( this->_values[ i ] != other[ i ] ) { // NOLINT no bound cehcking needed, as we use n
      return false;
    }
  }

  return true;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::operator!=( ArrayN<val_type, dim> const & other ) const -> bool
{
  return !this->operator==( other );
}

template<typename val_type, size_t dim>
ArrayN<val_type, dim>::operator std::array<val_type, dim>() const
{
  return this->_values;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::cget() const -> std::array<val_type, dim> const &
{
  return this->_values;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::_get() -> std::array<val_type, dim> &
{
  return this->_values;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::begin() -> typename ArrayN<val_type, dim>::iterator
{
  return _values.begin();
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::end() -> typename ArrayN<val_type, dim>::iterator
{
  return _values.end();
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::cbegin() const -> typename ArrayN<val_type, dim>::const_iterator
{
  return _values.cbegin();
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::cend() const -> typename ArrayN<val_type, dim>::const_iterator
{
  return _values.cend();
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::normalize() -> val_type
{
  val_type norm = this->norm();
  for ( size_t i = 0; i < ArrayN<val_type, dim>::dimension; ++i ) {
    this->_values[ i ] = this->_values[ i ] / norm;
  }
  return norm;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::norm() const -> val_type
{
  return std::sqrt( this->sum_of_squares() );
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::sum_of_squares() const -> val_type
{
  val_type sqsum = 0;
  for ( auto it = this->cbegin(); it != this->cend(); ++it ) {
    sqsum += ( *it ) * ( *it );
  }
  return sqsum;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::sum() const -> val_type
{
  val_type sum = 0;
  for ( auto it = this->cbegin(); it != this->cend(); ++it ) {
    sum += *it;
  }
  return sum;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::dot( ArrayN<val_type, dim> const & other ) const -> val_type
{
  val_type sum = 0;
  for ( size_t i = 0; i < dim; ++i ) {
    sum += ( *this )[ i ] * other[ i ];
  }
  return sum;
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::cos( ArrayN<val_type, dim> const & other ) const -> val_type
{
  val_type this_i;
  val_type other_i;
  val_type sqsum_this = 0;
  val_type sqsum_other = 0;
  val_type dot = 0;

  for ( size_t i = 0; i < dim; ++i ) {
    this_i = ( *this )[ i ];
    other_i = other[ i ];

    dot += this_i * other_i;
    sqsum_this += this_i * this_i;
    sqsum_other += other_i * other_i;
  }

  return dot / ( std::sqrt( sqsum_other ) * std::sqrt( sqsum_this ) );
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::cross( ArrayN<val_type, dim> const & other ) const -> ArrayN<val_type, dim>
  requires( dim == 3 )
{
  auto u = *this; // NOLINT for brevity reasons
  auto v = other; // NOLINT for brevity reasons
  auto aaa = u[ 1 ] * v[ 2 ] - u[ 2 ] * v[ 1 ];
  auto bbb = u[ 2 ] * v[ 0 ] - u[ 0 ] * v[ 2 ];
  auto ccc = u[ 0 ] * v[ 1 ] - u[ 1 ] * v[ 0 ];
  return ArrayN<val_type, dim>(
      { static_cast<val_type>( aaa ), static_cast<val_type>( bbb ), static_cast<val_type>( ccc ) }
  );
}

template<class vec>
GenericCoordBase<vec>::GenericCoordBase( vec const & position ) :
    _ydir( { 1, 0, 0 } ), _xdir( { 0, 1, 0 } ), _zdir( { 0, 0, 1 } ), _pos( position ){};

template<class vec>
GenericCoordBase<vec>::GenericCoordBase( vec const & upward, vec const & forward, vec const & pos ) :
    _ydir( upward ), _xdir( cross( forward, upward ) ), _zdir( forward ), _pos( pos )
{
  this->_normalize();
};

template<class vec>
void
GenericCoordBase<vec>::move( vec pos )
{
  this->_pos = std::move( pos );
}

template<class vec>
void
GenericCoordBase<vec>::flip_ydir()
{
  this->_ydir *= -1;
};

template<class vec>
void
GenericCoordBase<vec>::flip_xdir()
{
  this->_xdir *= -1;
};

template<class vec>
void
GenericCoordBase<vec>::flip_zdir()
{
  this->_zdir *= -1;
};

template<class base_vec>
void
GenericCoordBase<base_vec>::_normalize()
{
  this->_ydir.normalize();
  this->_xdir.normalize();
  this->_zdir.normalize();
}

template<typename val_type, size_t dim>
auto
ArrayN<val_type, dim>::repr() const -> std::string
{
  std::stringstream ret{};

  ret << "Arr(";

  size_t i = 0; // NOLINT Loop var
  for ( ; i < ( dim - 1 ); ++i ) {
    ret << ( *this )[ i ] << ", ";
  }
  ret << ( *this )[ dim - 1 ] << ")";

  return ret.str();
}

template<typename val_type, size_t dim>
auto
operator-( val_type const & scalar, ArrayN<val_type, dim> const & arr ) -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( arr );
  for ( size_t i = 0; i < ArrayN<val_type, dim>::dimension; ++i ) {
    ret[ i ] = scalar - ret[ i ];
  }
  return ret;
}

template<typename val_type, size_t dim>
auto
operator+( val_type const & scalar, ArrayN<val_type, dim> const & arr ) -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( arr );
  for ( size_t i = 0; i < ArrayN<val_type, dim>::dimension; ++i ) {
    ret[ i ] = scalar + ret[ i ];
  }
  return ret;
}

template<typename val_type, size_t dim>
auto
operator*( val_type const & scalar, ArrayN<val_type, dim> const & arr ) -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( arr );
  for ( size_t i = 0; i < ArrayN<val_type, dim>::dimension; ++i ) {
    ret[ i ] = scalar * ret[ i ];
  }
  return ret;
}

template<typename val_type, size_t dim>
auto
operator/( val_type const & scalar, ArrayN<val_type, dim> const & arr ) -> ArrayN<val_type, dim>
{
  auto ret = ArrayN<val_type, dim>( arr );
  for ( size_t i = 0; i < ArrayN<val_type, dim>::dimension; ++i ) {
    ret[ i ] = scalar / ret[ i ];
  }
  return ret;
}

template<typename val_type, size_t dim>
auto
cross( ArrayN<val_type, dim> const & some, ArrayN<val_type, dim> const & other ) -> ArrayN<val_type, dim>
  requires( dim == 3 )
{
  auto u = some;
  auto v = other;
  return ArrayN<val_type, dim>(
      { u[ 1 ] * v[ 2 ] - u[ 2 ] * v[ 1 ], u[ 2 ] * v[ 0 ] - u[ 0 ] * v[ 2 ], u[ 0 ] * v[ 1 ] - u[ 1 ] * v[ 0 ] }
  );
}

// Euclidean space
template class ArrayN<float, 3>;
template class ArrayN<double, 3>;

// Euclidean space + Time
template class ArrayN<float, 4>;
template class ArrayN<double, 4>;

// Image Space
template class ArrayN<float, 2>;
template class ArrayN<double, 2>;

// Color
template class ArrayN<uint8_t, 3>;
template class ArrayN<uint8_t, 4>;
template class ArrayN<uint16_t, 3>;
template class ArrayN<uint16_t, 4>;

template class GenericCoordBase<ArrayN<float, 3>>;
template class GenericCoordBase<ArrayN<double, 3>>;

template auto cross<float>( FArray3 const &, FArray3 const & ) -> FArray3;
template auto cross<double>( DArray3 const &, DArray3 const & ) -> DArray3;

template auto operator+( float const &, FArray3 const & ) -> FArray3;
template auto operator-( float const &, FArray3 const & ) -> FArray3;
template auto operator*( float const &, FArray3 const & ) -> FArray3;
template auto operator/( float const &, FArray3 const & ) -> FArray3;

template auto operator+( double const &, DArray3 const & ) -> DArray3;
template auto operator-( double const &, DArray3 const & ) -> DArray3;
template auto operator*( double const &, DArray3 const & ) -> DArray3;
template auto operator/( double const &, DArray3 const & ) -> DArray3;

// 4D Space
// template class ArrayN<rt_float, 4>;
// template class SpatialVector<rt_float, 4>;
// template class CoordBase<SpatialVector<rt_float, 4>>;

} // namespace linalg

auto
operator<<( std::ostream & out, linalg::Vector const & vec ) -> std::ostream &
{
  out << vec.repr();
  return out;
}
