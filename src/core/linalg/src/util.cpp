#include "khonsu/linalg/util.hpp"

#include <random>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/types.hpp"

namespace linalg::util {

thread_local std::uint64_t SEED = std::random_device()();
thread_local DefaultPrng GENERATOR{ SEED };

XorShift::XorShift( StateType seed ) : _state( seed ) {}

auto
XorShift::next_state() -> StateType
{
  // next_state ^= next_state << 13;
  // next_state ^= next_state >> 7;
  // next_state ^= next_state << 17;
  this->_state ^= this->_state >> 12;
  this->_state ^= this->_state << 25;
  this->_state ^= this->_state >> 27;
  this->_state *= 0x2545F4914F6CDD1DULL;
  return this->_state;
}

// Generate a random float in the range [0, 1)
auto
XorShift::next_value() -> FloatType
{
  return static_cast<FloatType>( this->next_state() ) * this->_divisor;
}

SplitMix::SplitMix( StateType seed ) : _state( seed ) {}

auto
SplitMix::next_state() -> StateType
{
  this->_state += 0x9E3779B97F4A7C15ULL;
  this->_state = ( this->_state ^ ( this->_state >> 30 ) ) * 0xBF58476D1CE4E5B9ULL;
  this->_state = ( this->_state ^ ( this->_state >> 27 ) ) * 0x94D049BB133111EBULL;
  return this->_state ^ ( this->_state >> 31 );
}

// Generate a random float in the range [0, 1)
auto
SplitMix::next_value() -> FloatType
{
  return static_cast<FloatType>( this->next_state() ) * this->_divisor;
}

auto
MersenneTwister::next_state() -> StateType
{
  return std::mt19937_64::operator()();
}

auto
MersenneTwister::next_value() -> FloatType
{
  return static_cast<FloatType>( this->next_state() ) * this->_divisor;
}


template<class rnd_engine>
StochasicSampler<rnd_engine>::StochasicSampler() = default;

template<class rnd_engine>
auto
StochasicSampler<rnd_engine>::get_random_value() -> FloatType
{
  return GENERATOR.next_value();
};

template<class rnd_engine>
auto
StochasicSampler<rnd_engine>::get_random_dir() -> Vector
{
  Vector ret{ { GENERATOR.next_value(), GENERATOR.next_value(), GENERATOR.next_value() } };
  ret.normalize();
  return ret;
};

template<class rnd_engine>
auto
StochasicSampler<rnd_engine>::get_random_value( FloatType lower, FloatType upper ) -> FloatType
{
  auto rnd_val = GENERATOR.next_value();
  return ( ( 1 - rnd_val ) * lower ) + ( upper * rnd_val );
};

template class StochasicSampler<MersenneTwister>;
template class StochasicSampler<XorShift>;
template class StochasicSampler<SplitMix>;

} // namespace linalg::util
