#include "khonsu/linalg/ray.hpp"

#include "khonsu/linalg/linalg.hpp"

namespace linalg {

Ray::Ray( Vector pos, Vector dir ) : _pos( pos ), _dir( dir )
{
  // TODO(#23) Make the Normal a type
  _dir.normalize();
}

auto
Ray::dir() const -> Vector const &

{
  return this->_dir;
}

auto
Ray::pos() const -> Vector const &
{
  return this->_pos;
}

void
Ray::set_image_pos( std::size_t row, std::size_t col )
{
  this->_image_col = col;
  this->_image_row = row;
}

auto
Ray::image_row() const -> std::size_t
{
  return static_cast<std::size_t>( this->_image_row );
}

auto
Ray::image_col() const -> std::size_t
{
  return static_cast<std::size_t>( this->_image_col );
}

auto
Ray::ref_point() const -> linalg::Vector
{
  return this->_points_at;
}

auto
Ray::color() const -> material::Color
{
  return this->_color;
}

void
Ray::add_color( material::Color const & color )
{
  this->_color += color;
}

void
Ray::reset_color()
{
  this->_color.fill( 0.0 );
}

void
Ray::move( Vector pos, Vector dir )
{
  this->_pos = pos;
  dir.normalize();
  this->_dir = dir;
}

auto
Ray::at_distance( FloatType dist ) const -> Vector
{
  return this->_pos + ( dist * this->_dir );
}

void
Ray::move( Vector pos )
{
  this->_pos = pos;
}

void
Ray::point_at( Vector to_pos )
{
  this->_points_at = to_pos;
  this->_dir = to_pos - this->_pos;
  this->_dir.normalize();
}


} // namespace linalg
