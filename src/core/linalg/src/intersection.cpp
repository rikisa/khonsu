#include "khonsu/linalg/intersection.hpp"

#include "khonsu/linalg/linalg.hpp"

namespace linalg {


void
Intersection::set_at( Ray const & ray, FloatType at_t )
{
  this->_at_t = at_t;
  this->_at_point = ray.pos() + ( at_t * ray.dir() );
}

void
Intersection::set_normal( Vector && norm )
{
  this->_with_normal = std::move( norm );
}

auto
Intersection::at_t() const -> FloatType
{
  return this->_at_t;
}

auto
Intersection::at_point() const -> Vector const &
{
  return this->_at_point;
}

auto
Intersection::with_normal() const -> Vector const &
{
  return this->_with_normal;
}

auto
Intersection::with_material() const -> Material const &
{
  return *this->_with_material;
}

void
Intersection::set_material( Material const * material )
{
  this->_with_material = material;
}

} // namespace linalg
