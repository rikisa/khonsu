#include "khonsu/linalg/material/color.hpp"

namespace material {

template<SupportedColor colorinfo>
auto
GenericColor<colorinfo>::blend( GenericColor<colorinfo> const & other ) -> GenericColor<colorinfo>
{
  return ( *this ) + other;
}

template<SupportedColor colorinfo>
GenericColor<colorinfo>::GenericColor( array_type const & arr ) : array_type( arr )
{
}

template<SupportedColor colorinfo>
GenericColor<colorinfo>::GenericColor( array_type && arr ) : array_type( std::move( arr ) )
{
}

template<SupportedColor colorinfo>
auto
GenericColor<colorinfo>::from_vector( linalg::Vector vec ) -> GenericColor<colorinfo>
{
  return GenericColor<colorinfo>(
      { static_cast<colorinfo::channel_type>( vec[ 0 ] ), static_cast<colorinfo::channel_type>( vec[ 1 ] ),
        static_cast<colorinfo::channel_type>( vec[ 2 ] ) }
  );
}

template class GenericColor<RgbFloat32bpc>;
template class GenericColor<RgbUint16bpc>;

} // namespace material
