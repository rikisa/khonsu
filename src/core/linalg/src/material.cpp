#include "khonsu/linalg/material.hpp"

#include <algorithm>
#include <cmath>
#include <span>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/linalg/util.hpp"

using linalg::FloatType;
using linalg::Ray;
using linalg::Vector;
using Sampler = linalg::util::StochasicSampler<>;

thread_local Sampler sampler{};

namespace material {

void
reflect( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal, FloatType fuzz )
{
  auto new_dir = ray.dir() - ( 2.0 * ( ( ray.dir().dot( scatter_normal ) ) * scatter_normal ) );

  if ( fuzz != 0.0 ) {
    ray.move( scatter_pos, new_dir + sampler.get_random_dir() * fuzz );
  } else {
    ray.move( scatter_pos, new_dir );
  }
}

void
refract(
    Ray & ray,
    Vector const & scatter_pos,
    Vector const & scatter_normal,
    FloatType fuzz,
    FloatType rel_refract,
    FloatType cos_theta
)
{
  auto r_out_perp = rel_refract * ( ray.dir() + cos_theta * scatter_normal );
  auto r_out_parallel = -std::sqrt( std::abs( 1.0 - r_out_perp.sum_of_squares() ) ) * scatter_normal;

  if ( fuzz != 0.0 ) {
    ray.move( scatter_pos, r_out_perp + r_out_parallel + sampler.get_random_dir() * fuzz );
  } else {
    ray.move( scatter_pos, r_out_perp + r_out_parallel );
  }
}

auto
schlick_reflect_approx( FloatType cosine, FloatType refraction_index ) -> FloatType
{
  FloatType constexpr to_power = 5.0;

  auto rcoef_0 = ( 1 - refraction_index ) / ( 1 + refraction_index );
  rcoef_0 = rcoef_0 * rcoef_0;
  return rcoef_0 + ( 1 - rcoef_0 ) * std::pow( ( 1 - cosine ), to_power );
}

void
refract( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal, FloatType fuzz, FloatType rel_refract )
{
  auto cos_theta = std::min( ( -1.0 * ray.dir() ).dot( scatter_normal ), 1.0 );
  refract( ray, scatter_pos, scatter_normal, fuzz, rel_refract, cos_theta );
}

BaseMaterial::BaseMaterial() = default;
BaseMaterial::~BaseMaterial() = default;

BaseMaterial::BaseMaterial( Color color ) : _color( color ) {}

void
BaseMaterial::set_color( Color color )
{
  this->_color = color;
}

[[nodiscard]] auto
BaseMaterial::get_color() const -> Color
{
  return this->_color;
}

void
BaseMaterial::scatter_ray( Ray & /*ray*/, Vector const & /*scatter_pos*/, Vector const & /*scatter_normal*/ ) const {};

void
DiffuseUniform::scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const
{
  auto new_dir = sampler.get_random_dir();
  if ( scatter_normal.dot( new_dir ) < 0 ) {
    ray.move( scatter_pos, new_dir * -1 );
  } else {
    ray.move( scatter_pos, new_dir );
  }
}

void
DiffuseLambertian::scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const
{
  auto new_dir = ( sampler.get_random_dir() + scatter_normal );

  // NOTE Move into ArrayN member function?
  auto near_zero =
      std::ranges::all_of( std::span( new_dir ), []( auto elem ) { return std::abs( elem ) <= linalg::eps; } );

  if ( near_zero ) {
    ray.move( scatter_pos, scatter_normal );
  } else {
    ray.move( scatter_pos, new_dir );
  }
}

Metal::Metal() = default;

Metal::Metal( Color color, FloatType fuzz ) : BaseMaterial( std::move( color ) ), _fuzz( fuzz ) {}

void
Metal::scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const
{
  reflect( ray, scatter_pos, scatter_normal, this->_fuzz );

  // auto project = ray.dir().dot( scatter_normal ) * scatter_normal;
  // auto project2 =
  //     std::span( project.begin(), project.end() ) | std::views::transform( []( auto elem ) { return elem * 2; } );

  // Vector new_dir{};
  // // reflection
  // std::ranges::transform( std::span( ray.dir().cbegin(), ray.dir().cend() ), project2, new_dir.begin(), std::plus{}
  // );

  // // fuzz
  // auto rnd = get_random_dir();
  // auto rnd_view = std::span( rnd.begin(), rnd.end() ) |
  //     std::views::transform( [ this ]( auto elem ) { return elem * this->_fuzz; } );
  // std::ranges::transform( new_dir, rnd_view, new_dir.begin(), std::plus{} );
}

Dielectric::Dielectric() = default;

Dielectric::Dielectric( Color color, FloatType fuzz, FloatType refractive ) :
    BaseMaterial( std::move( color ) ), _fuzz( fuzz ), _refractive( refractive )
{
}

void
Dielectric::scatter_ray( Ray & ray, Vector const & scatter_pos, Vector const & scatter_normal ) const
{
  // TODO(#34) refactor this into ray or intersect
  bool is_internal = ray.dir().dot( scatter_normal ) < 0;
  auto with_normal = is_internal ? scatter_normal : scatter_normal * -1.0;

  auto cos_theta = std::min( ( -1.0 * ray.dir() ).dot( with_normal ), 1.0 );
  auto sin_theta = std::sqrt( 1.0 - cos_theta * cos_theta );

  auto rel_refract = is_internal ? 1 / this->_refractive : this->_refractive;

  // TODO(#45) See whats wrong
  if ( rel_refract * sin_theta > 1.0 ||
       schlick_reflect_approx( cos_theta, rel_refract ) > sampler.get_random_value() ) {
    reflect( ray, scatter_pos, with_normal, this->_fuzz );
  } else {
    refract( ray, scatter_pos, with_normal, this->_fuzz, rel_refract, cos_theta );
  }
}

} // namespace material
