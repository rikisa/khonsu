#include "khonsu/rendering/scene.hpp"

#include <memory>

#include "khonsu/linalg/intersection.hpp" // IWYU pragma: keep
#include "khonsu/linalg/material.hpp"     // IWYU pragma: keep
#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/ray.hpp" // IWYU pragma: keep
#include "khonsu/linalg/types.hpp"
#include "khonsu/objects/objects.hpp"

namespace rendering {

void
Scene::add_object( objects::ObjectRef object )
{
  this->_scene_objects.emplace_back( std::move( object ) );
}

auto
Scene::intersect( Ray & ray, Intersection & intersect ) -> bool
{
  using linalg::FloatType;
  using linalg::infinity;
  using material::Color;

  FloatType max_dist = infinity;
  objects::BaseObject * nearest_obj = nullptr;

  for ( auto & obj : this->_scene_objects ) {
    if ( obj->get_intersect( ray, intersect, linalg::min_dist, max_dist ) ) {
      max_dist = intersect.at_t();
      nearest_obj = obj.get();
    }
  }
  return nearest_obj != nullptr;
}

} // namespace rendering
