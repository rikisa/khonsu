#include "khonsu/rendering/basecamera.hpp"

#include <cmath>
#include <stdexcept>

#include "khonsu/linalg/linalg.hpp"

namespace rendering {

BaseCamera::BaseCamera() { this->_origin.flip_ydir(); }

auto
BaseCamera::_get_step_row() -> Vector const &
{
  return this->_step_row;
}

auto
BaseCamera::_get_step_col() -> Vector const &
{
  return this->_step_col;
}

auto
BaseCamera::_get_image_ori_px_offset() -> Vector const &
{
  return this->_image_ori_px_offset;
}

void
BaseCamera::update_ray( linalg::Ray & ray, std::size_t index )
{
  auto new_row = index / this->_pixel_cols;
  auto new_col = index % this->_pixel_cols;
  ray.set_image_pos( new_row, new_col );

  // NOTE(Optimize)
  ray.move( this->_eye );
  ray.point_at(
      this->_image_ori_px_offset + ( static_cast<FloatType>( new_row ) * this->_step_row ) +
      ( static_cast<FloatType>( new_col ) * this->_step_col )
  );
}

void
BaseCamera::set_shape( std::size_t px_rows, std::size_t px_cols, FloatType fov )
{
  if ( px_rows <= 0 || px_cols <= 0 ) {
    throw std::invalid_argument( "Height and width must be larger than 0" );
  }

  // Set pixel grid information
  this->_pixel_rows = px_rows;
  this->_pixel_cols = px_cols;
  this->_size = px_rows * px_cols;
  // As float as well
  this->_rows_f = static_cast<FloatType>( this->_pixel_rows );
  this->_cols_f = static_cast<FloatType>( this->_pixel_cols );

  // set width = 1 and height to aspect
  auto height = static_cast<FloatType>( px_rows );
  auto width = static_cast<FloatType>( px_cols );
  this->_aspect = width / height;

  this->_height = 1.0;
  this->_width = this->_aspect;

  // store fov in rad, calculate eye pos
  this->_fov = fov;
  this->_eye_dist = std::tan( fov * 0.5 ) * 2;
  this->_eye = this->pos() - ( this->viewdir() * this->_eye_dist );

  auto img_col_offset = this->width() * 0.5;
  auto img_row_offset = this->height() * 0.5;

  this->_image_ori = this->pos();
  this->_image_ori -= this->row_dir() * img_row_offset;
  this->_image_ori -= this->col_dir() * img_col_offset;

  this->_px_width = this->_width / static_cast<FloatType>( px_cols );
  this->_px_height = this->_height / static_cast<FloatType>( px_rows );

  // NOTE Might not be needed in other cameras
  this->_step_col = this->col_dir() * this->_px_width;
  this->_step_row = this->row_dir() * this->_px_height;
  this->_image_ori_px_offset = this->_image_ori + ( this->_step_col * 0.5 ) + ( this->_step_row * 0.5 );
}

auto
BaseCamera::size() const -> std::size_t
{
  return this->_size;
}

auto
BaseCamera::get_rays_per_pixel() const -> std::size_t
{
  return 1;
}

auto
BaseCamera::rows() const -> std::size_t
{
  return this->_pixel_rows;
}

auto
BaseCamera::cols() const -> std::size_t
{
  return this->_pixel_cols;
}

auto
BaseCamera::width() const -> FloatType
{
  return this->_aspect;
}

auto
BaseCamera::height() const -> FloatType
{
  return this->_height;
}

auto
BaseCamera::px_width() const -> FloatType
{
  return this->_px_width;
}

auto
BaseCamera::px_height() const -> FloatType
{
  return this->_px_height;
}

// auto
// BaseCamera::begin() const -> std::size_t
// {
//   return 0;
// }
//
// auto
// BaseCamera::end() const -> std::size_t
// {
//   return this->_size + 1;
// }

auto
BaseCamera::pos() const -> linalg::Vector const &

{
  return this->_origin.pos();
}

auto
BaseCamera::eye() const -> linalg::Vector const &

{
  return this->_eye;
}

auto
BaseCamera::image_origin() const -> linalg::Vector const &
{
  return this->_image_ori;
}

auto
BaseCamera::viewdir() const -> linalg::Vector const &

{
  return this->_origin.zdir();
}

auto
BaseCamera::row_dir() const -> linalg::Vector const &

{
  return this->_origin.ydir();
}

auto
BaseCamera::col_dir() const -> linalg::Vector const &

{
  return this->_origin.xdir();
}


} // namespace rendering
