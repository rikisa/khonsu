#include "khonsu/rendering/stochasticcamera.hpp"

namespace rendering {

StochasticCamera::StochasticCamera( std::size_t sample_size ) : _sample_size( sample_size ) {}

void
StochasticCamera::update_ray( Ray & ray, std::size_t index )
{
  index = index % BaseCamera::size();
  auto new_row = index / this->cols();
  auto new_col = index % this->cols();
  ray.set_image_pos( new_row, new_col );

  // NOTE(Optimize)
  ray.move( this->eye() );

  auto row_vec = ( static_cast<FloatType>( new_row ) * this->_get_step_row() ) +
      ( this->_sampler.get_random_value( -0.5, 0.5 ) * this->_get_step_row() );
  auto row_col = ( static_cast<FloatType>( new_col ) * this->_get_step_col() ) +
      ( this->_sampler.get_random_value( -0.5, 0.5 ) * this->_get_step_col() );
  ray.point_at( this->_get_image_ori_px_offset() + row_vec + row_col );
}

auto
StochasticCamera::size() const -> std::size_t
{
  return BaseCamera::size() * this->_sample_size;
}

auto
StochasticCamera::get_rays_per_pixel() const -> std::size_t
{
  return this->_sample_size;
}

} // namespace rendering
