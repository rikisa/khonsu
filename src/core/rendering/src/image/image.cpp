#include "khonsu/rendering/image.hpp"

#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/material/colorinfo.hpp"

namespace rendering {

using material::Color;
using material::SupportedColor;

template<SupportedColor color_info>
void
GenericImage<color_info>::set_color( std::size_t row, std::size_t col, Color color )
{
  for ( std::size_t channel = 0; channel < Color::dimension; ++channel ) {
    this->get_value( row, col, channel ) = color[ channel ];
  }
}

template<SupportedColor color_info>
void
GenericImage<color_info>::add_color( std::size_t row, std::size_t col, Color color )
{
  for ( std::size_t channel = 0; channel < Color::dimension; ++channel ) {
    this->get_value( row, col, channel ) += color[ channel ];
  }
}

template<SupportedColor color_info>
GenericImage<color_info>::GenericImage( std::size_t height, std::size_t width ) : _height( height ), _width( width )
{
  this->_values.resize( height * width * color_info::channel_num );
};

template<SupportedColor color>
void
GenericImage<color>::set_value( std::size_t row, std::size_t col, color::index_type channel, color::storage_type value )
{
  this->get_value( row, col, channel ) = value;
}

template<SupportedColor color>
auto
GenericImage<color>::get_value( std::size_t row, std::size_t col, color::index_type channel ) -> color::storage_type &
{
  return this->_values.at( ( this->_width * row + col ) * color::channel_num + channel );
};

// template<SupportedColor color>
// auto
// GenericImage<color>::get_color_ref( std::size_t row, std::size_t col ) -> Pixel &
// {
//   auto begin = this->_values.begin() + ( ( this->_width * row + col ) * color::channel_num );
//   static Pixel pixel{ begin, begin + color::channel_num };
//   return pixel;
// };

template<SupportedColor color>
auto
GenericImage<color>::data() -> decltype( std::declval<Container>().data() )
{
  return this->_values.data();
}

template<SupportedColor color>
auto
GenericImage<color>::container() -> Container &
{
  return this->_values;
}

template<SupportedColor color>
auto
GenericImage<color>::begin() -> pixel_iterator
{
  return { this->_values.begin(), this->_values.end() };
}

template<SupportedColor color>
auto
GenericImage<color>::end() -> pixel_iterator
{
  return { this->_values.end(), this->_values.end() };
}

template<SupportedColor color>
auto
GenericImage<color>::height() const -> std::size_t
{
  return this->_height;
}

template<SupportedColor color>
auto
GenericImage<color>::width() const -> std::size_t
{
  return this->_width;
}

template class GenericImage<material::RgbUint16bpc>;
template class GenericImage<material::RgbFloat32bpc>;

} // namespace rendering
