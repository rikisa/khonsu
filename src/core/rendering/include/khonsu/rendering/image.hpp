#pragma once

#include <cstddef>
#include <memory>
#include <utility>
#include <vector>

#include "khonsu/linalg/material/color.hpp"

namespace rendering {

template<material::SupportedColor color_info>
class GenericImage {
  public:
    using Container = std::vector<typename color_info::storage_type>;
    using ColorInfo = color_info;
    using Pixel = std::ranges::subrange<typename Container::iterator, typename Container::iterator>;
    class pixel_iterator;

    GenericImage( std::size_t height, std::size_t width );

    void set_color( std::size_t row, std::size_t col, material::Color color );
    void add_color( std::size_t row, std::size_t col, material::Color color );

    void set_value( std::size_t row, std::size_t col, color_info::index_type channel, color_info::storage_type value );
    auto get_value( std::size_t row, std::size_t col, color_info::index_type channel ) -> color_info::storage_type &;

    // auto get_color_ref( std::size_t row, std::size_t col) -> Pixel &;

    auto data() -> decltype( std::declval<Container>().data() );
    auto container() -> Container &;

    auto begin() -> pixel_iterator;
    auto end() -> pixel_iterator;


    [[nodiscard]] auto height() const -> std::size_t;
    [[nodiscard]] auto width() const -> std::size_t;

  private:
    Container _values{};

    std::size_t _height;
    std::size_t _width;
};

template<material::SupportedColor color_info>
class GenericImage<color_info>::pixel_iterator {
  public:
    using Pixel = GenericImage<color_info>::Pixel;
    using internal_iterator = GenericImage<color_info>::Container::iterator;

    auto operator++() -> GenericImage<color_info>::pixel_iterator &
    {
      this->_begin += color_info::channel_num;
      this->_current = { this->_begin, this->_begin + color_info::channel_num };
      return *this;
    }

    auto operator*() -> Pixel & { return _current; };

    auto operator->() -> Pixel * { return &_current; };

    auto operator!=( GenericImage<color_info>::pixel_iterator const & other ) -> bool
    {
      return this->_begin != other._end;
    }

  private:
    pixel_iterator( internal_iterator begin, internal_iterator end ) :
        _begin( begin ), _end( end ), _current( begin, begin + color_info::channel_num ){};

    internal_iterator _begin;
    internal_iterator _end;
    Pixel _current;

    friend class GenericImage<color_info>;
};

// Default Image
using Image = GenericImage<material::RgbFloat32bpc>;
using ImageRef = std::shared_ptr<Image>;

} // namespace rendering
