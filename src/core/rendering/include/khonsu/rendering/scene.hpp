#pragma once

#include <memory>
#include <vector>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/objects/objects.hpp"

namespace rendering {

class Scene {
  protected:
    using Ray = linalg::Ray;
    using Intersection = linalg::Intersection;
    using BaseMaterial = material::BaseMaterial;

  public:
    // TODO(#13) test a associative array of vectors with any oject fullfilling a contract, maybe wrapped by an object
    // wrapper that is known at the compilation unit
    void add_object( objects::ObjectRef );

    auto intersect( Ray & ray, Intersection & intersect ) -> bool;

  private:
    std::vector<objects::ObjectRef> _scene_objects;
};

using SceneRef = std::shared_ptr<Scene>;

} // namespace rendering
