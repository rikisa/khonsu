#pragma once

#include <cstddef>
#include <memory>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"

namespace rendering {

class BaseCamera {
  public:
    using Vector = linalg::Vector;
    using Origin = linalg::CoordBase;
    using FloatType = linalg::FloatType;

    BaseCamera();
    virtual ~BaseCamera() = default;

    /*!
    @brief Sets the canvas size in pixels.

    @param height Canvas/Image height in pixel
    @param width Canvas/Image width in pixel
    @param fov Field of view \f$\theta\f$
    */
    virtual void set_shape( std::size_t px_rows, std::size_t px_cols, FloatType fov );

    /*!
    @brief Set dir to the next dir.

    @param[in,out] ray Referene to ray to be updated
    @param[in] index Cameras specific ray index, as provided by index_iterator
    */
    virtual void update_ray( linalg::Ray & ray, std::size_t index );

    /*!
    @brief Rays generated for each image pixel
    */
    [[nodiscard]] virtual auto get_rays_per_pixel() const -> std::size_t;

    /*!
    @brief Number of updates the camera performs
    */
    [[nodiscard]] virtual auto size() const -> std::size_t;

    [[nodiscard]] auto rows() const -> std::size_t;
    [[nodiscard]] auto cols() const -> std::size_t;

    [[nodiscard]] auto width() const -> FloatType;
    [[nodiscard]] auto height() const -> FloatType;
    [[nodiscard]] auto px_width() const -> FloatType;
    [[nodiscard]] auto px_height() const -> FloatType;

    [[nodiscard]] auto pos() const -> Vector const &;
    [[nodiscard]] auto eye() const -> Vector const &;
    [[nodiscard]] auto image_origin() const -> Vector const &;
    [[nodiscard]] auto viewdir() const -> Vector const &;
    [[nodiscard]] auto row_dir() const -> Vector const &;
    [[nodiscard]] auto col_dir() const -> Vector const &;

  protected:
    auto _get_step_row() -> Vector const &;
    auto _get_step_col() -> Vector const &;
    auto _get_image_ori_px_offset() -> Vector const &;

  private:
    std::size_t _pixel_cols = 0;
    std::size_t _pixel_rows = 0;
    std::size_t _size = 0;
    // Float casted values of _pixel_cols adn _pixel_rows
    FloatType _cols_f = 0.0;
    FloatType _rows_f = 0.0;

    FloatType _width = 0;
    FloatType _height = 0;
    FloatType _px_width = 0;
    FloatType _px_height = 0;
    // width to height == _height
    FloatType _aspect = 0;

    Origin _origin{ Vector{ { 0, 0, 0 } } };

    Vector _eye{ 0 };
    FloatType _fov = 0;
    FloatType _eye_dist = 1;

    Vector _image_ori{ 0 };
    Vector _dir_row{ 0 };
    Vector _dir_col{ 0 };
    // NOTE Might not be needed in other cameras
    Vector _step_row{ 0 };
    Vector _step_col{ 0 };
    Vector _image_ori_px_offset{ 0 };
};

using BaseCameraRef = std::shared_ptr<BaseCamera>;


} // namespace rendering
