#pragma once

#include <cstddef>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/util.hpp"
#include "khonsu/rendering/basecamera.hpp"

namespace rendering {

class StochasticCamera : public BaseCamera {
  public:
    using Vector = linalg::Vector;
    using Origin = linalg::CoordBase;
    using FloatType = linalg::FloatType;
    using Ray = linalg::Ray;

    /*!
    @brief Stochastic camera that samples multiple rays per pixel

    @param[in] sample_size Number of rays per pixel
    */
    explicit StochasticCamera( std::size_t sample_size = 4 );
    ~StochasticCamera() override = default;

    void update_ray( Ray & ray, std::size_t index ) override;


    [[nodiscard]] auto size() const -> std::size_t override;

    [[nodiscard]] auto get_rays_per_pixel() const -> std::size_t override;

  private:
    std::size_t _sample_size;
    linalg::util::StochasicSampler<> _sampler;
};

using StochasticCameraRef = std::shared_ptr<StochasticCamera>;


} // namespace rendering
