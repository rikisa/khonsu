#include "khonsu/objects/objects.hpp"

#include <cmath>
#include <format>
#include <memory>

#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/types.hpp"

namespace objects {

BaseObject::BaseObject( CoordBase origin, std::shared_ptr<BaseMaterial> material ) :
    _origin( origin ), _material( std::move( material ) )
{
}

void
BaseObject::set_origin( linalg::CoordBase origin )
{
  this->_origin = origin;
}

void
BaseObject::set_material( std::shared_ptr<BaseMaterial> material )
{
  this->_material = std::move( material );
}

auto
BaseObject::get_origin() const -> linalg::CoordBase
{
  return this->_origin;
}

auto
BaseObject::get_material() const -> BaseMaterial const *
{
  return this->_material.get();
}

auto
BaseObject::_get_material() -> BaseMaterial *
{
  return this->_material.get();
}

Sphere::Sphere( linalg::CoordBase origin, std::shared_ptr<BaseMaterial> material, linalg::FloatType radius ) :
    BaseObject( origin, std::move( material ) ), _radius( radius ), _radius_squared( radius * radius )
{
}

auto
Sphere::radius() const -> linalg::FloatType
{
  return this->_radius;
}

auto
Sphere::radius_squared() const -> linalg::FloatType
{
  return this->_radius_squared;
}

auto
// TODO(rikisa) Separate concerns of functions:
// Objects must not have same interface as Sphere
// intersect() only intersects, color_at() gets the color
Sphere::get_intersect( Ray & ray, Intersection & intersect, FloatType min_dist, FloatType max_dist ) -> bool
{
  // NOLINTBEGIN(*-length,*-magic-numbers)
  auto ray_to_sphere = this->get_origin().pos() - ray.pos();

  // TODO(#23) profile simplifications
  //// Optimized
  auto ray_to_sphere_norm = ray_to_sphere.norm();
  auto h = ray.dir().dot( ray_to_sphere );
  auto c = ray_to_sphere_norm * ray_to_sphere_norm - this->_radius_squared;
  auto discriminant = h * h - c;

  //// Unoptimized
  // auto a = ray.dir().dot( ray.dir() );
  // auto b = ( -2.0 * ray.dir() ).dot( ray_to_sphere );
  // auto c = ray_to_sphere.dot( ray_to_sphere ) - this->_radius_squared;
  // auto discriminant = ( b * b ) - ( 4 * a * c );

  // std::cout << "# " << ray.pos() << " + t * " << ray.dir() << " : ";
  if ( discriminant < 0 ) {
    // std::cout << "no intersect: " << discriminant << "< 0\n";
    return false;
  }

  auto root = std::sqrt( discriminant );
  auto maybe_t = h - root;
  if ( maybe_t < min_dist || maybe_t > max_dist ) {
    maybe_t = h + root;
    if ( maybe_t < min_dist || maybe_t > max_dist ) {
      // std::cout << std::format( "no intersect: {} < {} || {} > {}\n", maybe_t, min_dist, maybe_t, max_dist );
      return false;
    }
  }

  intersect.set_at( ray, maybe_t );
  intersect.set_normal( ( intersect.at_point() - this->get_origin().pos() ) / this->_radius );
  intersect.set_material( this->get_material() );

  // std::cout << "intersects: t = " << maybe_t << " -> " << intersect.at_point() << "\n";

  return true;

  // NOLINTEND(*-length,*-magic-numbers)
}

DebugSphere::DebugSphere( CoordBase origin, std::shared_ptr<BaseMaterial> material, FloatType radius, ColorMode mode ) :
    Sphere( origin, std::move( material ), radius ), _color_mode( mode )
{
}

void
DebugSphere::_set_checkboard_color( Intersection & intersect )
{
  using material::BaseMaterial;
  using material::Color;

  auto constexpr row_count = 20;
  auto constexpr col_count = 20;

  Color color_a{ { 0.1, 0.3, 0.5 } };
  Color color_b{ { 0.5, 0.5, 0.2 } };

  auto ref_point = intersect.at_point() - this->get_origin().pos();
  auto p_y = ref_point[ 0 ];
  auto p_x = ref_point[ 1 ];
  auto p_z = ref_point[ 2 ];
  auto theta = std::atan2( p_x, p_z );
  auto phi = std::acos( p_y / this->radius() );

  auto u_coord = 1 - ( theta / ( std::numbers::pi * 2 ) + 0.5 );
  auto v_coord = 1 - ( phi / std::numbers::pi );

  auto row = static_cast<int>( std::floor( u_coord * row_count ) );
  auto col = static_cast<int>( std::floor( v_coord * col_count ) );
  if ( ( row + col ) % 2 == 1 ) {
    this->_get_material()->set_color( color_a );
  } else {
    this->_get_material()->set_color( color_b );
  }
}

auto
DebugSphere::get_intersect( Ray & ray, Intersection & intersect, FloatType min_dist, FloatType max_dist ) -> bool
{
  using material::Color;

  // NOLINTBEGIN(*-length,*-magic-numbers)
  auto hit = Sphere::get_intersect( ray, intersect, min_dist, max_dist );

  if ( !hit ) {
    return false;
  }

  if ( this->_color_mode == ColorMode::Normals ) {
    auto normal_color = ( intersect.with_normal() + 1 ) * 0.5;

    this->_get_material()->set_color( Color::from_vector( normal_color ) );

  } else {
    auto center_dist = ( this->get_origin().pos() - ray.pos() ).norm();
    auto min_dist = center_dist - this->radius();
    auto max_dist = sqrt( center_dist * center_dist + this->radius_squared() );

    auto dist = intersect.at_t() - min_dist;
    auto ref_dist = max_dist - min_dist;

    auto rel_dist = dist / ref_dist;
    auto rel_dist_inv = 1 - rel_dist;

    switch ( this->_color_mode ) {
    case ColorMode::Distance:
      this->_get_material()->set_color( Color{ static_cast<Color::elem_type>( rel_dist ) } );
      break;
    case ColorMode::DistanceInverse:
      this->_get_material()->set_color( Color{ static_cast<Color::elem_type>( 1 - rel_dist ) } );
      break;
    case ColorMode::DistanceMix:
      this->_get_material()->set_color( Color(
          { static_cast<Color::elem_type>( rel_dist_inv * rel_dist_inv ), static_cast<Color::elem_type>( rel_dist ),
            static_cast<Color::elem_type>( ( rel_dist - ( rel_dist * rel_dist ) ) * 2 ) }
      ) );
      break;
    case ColorMode::CheckBoard:
      this->_set_checkboard_color( intersect );
    default:
      break;
    }
  }

  return true;
  // NOLINTEND(*-length,*-magic-numbers)
}


}; // namespace objects
