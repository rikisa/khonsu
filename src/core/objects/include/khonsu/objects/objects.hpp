#pragma once

#include <memory>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"

namespace objects {

class BaseObject {
  protected:
    using FloatType = linalg::FloatType;
    using CoordBase = linalg::CoordBase;
    using Vector = linalg::Vector;
    using Ray = linalg::Ray;
    using Intersection = linalg::Intersection;
    using BaseMaterial = material::BaseMaterial;

  public:
    BaseObject() = default;
    BaseObject( CoordBase origin, std::shared_ptr<BaseMaterial> material );

    virtual ~BaseObject() = default;
    /*!
    @brief Set the origin of the base object in world units.

    @param origin Origin as coordinate base
    */
    void set_origin( CoordBase origin );
    void set_material( std::shared_ptr<BaseMaterial> material );

    [[nodiscard]] auto get_origin() const -> CoordBase;
    [[nodiscard]] auto get_material() const -> BaseMaterial const *;

    virtual auto get_intersect( Ray & ray, Intersection & intersect, FloatType min_dist, FloatType max_dist )
        -> bool = 0;

  protected:
    auto _get_material() -> BaseMaterial *;

  private:
    CoordBase _origin{ Vector{ 0 } };
    std::shared_ptr<BaseMaterial> _material = std::make_shared<BaseMaterial>();
};

class Sphere : public BaseObject {
  public:
    Sphere() = default;
    Sphere( CoordBase origin, std::shared_ptr<BaseMaterial> material, FloatType radius );

    auto get_intersect( Ray & ray, Intersection & intersect, FloatType min_dist, FloatType max_dist ) -> bool override;

    [[nodiscard]] auto radius() const -> FloatType;
    [[nodiscard]] auto radius_squared() const -> FloatType;

  private:
    FloatType _radius = 1.0;
    FloatType _radius_squared = 1.0;
};

class DebugSphere : public Sphere {
  public:
    enum class ColorMode { Normals, Distance, DistanceInverse, DistanceMix, CheckBoard };

    DebugSphere( CoordBase origin, std::shared_ptr<BaseMaterial> material, FloatType radius, ColorMode mode );

    auto get_intersect( Ray & ray, Intersection & intersect, FloatType min_dist, FloatType max_dist ) -> bool override;

  private:
    void _set_checkboard_color( Intersection & intersect );
    ColorMode _color_mode;
};

using ObjectRef = std::shared_ptr<BaseObject>;

} // namespace objects
