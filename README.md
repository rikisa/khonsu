# Khonsu

A ray tracing engine with an embeded lua interpreter for rendereing simple 3D scenes. It is less about the actual raytracing, but testing out techniques for software desing and architecture using modern C++ 20 and CMake.

# Build Dependencies

- PNG ++
- Lua 5
- sol2
- glfw3
- fmt
- spdlog
- cxxopts
- Notcurses++

# Building

There are some CMake presets for building release, debug and coverage builds. To get a debug build:

```bash
git clone https://gitlab.com/rikisa/khonsu.git

cmake --preset  -B build-khonsu khonsu

cmake --build build-khonsu
```

# CI/CD
The python CLI script util/pipeline.py offers som wrapper/utilities building and aggregating profiling information into reports.

```bash
util/pipeline.py --help
usage: pipeline.py [-h] [-b --builddir BINDIR] [-s --sourcedir SRCDIR] {config,build,test,docs,analysis} ...

Run pipeline stages for CI/CD

options:
  -h, --help            show this help message and exit
  -b --builddir BINDIR  CMake binary dir to use (default=/home/rikisa/src/khonsu/build)
  -s --sourcedir SRCDIR
                        CMake source dir to use (default=/home/rikisa/src/khonsu)

stage:
  {config,build,test,docs,analysis}
                        Stage to perform
    config              Configure build
    build               Build targets
    test                Run tests
    docs                Generate docu
    analysis            Do variuos analysis steps
```

