# Find all the public headers

# add documentation
if(NOT DOXYGEN_FOUND)
  message(WARNING "Could not find Doxygen, omitting documentation")
  return()
endif()

if(DOXYGEN_DOT_FOUND)
  set(KHONSU_CONFIGURE_WITH_DOT "YES")
  message(DEBUG "Found dot: ${DOXYGEN_DOT_EXECUTABLE}")
  add_custom_target(
    DepGraph
    COMMAND "${CMAKE_COMMAND} --graphviz=deps.dot  ${CMAKE_SOURCE_DIR}"
  )
else()
  set(KHONSU_CONFIGURE_WITH_DOT "NO")
endif()

# Doxygen hardcoded for project
file(GLOB_RECURSE KHONSU_PUBLIC_HEADERS ${CMAKE_SOURCE_DIR}/src/*.hpp)

set(DOXYGEN_INPUT_DIR ${CMAKE_SOURCE_DIR}/src)
set(DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/doxygen)
set(DOXYGEN_INDEX_FILE ${DOXYGEN_OUTPUT_DIR}/html/index.html)
set(DOXYFILE_IN ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in)
set(DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

# Configure Doxifile
configure_file(${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY)

file(MAKE_DIRECTORY ${DOXYGEN_OUTPUT_DIR}) # Doxygen won't create this for us
add_custom_command(
  OUTPUT ${DOXYGEN_INDEX_FILE}
  DEPENDS ${KHONSU_PUBLIC_HEADERS}
  COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT "Generating api doc"
)

add_custom_target(DoxygenDocsGen DEPENDS ${DOXYGEN_INDEX_FILE})

# Doxygen to Sphinx
if(NOT Sphinx_FOUND)
  message(
    WARNING "Could not find Sphinx or Breath, omitting Sphinx/Breathe codegen"
  )
  return()
endif()

# setting some options for conf.py
find_program (plantuml_EXEC plantuml)
configure_file(conf.py.in ${CMAKE_CURRENT_BINARY_DIR}/conf.py)

set(SPHINX_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/src/sphinx)
set(SPHINX_BUILD ${CMAKE_CURRENT_BINARY_DIR}/sphinx)

set(RES_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/res)
set(RES_BUILD ${CMAKE_CURRENT_BINARY_DIR})

add_custom_target(
  CopyResources
  COMMAND ${CMAKE_COMMAND} -E copy_directory_if_different
          "${RES_SOURCE}/_static" "${RES_BUILD}/_static"
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT "Generating documentation with Sphinx using Doxygen XML output"
)

add_custom_target(
  SphinxDocsGen
  COMMAND
    ${Sphinx_EXECUTABLE} -b html -c ${CMAKE_CURRENT_BINARY_DIR}
    "$<$<NOT:$<CONFIG:Release>>:-tdev>"
    "$<$<BOOL:${KHONSU_RENDER_PUML}>:-tplantuml>"
    -Dbreathe_projects.${PROJECT_NAME}=${DOXYGEN_OUTPUT_DIR}/xml
    ${SPHINX_SOURCE} ${SPHINX_BUILD}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT "Generating documentation with Sphinx using Doxygen XML output"
)
add_dependencies(SphinxDocsGen DoxygenDocsGen)
add_dependencies(SphinxDocsGen DoxygenDocsGen)
add_dependencies(SphinxDocsGen CopyResources)
