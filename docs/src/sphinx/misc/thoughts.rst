Thoughts
========

How to make something nice, iteratively
---------------------------------------
After some time, there was some good progress – and some thoughts. First, facing uncertainty regarding the exact result, it was the right idea not to get lost in detail.
The key idea of Khonsu (see :ref:`overview <top-level-req>`) is clear: a platform designed to allow for the replacement and refactoring of any part of the renderer.
With that in mind, I started building small, independent programs focusing on use cases utilizing specific components. This helps to get an impression of the consumer-component and component-component interactions, hinting at possible oversights or over-complicated usage and helping in refactoring all components. (This is a case of sounds good on paper but really bad in practice. This kind of issue is caught super early on.)
Furthermore, it feels like more "realistic" usage than integration tests. Typically, it is not a proper use case, but the interactions between components are tested.

These independent programs make for good smoke tests as well (smoke test meaning here: some test that tests a specific happy-path *use case*). They tend to wiggle the tested/used components loose from other components, not really concerned with a specific use case, but also bring other components closer together.

|
|  *6. Jul. 2024*
 

Too much detail hurts
---------------------
While coming up with a very thought-out concept, deriving some design first and then *merely* implementing it is a nice idea: It only works well up to a certain complexity.
For once: It slows the overall time down. Given a thoroughly planned design, there are few surprises left in the implementation. However, a design can become too detailed, and the overall costs for design and implementation increase: the actual codes become just a paraphrase of the design.
Furthermore, too much design feels rigid and does not allow "finding* a suitable technical solution. Again, if the design is technical, the implementation becomes a repetition.
There is a sweet spot between laying out every detail and being abstract. For Khonsu all concepts and designs will not reflect actual interfaces but merely the general interaction between classes and components. A critical part will be concurrency [#]_ . This (and similar) should be detailed. The rest is abstract interactions, supplemented by Docstrings.

On a further note, while `Mermaid <https://mermaid.js.org>`_ is nice, but `PlantUML <https://plantuml.com>`_ feels complete. Mermaid has a nicer, fresher look. Especially the mindmap I liked over the PlantUML version. However, Mermaid needs more support for some basic features, like Generics with multiple parameters. Overall, both will be used in the documentation. Because why not ;)

|
|  *16. Aug. 2023*

.. rubric:: Footnotes

.. [#] Due to OpenGL preferring to live in the main thread and concurrently running the control thread. With both threads capable of quitting the thread.


Starting with the Basics
------------------------

Seemingly, an idea comes to mind, is deemed good, and then everything must be fast. So fast that the original idea becomes an incomprehensible shadow of its former glory.
To slow down this - seemingly inevitable - decline, I want to start with the basics. And this is having a good testing infrastructure (as in CI/CD) in place before writing any meaningful portion of code. In the optimal case, everything is then ready and somewhat stable; one does not have to bother with it anymore. It was also quite a nice but cumbersome exercise in writing a :code:`.gitlab-ci` pipeline -- how CI/CD is done in GitLab and what fun bugs come up... Looking at you `#370052 <https://gitlab.com/gitlab-org/gitlab/-/issues/370052>`_ .

Along the GitLab CI/CD, a merging strategy emerged quite naturally (when to do all the pedantic tests and analysis, when only to make sure all tests are dandy). The basic idea is that upon merging into the main, everything goes: Static analysis, dynamic analysis, all errors as warnings, and everything that might come.
Also, a build release build is triggered, which might fail in funny ways while debug is fine, to have something that runs after every merge into main. In the development branch, no such things exist.
Finally, there is a docu branch, where no tests should happen, but the documentation should be generated.
Next on the list: some design documents and a definition of _what_ should be done. No feature creep! The nice part is: my past time and interest being the only stakeholders

|
|  *09. Aug. 2023*

.. todo::

   Dynamic analysis as soon as somthing runnable exsists

.. todo::
   CMake smoke tests, as soon as something runnable exsists


Init
----

Finally, I figured out what to use this side project for. For years I was toying around with several implementations of raytracers. First in Python, then in C++, and now I want to throw in some GLSL!

I used the various iterations of this raytracer to learn a lot about linear algebra, shading programming, and software engineering in general. At the same time, most of the time, I work towards a specific goal. This puts learning and playing around with technology on a side track. But to be honest, this is part of fun recreational programming: the journey is the reward.

So this is what this project is for scratching that itch of polishing something to perfection, no matter if needed or not. To be careless and go fast where I am not interested in too much. And write about it.

With this commit, I start documenting all further steps. Here are some topics I want to hover about to play around with:

- OpenGL (I just want to keep my GPU busy, and I have done ML already)
- C++20 (it's still new, especially interested in the ranges library)
- The GitLab flavor of CI/CD (GitHub and Bitbucket are not the solutions)
- Software Modularization (in other words: will every non-trivial software design turn into a ball of mud?)
- Blogging
- Software Architecture (This one will be pure, without compromise, and in general fantastic. Promise™)
- Project Planning and Documentation (Until I am fed up with it again)
- Embedded Lua
- Writing a linalg library (and )
- Diagrams as Code (PlantUML for now, but Mermaid seems cool, too)
- CI/CD (does it have to be painful?)
- modern cmake (and treat it as a first-class citizen, aka code)

Some of these topics are new to me, and I want to learn them; others I have encountered already and want to get into it, and some I do daily. I want to 
see: will I do things differently without compelling external factors? Or will it be the same? Just for the heck of it! (I will write my results here!)

|
|  *02. Aug. 2023*
