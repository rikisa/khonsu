Snapshots
=========
Some renderings, reflecting the current state of Khonsu

Basic Raytracing with ray depth and some shading
------------------------------------------------

   .. figure:: ../../../res/img/firstpicture.png
      :alt: Hello, little guy - first image of Khonsu

      A first render with with uniform sampling for diffuse material - and some bugs


   .. figure:: ../../../res/img/iteration2.png
      :alt: Lambertian distribution

      Fixed bugs and Lambertian distribution for diffuse material

   .. figure:: ../../../res/img/reflections.png
      :alt: Metalic reflections

      Some refactor and reflective material

   .. figure:: ../../../res/img/dielectrics.png
      :alt: Dielectrics
      
      Dielectrics with total reflections
