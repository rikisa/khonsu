Naming and Style Conventions
============================

This project has few but specific conventions regarding code style and naming. These conventions cover naming and style in C++ and CMake files.
However, the CMake rules are considered optional.

Project Structure
-----------------
The following hierarchy is a brief overview of the project directories::

    .
    ├── cmake ......... Generic CMake files, eg, Find<Dependency>.cmake
    │   └── khonsu .... Projec specific cmake files, eg, coverage, dependencies ...
    ├── docs .......... Project and API documentation
    ├── src ........... Khonsu source code
    │   ├── app ....... The Khonsu application
    │   ├── core ...... Core components, used in several engine components, e.g. linalg
    │   └── engine .... Definition and implementation of the abstract engine components, e.g. viewer
    ├── tests ......... All tests for src. Mirrors the hierarchy in src
    │   └── imitates .. All imitates: Mocks and Fakes
    └── util .......... General scripts for housekeeping, linting, static analysis, docker ...

Directories and Filenames are in CamelCase, where the hierarchy of directories should reflect the hierarchy of namespaces. Implementation and header files should have the same name as the essential class struct or namespace they are implementing or declaring.

CMake
-----
In the root directory is a *cmake-format.py* file, from which cmake-format_ takes parses formatting rules. The following conventions apply to cmake files:

============= ============
Topic         Convention
============= ============
Formatting    All CMakeLists.txt and *\*.cmake* files are formatted using cmake-format and the projects *cmake-format.py* file.
Target names  CMake targets (defined by :code:`add_library()`, :code:`add_executable()` or :code:`add_custom_target()`) must be in *CamelCase*.
============= ============

C++
---
The root directory contains a *.clang-format* and a *.clang-tidy* file. These files arr understood by the clang tools clang-format_ and clang-tidy_, respectively.
Commits that do not pass the clang-format check are rejected. The project uses the C++20 standard without compiler-specific extension.  The *.clang-tidy* file lists the static analysis checks, that every release must pass.

Usage and Semantics
^^^^^^^^^^^^^^^^^^^

====================== ============
Topic                  Convention
====================== ============
:cpp:`class`           If a class is never passed around or has no members, it should be refactored to, e.g. (namespaced) free functions
:cpp:`struct`          Used for aggregate initialization whenever possible. Otherwise indicates a simple data structure. All members must
                       be public and non-const. Bound functions are allowed if they have no side effects and are const.
                       No inheritance with structs as a base class.
Member access          Instead of any naming rules, member functions and variables are accessed using the this reference: :cpp:`return this->foo;`
Inheritance            Preferably only for implementation of Abstract interfaces or fir mixins.
Templates              All paramter must be constrained. If realy any type is possible, an *AnyConept* must be used. No enable this, or similar.
                       Just C++20 concepts and constrains
Pointer                Use standard lib smart pointers. If raw pointers are used, they shoudl contained in a local scope and never leave it.
Nullptr                Use only in context of dynamic pointer casts. In terms of return values, :code:`std::optional` is the better alternative.
====================== ============

Naming
^^^^^^

========================= ============
Topic                     Convention
========================= ============
                   :cpp:`class`
--------------------------------------
Class Names               CamelCase; prepending on inheritance if possible (:code:`BaseObject <>- SphereObject`).
Interface Classes         CamelCase; must start with *Base* (matches :code:`Base(.*)`).
Bound public functions    snake_case; getter, and setter must start with *get* or *set*, respectively.
Bound private functions   snake_case_with_postfix\_underscore\_; getter, and setter must start with *get* or *set*, respectively.
Static functions          snake_case
Private Member variables  snake_case_with_postfix\_underscore\_
                   :cpp:`struct`
--------------------------------------
Class Names               CamelCase
Interface Classes         Should not exist; see `Usage and Semantics`_.
Bound functions           snake_case; no getter or setters.
Public Member variables   snake_case
                   :cpp:`template`
--------------------------------------
Template Parameter        Preferably a single, lower case word, no compounds
                   Testing
--------------------------------------
Tests filename            Any file implementing tests starts with a `test_` prefix. The name should match the filename of the source file it covers most.
Fakes and Mocks           The files implementing the fakes or mocks have a `fake_` or `mock_` prefix. The name should match the filename of the source file it covers most.
                          The fake or mock class must have the same but with a `Fake` or `Mock` prefix.
========================= ============

Style
^^^^^

====================== ============
Topic                  Convention
====================== ============
Types                  Use :cpp:`auto` with trailing types when possible.
CV-Qualifiers          East side
====================== ============

Documentation
^^^^^^^^^^^^^

====================== ============
Topic                  Convention
====================== ============
   :cpp:`class` and :cpp:`struct`
-----------------------------------
Docstrings             Every class or struct must have at least a brief description that describes the reponsibility.
Bound functions        Must explain all parameters passed to the function and any non-void return type.
               DocStrings
-----------------------------------
Doxygen commands       Doxygen commands must be of the backslash form e.g.,  :code:`\param`
Comment blocks         Use three backslash :code:`///` with JAVA_AUTOBRIEF=ON. This means, the first line till the dot is
                       interpreted as brief discription. This also helps to keep docs *brief* and proper punctuatuated :)
                       For full documentation blocks use Qt style doxygen documentation without asterisk.

                       .. code-block:: cpp

                           /// The brief description.
                           /// The more detailed description!
                           class AnExample {};


                           void func(int a) {};

Namespace              Closing namespaces end with an comment, mirroing the opening namespace.
====================== ============



.. _cmake-format: https://cmake-format.readthedocs.io/en/latest/installation.html
.. _clang-format: https://clang.llvm.org/docs/ClangFormat.html
.. _clang-tidy: https://clang.llvm.org/extra/clang-tidy
