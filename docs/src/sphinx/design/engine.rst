Toplevel View of the Khonsu Engine
==================================

The engine is a composite of multiple components which are accessible through an 
engine facade interface. The facade allows to get the available modules and their
configurations. 

The facade is used during configuration and setup of Khonsu, and finally as an entrypoint to the main loop.

.. only:: plantuml

   .. uml:: ../../plantuml/structur/core-classes.puml

Composition of Khonsu
---------------------
At its heart, khonsu is an assembly of parts. Parts are composed into an assembly. The following BaseParts exist:
- BaseController
- BaseViewer
- BaseTracer
- BaseElements

They must be derived from BasePart interface, which implements the common way of creation and configuration. Thereby Controller, Viewer and Tracer becom interchangeable.

Then there is the Core library, which contains all math and utilies and is a compile time dependency.

General Controlflow
-------------------
A assembly is generated via the Khonsu interface, and control flow goes to the asembly.
The assembly registers the BaseParts (and resolves inter part dependencies) to each other. Then the controller entrypoint is called in a new thread and the viewer entrypoint is called in the main thread (to allow real time opengl viewer)

The controller makes use of the Viewer, Tracer and XXX as it sees fit (batch / interactive)

Renderloop
----------
Elements to render are registered to the controller. The controller can compose them to a scene and passes them to the renderer. The controller lives in a concurrent non-main tread and can spawn as many worker threads as needed, e.g. via the renderer. It can block, but musnt.
