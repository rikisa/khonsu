Technical Details
=================
Functional description of all functional elements and definitions used in the program.

Camera
------
Camera generates rays, used to trace the scene

   .. figure:: ../../../res/img/camera.svg
      :scale: 250 %
      :alt: Schema of the camera

      The representation of the camera model with the eye point :math:`E`, looking towards an image plane :math:`I`.
      The width of the image plane, :math:`W_i`, is set to 1 world unit. The canvas height, :math:`H`, is determined by the
      desired aspect ratio. The focal length, :math:`f`, defines the distance from :math:`E` to I. The row vector, :math:`i_r`, and
      column vector, :math:`i_c`, align with the x and y-axes of the image plane. The camera's position and location in the world is
      defined by the unit vectors :math:`e_x` (red axis), :math:`e_y` (green axis), and :math:`e_z` (blue axis). The pixel center :math:`p_{r,c}` indicates the location
      of a pixel's center on the image plane. The angle theta represents the field of view, which is determined by the focal length
      and the image plane's size. Finally, :math:`r_d` stands for the ray direction from the eye point :math:`E` through :math:`p_{r,c}`, used to
      project a 3D point onto the 2D image plane.
