Roadmap
=======

A roadmap to decide what to start and do now, and what to do later on. Subject to change and made, and follow up steps are made based on past iterations. The overall goal is
to reach everything mentioned in _`Intended Features of Khonsu`


Baseline
--------
Baseline functionality should be able to render something already

Modularity
   - CLI frontend that loads a script
   - Parse Lua Filse with simple scene (camera, image, scene, sphere, point light)
   - Render engine producing a PNG from lua script

Raytracer
   - Pinhole camera
   - Sphere
   - Point light
   - Hard shadows

Project
   - Automated static analysis
   - C++20
   - Unittests
   - Smoke test running a test script
