Intended Features of Khonsu
===========================

There are many things Khonsu aims to implement. Here is a more refined overview of what Khonsu should be and what not. Starting from the principal, overall goals
going to the specific features and ending at some stretch features that are nice but currently not of uttermost priority interest.

General Ideas
-------------
.. _top-level-req:

Sandbox for playing around with software
   The overall goal is an solid testbed or structure that allows testing out stuff. From techniques to benchmarking to algorithms. In principle, most
   Stuff should fit in when related to raytracing or linear algebra in general.

Raytracer
   The overall goal (or considered 1.0) is the capability to render the correl box in one of its many variations.

   .. figure:: ../../../res/img/cornellbox.png
      :scale: 100 %
      :alt: The Cornell Box

      Cornell Box rendered by SeeSchloss for Wikipedia 

Project template
   Test different ways of doing things project-wise. Foremost putting design and documentation in a single contained repository
   and hosting it on GitLab

Optimization
   Beeing on the dark side and let some steam off with premature otpimizations :)

Features
--------

Sandbox
^^^^^^^

Modularity
   Some modular composition. There should be many potential apps using the same backend components. One Part is the CLI, and another is a GUI app, binding to the same backend via a single interface.

Gouverning Interface
   There is one fundamental interface: Firstly the overall engine assembly, consisting of a single component that holds and connects all
   engine parts. This assembly is supposed to be the main composition root. An application using the engine can use helper to create
   configurations. These configurations are then set in the assembly, that binds the engine and has the main entry point.

Linear Algebra Implementations
   The design separates everything linear algebra from all other parts. All top-level linear algebra classes should be concrete template classes so that a header-only library is possible (for expression templates)
   In the Khonus sandbox, these are pitted against alternative implementation in a benchmark.

Benchmarking
   The assembly has simple benchmark and instrospection capabilities. Minimum the runtime of the main entrypoint is tracked.

Raytracer
^^^^^^^^^

- Dielectric shading
- Soft shadows
- Hard shadows
- Sphere, Plane, and Triangle; support for more geometries
- Textures (with maybe filtering)
- Anti-Aliasing and supersampling
- Monte Carlo sampling at any intersection point
- Pinhole camera
- Fixed aperture camera
- Live Rendering View with OpenGL
- PNG output
- Interactive, embedded Lua interpreter and parser
- Physical correct light and color model


Project Template
^^^^^^^^^^^^^^^^

- Automated Static Analysis
- Automated Dynamic Analysis via Smoke tests
- C++ 20
- Swappable Tracer Backend / Renderer
- Everything, including docu in one repo
- Testing: Unit, Integration, Smoke, Static
- Logging
- Higly decoupled, highly modularized along performance criticall boundaries


Optimization
^^^^^^^^^^^^

- Virtual Interface (Baseline) vs. templated constructor + std::function vs. PIMPL
- Preprocessing (ordering objects by distance before tracing)
- Software Linalg (Baseline) vs. SIMD vs. Template Expressions vs. TE + SIMD vs. OpenGL Shader

Stretch
^^^^^^^
- Time and moving objects
- Motion blurr
- CMake source only build: fetch all dependencies and build them from source

Mindmap
^^^^^^^
.. mermaid:: ../../mermaid/mindmap-overall.mmd
