Welcome to the Khonsu documentation!
=====================================

.. role:: cpp(code)
   :language: cpp

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./design/overall.rst
   ./design/roadmap.rst
   ./design/engine.rst

   ./design/details.rst

   ./docgen/apidoc.rst
   

   ./misc/conventions.rst
   ./misc/thoughts.rst
   ./misc/snapshots.rst

   ./misc/todos.rst



This playground past-time project implements a simple but non-trivial raytracing engine. While I believe in good documentation (which in turn does not mean that it exists), 
chances are this project is of little help, entertaining, or of any specific value other than me learning and trying out stuff. It is online for two simple reasons: sharing is caring and I want to write about the journey.

If think it is worthwile your time or you are bored beyoind saving: read on!

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
* `Code Coverage Report <covreport/index.html>`_
