#include <cmath>
#include <cstddef>
#include <memory>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material/color.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/viewer/pngwriter.hpp"

using Vec2 = linalg::ArrayN<double, 2>;

auto
dist( Vec2 one, Vec2 other ) -> double
{
  auto diff = one - other;
  return std::sqrt( ( diff * diff ).sum() );
}

auto
main() -> int
{
  auto img = std::make_shared<rendering::Image>( 30, 50 );

  Vec2 center_left{ { -6, -9 } };
  double radius_left = 3;

  Vec2 center_right{ { -8, 10 } };
  double radius_right = 4;

  Vec2 center_mouth_upper{ { -11, 0 } };
  Vec2 center_mouth_lower{ { -6, 0 } };
  double radius_mouth_upper = 20;
  double radius_mouth_lower = 18;

  for ( double row = -15; row < 15; ++row ) {
    for ( double col = -25; col < 25; ++col ) {
      Vec2 cur_pos{ { row, col } };

      auto row_i = static_cast<std::size_t>( row + 15 );
      auto col_i = static_cast<std::size_t>( col + 25 );

      img->set_color( row_i, col_i, material::Color{ { 0.2, 0.2, 0.2 } } );

      if ( dist( center_left, cur_pos ) <= radius_left ) {
        img->set_color( row_i, col_i, material::Color{ { 0, 1, 1 } } );
      }

      if ( dist( center_right, cur_pos ) <= radius_right ) {
        img->set_color( row_i, col_i, material::Color{ { 1, 1, 0 } } );
      }

      auto mdist_upper = dist( center_mouth_upper, cur_pos );
      auto mdist_lower = dist( center_mouth_lower, cur_pos );
      if ( ( mdist_upper >= radius_mouth_upper ) && ( mdist_lower <= radius_mouth_lower ) ) {
        img->set_color( row_i, col_i, material::Color{ { 1, 0, 1 } } );
      }
    }
  }

  viewer::PNGWriter png( "smiley.png" );
  png.read_image( img );
  png.show();

  return 0;
}
