// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/objects/objects.hpp"

namespace imitates::rendering {

class MockObject : public ::objects::BaseObject {
    using Ray = linalg::Ray;
    using Intersection = linalg::Intersection;
    using FloatT = linalg::FloatType;

  public:
    MAKE_MOCK4( get_intersect, bool( Ray &, Intersection &, FloatT, FloatT ), override );
};

} // namespace imitates::rendering

// NOLINTEND(*-trailing-*)
