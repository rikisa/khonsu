// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/rendering/image.hpp"

namespace imitates::rendering {

class MockImage : public ::rendering::Image {};

} // namespace imitates::rendering

// NOLINTEND(*-trailing-*)
