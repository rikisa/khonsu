// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/linalg/material.hpp"

namespace imitates::linalg {

class MockObject : public material::BaseMaterial {
  public:
    MAKE_MOCK2( scatter_ray, void( Ray & ray, Vector & normal ), override );
};

} // namespace imitates::linalg

// NOLINTEND(*-trailing-*)
