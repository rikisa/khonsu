#include "imitates/engine/fakeviewer.hpp"

#include <memory>

#include "khonsu/part/partconfig.hpp"
#include "khonsu/partforwards.hpp"

namespace imitates::engine {

void
FakeViewerWithConf::use_logsink( ::engine::part::BaseLogRef log )
{
  (void)log;
}

void
FakeViewerWithConf::use_eventqueue( ::engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

void
FakeViewerWithConf::read_image( rendering::ImageRef image )
{
  (void)image;
}

void
FakeViewerWithConf::show()
{
}

void
FakeViewerWithConf::hide()
{
}

auto
FakeViewerWithConf::create_config() -> ::engine::PartConfig
{
  ::engine::PartConfig conf{ "FakeViewer" };
  conf.add_parameter<int>( "foo" );
  conf.add_parameter<double>( "bar", 0.0 );

  return conf;
}

auto
FakeViewerWithConf::get_part_config() const -> ::engine::PartConfig
{
  auto conf = FakeViewerWithConf::create_config();
  conf.set_parameter<int>( "foo", this->_foo.value_or( 0 ) );

  return conf;
}

auto
FakeViewerWithConf::create_part( ::engine::PartConfig const & from_conf ) -> ::engine::part::BaseViewerRef
{
  if ( !from_conf.get_parameter( "foo" ).has_value() ) {
    return std::make_shared<FakeViewerWithConf>( std::nullopt );
  }

  try {
    // NOLINTNEXTLINE(*-access) Checked before
    return std::make_shared<FakeViewerWithConf>( std::any_cast<int>( from_conf.get_parameter( "foo" ).value().value ) );
  } catch ( std::bad_any_cast const & ) {
    return std::make_shared<FakeViewerWithConf>( std::nullopt );
  }
}

} // namespace imitates::engine
