#include "imitates/engine/faketracer.hpp"

#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {


void
FakeTracer::use_logsink( ::engine::part::BaseLogRef log )
{
  (void)log;
}

void
FakeTracer::use_eventqueue( ::engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

// CoFakeTracer::ntroller exposed

void
FakeTracer::write_image( rendering::ImageRef image )
{
  (void)image;
}

void FakeTracer::trace_scene( rendering::SceneRef /*scene*/ ){};
void FakeTracer::set_camera( rendering::BaseCameraRef /*camera*/ ){};

auto
FakeTracer::create_config() -> ::engine::PartConfig
{
  ::engine::PartConfig conf{ "FakeTracer" };
  conf.add_parameter<int>( "bar" );

  return conf;
}

auto
FakeTracer::create_part( ::engine::PartConfig const & /*unused*/ ) -> ::engine::part::BaseTracerRef
{
  return std::shared_ptr<FakeTracer>();
}

} // namespace imitates::engine
