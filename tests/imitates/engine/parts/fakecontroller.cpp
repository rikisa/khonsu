#include "imitates/engine/fakecontroller.hpp"

namespace imitates::engine {

void
FakeController::init()
{
}

auto
FakeController::create_config() -> ::engine::PartConfig
{
  ::engine::PartConfig conf{ "FakeController" };
  conf.add_parameter<int>( "foobar" );

  return conf;
}

auto
FakeController::create_part( ::engine::PartConfig config ) -> ::engine::part::BaseControllerRef
{
  (void)config;
  return std::shared_ptr<FakeController>();
}

void
FakeController::use_logsink( ::engine::part::BaseLogRef log )
{
  (void)log;
}

void
FakeController::use_eventqueue( ::engine::part::BaseEventQueueRef equeue )
{
  (void)equeue;
}

void
FakeController::use_viewer( ::engine::part::BaseViewerRef viewer )
{
  (void)viewer;
}

void
FakeController::use_tracer( ::engine::part::BaseTracerRef tracer )
{
  (void)tracer;
}

} // namespace imitates::engine
