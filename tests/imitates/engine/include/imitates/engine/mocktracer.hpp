// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class MockPartTracer : public ::engine::part::BaseTracer {
  public:
    MAKE_MOCK1( use_logsink, void( ::engine::part::BaseLogRef ), override );
    MAKE_MOCK1( use_eventqueue, void( ::engine::part::BaseEventQueueRef ), override );

    // controller interface
    MAKE_MOCK1( write_image, void( rendering::ImageRef ), override );
    MAKE_MOCK1( trace_scene, void( rendering::SceneRef ), override );
    MAKE_MOCK1( set_camera, void( rendering::BaseCameraRef ), override );
};

} // namespace imitates::engine

// NOLINTEND(*-trailing-*)
