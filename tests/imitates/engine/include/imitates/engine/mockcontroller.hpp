// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/part/basecontroller.hpp"

namespace imitates::engine {

class MockPartController : public ::engine::part::BaseController {
  public:
    MAKE_MOCK1( use_logsink, void( ::engine::part::BaseLogRef ), override );
    MAKE_MOCK1( use_eventqueue, void( ::engine::part::BaseEventQueueRef ), override );

    MAKE_MOCK0( init, void(), override );

    // controller interface
    MAKE_MOCK1( use_viewer, void( ::engine::part::BaseViewerRef ), override );
    MAKE_MOCK1( use_tracer, void( ::engine::part::BaseTracerRef ), override );
};

} // namespace imitates::engine

// NOLINTEND(*-trailing-*)
