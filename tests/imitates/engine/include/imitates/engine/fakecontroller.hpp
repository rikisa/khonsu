#pragma once

#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class FakeController : public ::engine::part::BaseController {
  public:
    ~FakeController() override = default;

    void use_logsink( ::engine::part::BaseLogRef log ) override;
    void use_eventqueue( ::engine::part::BaseEventQueueRef equeue ) override;

    void use_viewer( ::engine::part::BaseViewerRef viewer ) override;
    void use_tracer( ::engine::part::BaseTracerRef tracer ) override;

    void init() final;

    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig ) -> ::engine::part::BaseControllerRef;
};

} // namespace imitates::engine
