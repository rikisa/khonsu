#pragma once

#include <optional>

#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class FakeViewerWithConf : public ::engine::part::BaseViewer {
  public:
    ~FakeViewerWithConf() override = default;
    explicit FakeViewerWithConf( std::optional<int> foo_val = std::nullopt ) : _foo( foo_val ){};

    void use_logsink( ::engine::part::BaseLogRef ) override;
    void use_eventqueue( ::engine::part::BaseEventQueueRef ) override;

    void read_image( rendering::ImageRef image ) override;
    void show() override;
    void hide() override;


    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig const & ) -> ::engine::part::BaseViewerRef;

    [[nodiscard]] auto get_part_config() const -> ::engine::PartConfig override;

    auto get_foo() -> std::optional<int> { return _foo; }

  private:
    std::optional<int> _foo;
};

} // namespace imitates::engine
