// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class MockPartViewer : public ::engine::part::BaseViewer {
  public:
    MAKE_MOCK1( use_logsink, void( ::engine::part::BaseLogRef ), override );
    MAKE_MOCK1( use_eventqueue, void( ::engine::part::BaseEventQueueRef ), override );
    MAKE_MOCK0( init, void(), override );

    // controller interface
    MAKE_MOCK1( read_image, void( rendering::ImageRef ), override );
    MAKE_MOCK0( show, void(), override );
    MAKE_MOCK0( hide, void(), override );
    // clang-format off
    MAKE_MOCK0( get_part_config, ::engine::PartConfig(), const override  );
    // clang-format on
};

} // namespace imitates::engine

// NOLINTEND(*-trailing-*)
