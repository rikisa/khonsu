#pragma once

#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class FakeTracer : public ::engine::part::BaseTracer {
  public:
    ~FakeTracer() override = default;

    void use_logsink( ::engine::part::BaseLogRef log ) override;
    void use_eventqueue( ::engine::part::BaseEventQueueRef equeue ) override;

    // Controller exposed

    void write_image( rendering::ImageRef image ) override;
    void set_camera( rendering::BaseCameraRef camera ) override;
    void trace_scene( rendering::SceneRef scene ) override;

    static auto create_config() -> ::engine::PartConfig;
    static auto create_part( ::engine::PartConfig const & /*unused*/ ) -> ::engine::part::BaseTracerRef;
};

} // namespace imitates::engine
