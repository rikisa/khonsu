// NOLINTBEGIN(*-trailing-*)
#pragma once

#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/trompeloeil.hpp>

#include "khonsu/basepartregistry.hpp"
#include "khonsu/part/partconfig.hpp"

namespace imitates::engine {

class MockPartRegistry : public ::engine::BasePartRegistry {
  public:
    MAKE_MOCK1( create_config, std::optional<::engine::PartConfig>( std::string const & ), override );

    MAKE_MOCK0( registered, std::set<std::string>(), override );


    MAKE_MOCK2(
        _register_part, void( std::string const &, std::function<std::any( ::engine::PartConfig const & )> ), override
    );

    MAKE_MOCK2( _register_config, void( std::string const &, std::function<::engine::PartConfig()> ), override );

    MAKE_MOCK2( _create_part, std::any( std::string const &, ::engine::PartConfig const & ), override );
};

} // namespace imitates::engine

// NOLINTEND(*-trailing-*)
