#include <format>
#include <iostream>
#include <vector>

#include "khonsu/linalg/types.hpp"
#include "khonsu/rendering/basecamera.hpp"

auto
vec_to_coordinate( std::string name, linalg::Vector vec )
{
  return std::format(
      "\\coordinate ({}) at ({}, {}, {});\n", name, vec.cget().at( 0 ), vec.cget().at( 1 ), vec.cget().at( 2 )
  );
}

auto
main() -> int
{
  auto constexpr rows = 30;
  auto constexpr cols = 40;
  auto constexpr fov = 0.1;

  // auto constexpr sym_eye = "E";
  // auto constexpr sym_cam_origin = "O_{C}";
  // auto constexpr sym_img_origin = "O_{I}";

  rendering::BaseCamera cam{};
  cam.set_shape( rows, cols, fov );

  linalg::Ray ray{};

  // std::cout << vec_to_coordinate( sym_eye, cam.eye() );
  // std::cout << vec_to_coordinate( sym_cam_origin, cam.pos() );
  // std::cout << vec_to_coordinate( sym_img_origin, cam.image_origin() );

  // std::cout << std::format( "\\draw ({}) node[point]{{}} node[above=3,left]{{${}$}};\n", sym_eye, sym_eye );
  // std::cout << std::format(
  //     "\\draw ({}) node[point]{{}} node[above=3,left]{{${}$}};\n", sym_cam_origin, sym_cam_origin
  // );
  // std::cout << std::format(
  //     "\\draw ({}) node[point]{{}} node[above=3,left]{{${}$}};\n", sym_img_origin, sym_img_origin
  // );

  std::vector<linalg::FloatType> px_x;
  std::vector<linalg::FloatType> px_y;
  std::vector<linalg::FloatType> px_z;

  for ( std::size_t i = 0; i < cam.size(); ++i ) {
    cam.update_ray( ray, i );
    auto ray_end = std::format( "R{}", i );
    // std::cout << vec_to_coordinate( ray_end, ray_to );
    // std::cout << std::format( "\\draw (E) -- ({});\n", ray_end );

    auto diff = ( ray.ref_point() - ray.pos() );
    auto to_px = ray.pos() + ( ray.dir() * diff.norm() );

    px_x.emplace_back( to_px.cget().at( 0 ) );
    px_y.emplace_back( to_px.cget().at( 1 ) );
    px_z.emplace_back( to_px.cget().at( 2 ) );
  }

  std::cout << "x = [";
  for ( auto comp : px_x ) {
    std::cout << std::format( "{}, ", comp );
  };
  std::cout << "]\n";

  std::cout << "y = [";
  for ( auto comp : px_y ) {
    std::cout << std::format( "{}, ", comp );
  };
  std::cout << "]\n";

  std::cout << "z = [";
  for ( auto comp : px_z ) {
    std::cout << std::format( "{}, ", comp );
  };
  std::cout << "]\n";

  return 0;
}
