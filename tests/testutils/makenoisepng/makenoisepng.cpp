#include <memory>

#include "khonsu/linalg/material/color.hpp"
#include "khonsu/linalg/util.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/viewer/pngwriter.hpp"

auto
main() -> int
{
  std::size_t constexpr width = 400;
  std::size_t constexpr height = 400;

  auto img = std::make_shared<rendering::Image>( width, height );

  linalg::util::StochasicSampler<> rng{};

  for ( std::size_t i = 0; i < width * height; ++i ) {
    auto val = rng.get_random_value();
    if ( val < 0 || val >= 1.0 ) {
      std::cout << "Val is: " << val << "!\n";
    }
    auto fval = static_cast<material::Color::elem_type>( val );
    img->set_color( i / width, i % width, material::Color( { fval, 1.0F - fval, 0.0 } ) );
  }

  viewer::PNGWriter writer{ "noise.png" };
  writer.read_image( img );
  writer.show();

  return 0;
}
