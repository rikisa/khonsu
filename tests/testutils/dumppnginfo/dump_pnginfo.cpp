#include <format>

#include "testutils/pngwalker.hpp"

auto
main( int argc, char ** argv ) -> int
{
  // Pack argv into a vector of strings
  auto arg_span = std::span{ argv, static_cast<std::size_t>( argc ) } |
      std::views::transform( []( auto arg ) { return std::string{ arg }; } );
  std::vector<std::string> args{ arg_span.begin(), arg_span.end() };

  if ( args.size() != 2 ) {
    std::cerr << std::format(
        "Need exactly on parameter: path to png file to open. Got {} instead\n", args.size() - 1
    );
    return 1;
  }

  std::filesystem::path path_to_image = args.at( 1 );

  testutils::PngWalker walker{ path_to_image };

  if ( !walker.is_open() ) {
    std::cerr << std::format( "Could not open the file {}\n", path_to_image.string() );
    return 1;
  }
  std::cout << std::format( "Opened {}\n", path_to_image.string() );


  // read signature
  if ( !walker.validate_signature() ) {
    std::cerr << std::format( "Invalid file signature!\n" );
  } else {
    std::cout << "Signature OK\n";
  }

  auto ihdr = walker.read_ihdr();

  std::cout << std::format( "Chunktype: {} (0x{:x})\n", ihdr.chunk_type_str, ihdr.chunk_type_id );
  std::cout << "Width: " << ihdr.width << '\n';
  std::cout << "Height: " << ihdr.height << '\n';


  // FIXME
  std::cout << std::format(
      "Bit depth: {}\nColor type: {}\nCompress: {}\nFilter: {}\nInterlace: {}\n", ihdr.bit_depth, ihdr.color_type,
      ihdr.compress, ihdr.filter, ihdr.interlace
  );

  return 0;
}
