#pragma once

#include <filesystem>
#include <string_view>

namespace testutils {
auto get_temp_file( std::filesystem::path const & tempdir, std::string file_prefix = {}, std::string file_ext = {} )
    -> std::filesystem::path;
auto
get_temp_file( std::filesystem::path const & tempdir, std::string_view const & file_prefix, std::string file_ext = {} )
    -> std::filesystem::path;
} // namespace testutils
