#pragma once

#include <limits>

#include <catch2/catch_message.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include "khonsu/linalg/linalg.hpp"

namespace testing {

constexpr double precf = std::numeric_limits<float>::epsilon();
constexpr double precd = std::numeric_limits<double>::epsilon();
using Catch::Matchers::WithinAbs;
using Catch::Matchers::WithinRel;

} // namespace testing

#define REQUIRE_IS_CLOSEX( a, b, T ) REQUIRE_THAT( a, testing::WithinAbs( b, std::numeric_limits<T>::epsilon() ) )
#define REQUIRE_IS_CLOSEF( a, b ) REQUIRE_THAT( a, testing::WithinAbs( b, testing::precf ) )
#define REQUIRE_IS_CLOSED( a, b ) REQUIRE_THAT( a, testing::WithinAbs( b, testing::precd ) )

#define _CHECK( a, b )                                                                                                 \
  using a_t = std::remove_cvref_t<decltype( ( a ) )>;                                                                  \
  using b_t = std::remove_cvref_t<decltype( ( b ) )>;                                                                  \
  static_assert( std::is_same_v<a_t, b_t>, "Macro assumes all vecs of same element" );


#define REQUIRE_IS_GE( a, b )                                                                                          \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<a_t>::epsilon();                                                                    \
    INFO( ( a ) << " < " << ( b ) );                                                                                   \
    REQUIRE( ( a ) >= ( b ) );                                                                                         \
  }

#define REQUIRE_IS_LT( a, b )                                                                                          \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<a_t>::epsilon();                                                                    \
    INFO( ( a ) << " >= " << ( b ) );                                                                                  \
    REQUIRE( ( a ) < ( b ) );                                                                                          \
  }

#define REQUIRE_IS_CLOSE( a, b )                                                                                       \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<a_t>::epsilon();                                                                    \
    INFO(                                                                                                              \
        ( a ) << " != " << ( b ) << "(+/- " << eps * 100 * std::max( std::abs( ( a ) ), std::abs( ( b ) ) ) << ")"     \
    );                                                                                                                 \
    REQUIRE_THAT( a, testing::WithinRel( b ) );                                                                        \
  }


#define REQUIRE_ALL_CLOSE3( a, b, T )                                                                                  \
  REQUIRE(                                                                                                             \
      ( testing::WithinAbs( ( a )[ 0 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 0 ] ) &&                     \
        testing::WithinAbs( ( a )[ 1 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 1 ] ) &&                     \
        testing::WithinAbs( ( a )[ 2 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 2 ] ) )                      \
  )

#define REQUIRE_ALL_CLOSEXX( a, b, T, d )                                                                              \
  bool is_same = true;                                                                                                 \
  for ( size_t i = 0; i < ( d ); ++i ) {                                                                               \
    is_same = is_same && testing::WithinAbs( ( a )[ i ], std::numeric_limits<T>::epsilon() ).match( ( b )[ i ] );      \
  }                                                                                                                    \
  REQUIRE( is_same );

#define REQUIRE_ALL_CLOSEX( a, b, T )                                                                                  \
  {                                                                                                                    \
    REQUIRE( ( a ).dimension == ( b ).dimension );                                                                     \
    bool is_same = true;                                                                                               \
    for ( size_t i = 0; i < ( a ).dimension; ++i ) {                                                                   \
      is_same = is_same && testing::WithinAbs( ( a )[ i ], std::numeric_limits<T>::epsilon() ).match( ( b )[ i ] );    \
    }                                                                                                                  \
    REQUIRE( is_same );                                                                                                \
  }

//  REQUIRE( std::equal( points_at.cbegin(), points_at.cend(), point2.cbegin(), point2.cend(), []( auto a, auto b ) {
//    return testing::WithinAbs( a, linalg::float_type_eps ).match( b );
//  } ) );

#define REQUIRE_ALL_CLOSE( a, b )                                                                                      \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<typename a_t::elem_type>::epsilon();                                                \
    bool is_same = true;                                                                                               \
    for ( std::size_t i = 0; i < ( a ).dimension; ++i ) {                                                              \
      is_same = testing::WithinAbs( ( a )[ i ], eps ).match( ( b )[ i ] );                                             \
      if ( !is_same ) {                                                                                                \
        break;                                                                                                         \
      }                                                                                                                \
    }                                                                                                                  \
    INFO( ( a ).repr() + " != " + ( b ).repr() );                                                                      \
    REQUIRE( is_same );                                                                                                \
  }

#define REQUIRE_NONE_CLOSE( a, b )                                                                                     \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<typename a_t::elem_type>::epsilon();                                                \
    bool is_same = false;                                                                                              \
    for ( std::size_t i = 0; i < ( a ).dimension; ++i ) {                                                              \
      is_same = testing::WithinAbs( ( a )[ i ], eps ).match( ( b )[ i ] );                                             \
      if ( is_same ) {                                                                                                 \
        break;                                                                                                         \
      }                                                                                                                \
    }                                                                                                                  \
    INFO( ( a ).repr() + " partly equal " + ( b ).repr() );                                                            \
    REQUIRE( !is_same );                                                                                               \
  }

#define REQUIRE_ALL_GT( a, b )                                                                                         \
  {                                                                                                                    \
    _CHECK( ( a ), ( b ) )                                                                                             \
    auto eps = std::numeric_limits<a_t::elem_type>::epsilon();                                                         \
    bool is_greater = false;                                                                                           \
    for ( std::size_t i = 0; i < ( a ).dimension; ++i ) {                                                              \
      is_greater = ( a )[ i ] > ( b )[ i ] && ( ( a )[ i ] - ( b )[ i ] ) > eps;                                       \
      if ( !is_greater ) {                                                                                             \
        break;                                                                                                         \
      }                                                                                                                \
    }                                                                                                                  \
    REQUIRE( is_greater );                                                                                             \
  }

#define REQUIRE_NOT_ALL_CLOSE3( a, b, T )                                                                              \
  REQUIRE(                                                                                                             \
      !( testing::WithinAbs( ( a )[ 0 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 0 ] ) &&                    \
         testing::WithinAbs( ( a )[ 1 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 1 ] ) &&                    \
         testing::WithinAbs( ( a )[ 2 ], std::numeric_limits<T>::epsilon() ).match( ( b )[ 2 ] ) )                     \
  )

// constexpr double eps = std::numeric_limits<PosVec::elem_type>::epsilon() * 1000;
// #define is_close(a, b) a == Approx(b).epsilon(eps)
// #define all_close_3d(a, b) (a[0] == Approx(b[0]).epsilon(eps) && a[1] ==
// Approx(b[1]).epsilon(eps) && a[2] == Approx(b[2]).epsilon(eps))

template<>
inline auto
Catch::MessageBuilder::operator<<( linalg::Vector const & value ) && -> Catch::MessageBuilder &&
{
  m_stream << value.repr();
  return CATCH_MOVE( *this );
}
