#pragma once

#include <algorithm>
#include <array>
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <ranges>
#include <string>
#include <vector>

namespace testutils {

struct IHDR {
    std::string chunk_type_str;
    int chunk_type_id;
    int width;
    int height;
    unsigned int bit_depth;
    unsigned int color_type;
    unsigned int compress;
    unsigned int filter;
    unsigned int interlace;
};

// NOLINTBEGIN(*-magic-numbers, *-reinterpret-cast)
class PngWalker {
    static std::array<unsigned char, 8> constexpr png_signature = { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };

  public:
    PngWalker( PngWalker const & ) = delete;
    PngWalker( PngWalker && ) noexcept = default;
    auto operator=( PngWalker const & ) -> PngWalker & = delete;
    auto operator=( PngWalker && ) -> PngWalker & = default;

    ~PngWalker() { this->_file_stream.close(); }

    explicit PngWalker( std::filesystem::path png_path ) : _png_path( std::move( png_path ) )
    {
      this->_file_stream = std::ifstream( this->_png_path, std::ios::binary | std::ios::in );
    };

    auto is_open() const -> bool { return this->_file_stream.is_open(); }

    auto validate_signature() -> bool
    {
      if ( !this->is_open() ) {
        return false;
      }

      this->_read_bytes_into_buffer( 0, png_signature.size() );

      return std::ranges::equal(
          std::span( this->_buffer ) | std::ranges::views::transform( []( auto chr ) -> unsigned char {
            return static_cast<unsigned char>( chr );
          } ),
          std::span( png_signature )
      );
    }

    auto read_ihdr() -> IHDR
    {
      if ( !this->is_open() ) {
        return {};
      }

      this->_read_bytes_into_buffer( 8, 21 );

      // // chunksize
      // auto chunksize_view = std::span( this->_buffer.begin(), 4 ) | std::views::reverse |
      //     std::views::transform( []( auto chr ) -> unsigned char { return static_cast<unsigned char>( chr ); } );
      // std::vector<unsigned char> cont = { chunksize_view.begin(), chunksize_view.end() };
      // auto chunksize = *reinterpret_cast<unsigned int *>( cont.data() );

      // chunktype
      auto type_view = std::span( this->_buffer.begin() + 4, 4 ) | std::views::reverse |
          std::views::transform( []( auto chr ) -> unsigned char { return static_cast<unsigned char>( chr ); } );
      std::vector<unsigned char> cont_type = { type_view.begin(), type_view.end() };
      auto type = *reinterpret_cast<int *>( cont_type.data() );
      auto type_str = std::string(
          std::span( this->_buffer.begin() + 4, 4 ).begin(), std::span( this->_buffer.begin() + 4, 4 ).end()
      );

      // width
      auto width_view = std::span( this->_buffer.begin() + 8, 4 ) | std::views::reverse |
          std::views::transform( []( auto chr ) -> unsigned char { return static_cast<unsigned char>( chr ); } );
      std::vector<unsigned char> cont_width = { width_view.begin(), width_view.end() };
      auto width = *reinterpret_cast<int *>( cont_width.data() );

      // height
      auto height_view = std::span( this->_buffer.begin() + 12, 4 ) | std::views::reverse |
          std::views::transform( []( auto chr ) -> unsigned char { return static_cast<unsigned char>( chr ); } );
      std::vector<unsigned char> cont_height = { height_view.begin(), height_view.end() };
      auto height = *reinterpret_cast<int *>( cont_height.data() );


      // FIXME
      auto buff_view = std::span( this->_buffer.begin() + 16, 5 ) |
          std::views::transform( []( auto chr ) -> unsigned int { return static_cast<unsigned int>( chr ); } );
      std::vector<unsigned int> cont_buff = { buff_view.begin(), buff_view.end() };

      return IHDR{
          .chunk_type_str = type_str,
          .chunk_type_id = type,
          .width = width,
          .height = height,
          .bit_depth = cont_buff.at( 0 ),
          .color_type = cont_buff.at( 1 ),
          .compress = cont_buff.at( 2 ),
          .filter = cont_buff.at( 3 ),
          .interlace = cont_buff.at( 4 )
      };
    }

  private:
    void _read_bytes_into_buffer( std::streamoff offset, std::size_t num )
    {
      this->_file_stream.seekg( offset );
      this->_buffer.resize( num );
      this->_file_stream.read( this->_buffer.data(), static_cast<int>( num ) );
    }

    std::vector<char> _buffer{};
    std::filesystem::path _png_path{};
    std::ifstream _file_stream{};
};
} // namespace testutils

// NOLINTEND(*-magic-numbers, *-reinterpret-cast)
