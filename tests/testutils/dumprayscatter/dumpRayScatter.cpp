#include <cstddef>
#include <format>
#include <iostream>
#include <memory>
#include <vector>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/linalg/util.hpp"
#include "khonsu/objects/objects.hpp"

using linalg::CoordBase;
using linalg::FloatType;
using linalg::Intersection;
using linalg::Ray;
using linalg::Vector;
using material::Color;
using material::Dielectric;
using objects::Sphere;

thread_local linalg::util::StochasicSampler<> Sampler;

auto
main() -> int
{
  FloatType sphere_radius = 1.0;
  FloatType refractive_index = 0.8;

  auto trace_samples = 50;
  std::size_t sphere_samples = 200;
  FloatType noise = 0.01;
  FloatType spread = 1;

  auto pos_0 = Vector{ { 0.5, -2.0, 0.0 } };
  auto dir_0 = Vector{ { 0, 1, 0 } };

  Ray ray{ pos_0, dir_0 };

  std::vector<std::string> traces_x{};
  std::vector<std::string> traces_y{};

  Intersection intersect{};

  Sphere sphere(
      CoordBase{ Vector( { 0.0, 0.0, 0.0 } ) }, std::make_shared<Dielectric>( Color{ { 0 } }, 0.0, refractive_index ),
      sphere_radius
  );

  for ( auto cur_sample = 0; cur_sample < trace_samples; ++cur_sample ) {
    auto cur_pos = pos_0;

    cur_pos[ 0 ] += ( static_cast<FloatType>( trace_samples ) * -0.5 + static_cast<FloatType>( cur_sample ) ) /
        static_cast<FloatType>( trace_samples ) * spread;

    cur_pos += Sampler.get_random_dir() * noise;
    auto cur_dir = dir_0 + Sampler.get_random_dir() * noise;
    cur_pos[ 2 ] = 0;
    cur_dir[ 2 ] = 0;


    ray.move( cur_pos, cur_dir );

    std::vector<FloatType> ray_path_y{};
    std::vector<FloatType> ray_path_x{};

    ray_path_y.push_back( ray.pos()[ 0 ] );
    ray_path_x.push_back( ray.pos()[ 1 ] );

    for ( auto i = 0; i < 5; ++i ) {
      auto hit = sphere.get_intersect( ray, intersect, linalg::min_dist, 100 );
      if ( !hit ) {
        break;
      }
      ray_path_y.push_back( intersect.at_point()[ 0 ] );
      ray_path_x.push_back( intersect.at_point()[ 1 ] );

      intersect.with_material().scatter_ray( ray, intersect.at_point(), intersect.with_normal() );

      ray_path_y.push_back( ray.pos()[ 0 ] );
      ray_path_x.push_back( ray.pos()[ 1 ] );
    }
    ray_path_y.push_back( ray.pos()[ 0 ] + ray.dir()[ 0 ] );
    ray_path_x.push_back( ray.pos()[ 1 ] + ray.dir()[ 1 ] );

    std::string ray_path_values_x = "(";
    std::string ray_path_values_y = "(";
    for ( auto const & xi : ray_path_x ) {
      ray_path_values_x += std::format( "{},", xi );
    }
    for ( auto const & yi : ray_path_y ) {
      ray_path_values_y += std::format( "{},", yi );
    }
    ray_path_values_x += ')';
    ray_path_values_y += ')';

    traces_x.emplace_back( ray_path_values_x );
    traces_y.emplace_back( ray_path_values_y );
  }

  auto spos = sphere.get_origin().pos();
  std::cout << "import matplotlib.pyplot as plt \n"
            << "import numpy as np\n\n"
            << std::format(
                   "trace_x = {} + {} * np.cos(np.linspace(0, np.pi * 2, {}))\n", spos[ 1 ], sphere.radius(),
                   sphere_samples
               )
            << std::format(
                   "trace_y = {} + {} * np.sin(np.linspace(0, np.pi * 2, {}))\n", spos[ 0 ], sphere.radius(),
                   sphere_samples
               )
            << "fig, ax = plt.subplots(1)\n"
            << "ax.plot(trace_x, trace_y)\n";

  for ( auto cur_sample = 0; cur_sample < trace_samples; ++cur_sample ) {
    std::cout << std::format( "ax.plot({}, {}, marker='x')\n", traces_x.at( cur_sample ), traces_y.at( cur_sample ) );
  }

  std::cout << "ax.set_xlim(-3.0, 3.0)\n"
            << "ax.set_ylim(-3.0, 3.0)\n"
            << "ax.set_aspect(1)\n\n"
            << "plt.show()\n";

  return 0;
}
