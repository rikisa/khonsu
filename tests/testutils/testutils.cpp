#include <filesystem>
#include <format>
#include <mutex>
#include <random>
#include <stdexcept>
#include <string>
#include <utility>

#include "testutils/utils.hpp"

namespace testutils {
// NOLINTBEGIN(*-magic-numbers)
auto
get_temp_file( std::filesystem::path const & tempdir, std::string file_prefix, std::string file_ext )
    -> std::filesystem::path
{
  static std::mutex flock;
  std::unique_lock<std::mutex> lock{ flock };

  static std::random_device random_src; // a seed source for the random number engine
  static std::mt19937 prn_gen( random_src() );
  static std::uniform_int_distribution<> distrib( 0x100000, 0xffffff );

  std::FILE * file_descriptor = nullptr;
  std::string fname;
  int tries = 0;

  while ( file_descriptor == nullptr && tries < 5 ) {
    fname = tempdir / std::format( "{}-{:x}{}", file_prefix, distrib( prn_gen ), file_ext );

    std::vector<char> fname_buff( fname.begin(), fname.end() );
    fname_buff.push_back( '\0' );

    file_descriptor = std::fopen( fname_buff.data(), "w+x" ); // NOLINT(*-owning-memory)
  }

  std::filesystem::path temp_file_path( std::move( fname ) );

  // Check if file creation was successful
  if ( !std::filesystem::exists( temp_file_path ) ) {
    throw std::runtime_error( "Could not create temporary file: " + temp_file_path.string() );
  }

  return temp_file_path;
}

auto
get_temp_file( std::filesystem::path const & tempdir, std::string_view const & file_prefix, std::string file_ext )
    -> std::filesystem::path
{
  return get_temp_file( tempdir, std::string( file_prefix.cbegin() ), std::move( file_ext ) );
}

} // namespace testutils

// NOLINTEND(*-magic-numbers)
