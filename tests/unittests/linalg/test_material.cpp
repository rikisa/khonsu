// NOLINTBEGIN(*-magic-numbers)

#include <catch2/catch_template_test_macros.hpp>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/material/color.hpp"

#include "testutils/macros.hpp"

using linalg::FloatType;
using linalg::Intersection;
using linalg::Ray;
using linalg::Vector;
using material::BaseMaterial;
using material::Color;
using material::Dielectric;
using material::DiffuseLambertian;
using material::DiffuseUniform;
using material::Metal;

TEST_CASE( "BaseMaterial", "[unittest][BaseMaterial][Intersection][Ray][core][linalg]" )
{
  Ray const ray_before{ Vector{ 0 }, Vector{ { -1, -1, -1 } } };
  Ray ray_after{ ray_before };
  Intersection some_intersect{};
  some_intersect.set_normal( Vector{ { 1, 0, 0 } } );

  BaseMaterial mat{};

  SECTION( "Setting/Getting Color" )
  {
    Color some_color{ 1 };

    REQUIRE_ALL_CLOSE( mat.get_color(), Color( { 0 } ) );

    mat.set_color( some_color );

    REQUIRE_ALL_CLOSE( mat.get_color(), Color( { 1 } ) );
  }

  SECTION( "No Scattering" )
  {
    REQUIRE_ALL_CLOSE( ray_after.dir(), ray_before.dir() );

    mat.scatter_ray( ray_after, some_intersect.at_point(), some_intersect.with_normal() );

    REQUIRE_ALL_CLOSE( ray_after.dir(), ray_before.dir() );
  }
}

TEMPLATE_TEST_CASE(
    "Material Scattering",
    "[unittest][scatter_ray][Ray][core][linalg]",
    DiffuseUniform,
    DiffuseLambertian,
    Metal,
    Dielectric
)
{
  Ray const ray_before{ Vector{ 0 }, Vector{ { -1, -1, -1 } } };
  Ray ray_after{ ray_before };
  Intersection some_intersect{};
  some_intersect.set_normal( Vector{ { 1, 0.5, 1 } } );
  some_intersect.set_at( ray_before, 3.0 );

  TestType mat{};

  SECTION( "Dir and pos changed" )
  {
    REQUIRE_ALL_CLOSE( ray_after.dir(), ray_before.dir() );
    mat.scatter_ray( ray_after, some_intersect.at_point(), some_intersect.with_normal() );
    REQUIRE_NONE_CLOSE( ray_after.pos(), ray_before.pos() );
    REQUIRE_NONE_CLOSE( ray_after.dir(), ray_before.dir() );
  }

  SECTION( "New dir is in direction of normal" )
  {
    REQUIRE( some_intersect.with_normal().dot( ray_before.dir() ) < 0 );
    mat.scatter_ray( ray_after, some_intersect.at_point(), some_intersect.with_normal() );
    INFO( "N: " << some_intersect.with_normal() << " Rb: " << ray_before.dir() << " Ra: " << ray_after.dir() );

    // Dielectric gets refracted inside, all others outside
    if constexpr ( std::is_same_v<TestType, Dielectric> ) {
      REQUIRE( some_intersect.with_normal().dot( ray_after.dir() ) < 0 );
    } else {
      REQUIRE( some_intersect.with_normal().dot( ray_after.dir() ) > 0 );
    }
  }
}

TEST_CASE( "Refraction", "[unittest][Dielectric][Ray][core][linalg]" ) {}

// NOLINTEND(*-magic-numbers)
