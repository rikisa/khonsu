// NOLINTBEGIN(*-magic-numbers,*-pointer-ar*)

#include <cstddef>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/linalg/util.hpp" // IWYU pragma: keep

#include "testutils/macros.hpp"

using linalg::util::MersenneTwister;
using linalg::util::SplitMix;
using linalg::util::StochasicSampler;
using linalg::util::XorShift;

TEMPLATE_TEST_CASE( "LinAlg Utils", "[unittest][util][linalg][core]", MersenneTwister, SplitMix, XorShift )
{
  using SamplerT = StochasicSampler<TestType>;

  SamplerT rng{};

  auto foo = rng.get_random_dir();
  auto bar = rng.get_random_dir();

  SECTION( "Random dir is random" ) { REQUIRE_NONE_CLOSE( foo, bar ); }

  SECTION( "Random dir is normalized" )
  {
    REQUIRE_IS_CLOSE( foo.norm(), 1.0 );

    auto new_bar = linalg::Vector{ bar };
    new_bar.normalize();
    REQUIRE_ALL_CLOSE( bar, new_bar );
  }

  SECTION( "Random value is within [0, 1)" )
  {
    linalg::FloatType constexpr lower = 0;
    linalg::FloatType constexpr upper = 1;

    std::size_t samples = 200;
    std::vector<decltype( std::declval<SamplerT>().get_random_value() )> buffer;


    for ( std::size_t i = 0; i < samples; ++i ) {
      buffer.push_back( rng.get_random_value() );
    }

    REQUIRE( *std::min_element( buffer.cbegin(), buffer.cend() ) >= lower );
    REQUIRE( *std::max_element( buffer.cbegin(), buffer.cend() ) < upper );
  }

  SECTION( "Random value is within dynamic bounds" )
  {
    linalg::FloatType lower = GENERATE( -1, 0, linalg::eps );
    linalg::FloatType upper = GENERATE( 0.1, 1, 4 );

    std::size_t samples = 200;
    std::vector<decltype( std::declval<SamplerT>().get_random_value( lower, upper ) )> buffer;

    for ( std::size_t i = 0; i < samples; ++i ) {
      buffer.push_back( rng.get_random_value( lower, upper ) );
    }

    REQUIRE( *std::min_element( buffer.cbegin(), buffer.cend() ) >= lower );
    REQUIRE( *std::max_element( buffer.cbegin(), buffer.cend() ) < upper );
  }
}

TEMPLATE_TEST_CASE( "LinAlg Utils RNG", "[unittest][rng][util][linalg][core]", MersenneTwister, SplitMix, XorShift )
{
  std::uint64_t constexpr seed = -3;
  TestType rng{ seed };

  SECTION( "Next state is within bounds" )
  {
    std::uint64_t constexpr lower = 0;
    std::uint64_t constexpr upper = std::numeric_limits<std::uint64_t>::max();

    std::size_t samples = 200;
    std::vector<decltype( std::declval<TestType>().next_state() )> buffer;

    for ( std::size_t i = 0; i < samples; ++i ) {
      buffer.push_back( rng.next_state() );
    }

    REQUIRE( *std::min_element( buffer.cbegin(), buffer.cend() ) >= lower );
    REQUIRE( *std::max_element( buffer.cbegin(), buffer.cend() ) <= upper );
  }

  SECTION( "Next value is within [0, 1)" )
  {
    linalg::FloatType constexpr lower = 0.0;
    linalg::FloatType constexpr upper = 1.0;

    std::size_t samples = 200;
    std::vector<decltype( std::declval<TestType>().next_value() )> buffer;

    for ( std::size_t i = 0; i < samples; ++i ) {
      buffer.push_back( rng.next_value() );
    }

    REQUIRE( *std::min_element( buffer.cbegin(), buffer.cend() ) >= lower );
    REQUIRE( *std::max_element( buffer.cbegin(), buffer.cend() ) < upper );
  }
}
