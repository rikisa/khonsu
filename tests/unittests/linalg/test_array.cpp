// NOLINTBEGIN(*-magic-numbers,*-pointer-ar*)

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/linalg.hpp"

#include "testdata/vectors.hpp"

#include "testutils/macros.hpp"

namespace tv = testvals;

using linalg::FArray3;
using linalg::FArray4;

using linalg::DArray3;
using linalg::DArray4;

TEMPLATE_TEST_CASE( "ArrayN Constructors", "[ArrayN][core][linalg][constructors]", FArray3, FArray4, DArray3, DArray4 )
{
  using for_type = typename TestType::elem_type;

  for_type constexpr scalar = 4.0;

  // from list

  auto arr0 = TestType( { 1, 2, 3 } );
  // from other
  auto arr1 = TestType( arr0 );
  // default
  auto arr2 = TestType();
  // from const arr
  TestType arr3( { 2, 3, 4 } );
  // copy assignment
  arr2 = arr1;
  // from scalar
  TestType arr4( scalar );

  arr0 = { 2, 3, 4 };

  for ( auto i = 0; i < 2; ++i ) {
    REQUIRE( arr0[ i ] - 1 == arr1[ i ] );
    REQUIRE( arr1[ i ] == arr2[ i ] );
    REQUIRE( arr3[ i ] == arr0[ i ] );
    REQUIRE( arr4[ i ] == scalar );
  }

} // TEMPLATE_TEST_CASE

TEMPLATE_TEST_CASE( "Vector Defaults", "[Vector][core][linalg][defaults]", FArray3, FArray4, DArray3, DArray4 )
{
  // default values
  auto arr = TestType();

  REQUIRE( arr.cget()[ 0 ] == 0 );
  REQUIRE( arr.cget()[ 1 ] == 0 );
  REQUIRE( arr.cget()[ 2 ] == 0 );

  if ( TestType::dimension == 4 ) {
    REQUIRE( arr.cget()[ 3 ] == 0 );
  }

  TestType pos{};
  REQUIRE( pos.dimension == TestType::dimension );

} // TEMPLATE_TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN Iterator", "[ArrayN][core][linalg][iterator]", FArray3, FArray4, DArray3, DArray4 )
{
  using for_type = typename TestType::elem_type;

  TestType test( { 7, 8, 9 } );

  // looping
  for_type ref = 7;
  for ( auto val : test ) {
    // For case dimension 4, the last value is default == 0
    if ( ref >= 10 ) {
      ref = 0.0;
    }
    REQUIRE_IS_CLOSEX( val, ref, for_type );
    ref++;
  }

  // bidirectional, referecne operator
  auto * val = test.begin();
  REQUIRE( *val == 7 );
  val += 2;
  REQUIRE( *val == 9 );
  val--;
  REQUIRE( *val == 8 );

  // writing
  for ( auto * val = test.begin(); val != test.end(); ++val ) {
    *val = *val + 1;
    INFO( "*it = " << val );
  }
  REQUIRE( test.end() - test.begin() == TestType::dimension );

  // via []
  REQUIRE_IS_CLOSEX( test[ 0 ], 8, for_type );
  REQUIRE_IS_CLOSEX( test[ 1 ], 9, for_type );
  REQUIRE_IS_CLOSEX( test[ 2 ], 10, for_type );

  // via iterator
  val = test.begin();
  REQUIRE_IS_CLOSEX( *val, 8, for_type );
  REQUIRE_IS_CLOSEX( *( val + 2 ), 10, for_type );

} // TEMPLATE_TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN Norm", "[ArrayN][core][linalg][algebra]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  for_type test_3_gt_sum_of_sq( tv::vec3_gt_square_sum<for_type> );

  REQUIRE_IS_CLOSEX( test_3_gt.sum(), tv::vec3_gt_sum<for_type>, for_type );
  REQUIRE_IS_CLOSEX( test_3_gt.norm(), tv::vec3_gt_l2<for_type>, for_type );

  REQUIRE_IS_CLOSEX( test_3_lt.sum(), tv::vec3_lt_sum<for_type>, for_type );
  REQUIRE_IS_CLOSEX( test_3_lt.norm(), tv::vec3_lt_l2<for_type>, for_type );

  TestType base_x( { 1, 0, 0 } );
  TestType base_y( { 0, 1, 0 } );
  TestType base_z( { 0, 0, 1 } );

  SECTION( "Norm after changes updates itself" )
  {
    test_3_gt[ 0 ] = 0;
    test_3_lt[ 0 ] = 0;

    test_3_gt[ 1 ] = 0;
    test_3_lt[ 1 ] = 0;

    test_3_gt.normalize();
    test_3_lt.normalize();

    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], 1, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], 1, for_type );

    REQUIRE_IS_CLOSEX( test_3_gt.norm(), 1, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt.norm(), 1, for_type );

    REQUIRE_IS_CLOSEX( test_3_gt.sum(), 1, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt.sum(), 1, for_type );

  } // SECTION

  SECTION( "in-place, L2 normalization" )
  {
    test_3_gt.normalize();
    test_3_lt.normalize();

    REQUIRE_IS_CLOSEX( test_3_gt.norm(), 1, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt.norm(), 1, for_type );

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt_normed<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt_normed<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt_normed<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt_normed<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt_normed<for_type>[ 2 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt_normed<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "sum of squares" )
  {
    REQUIRE( test_3_gt.norm() > test_3_lt.sum_of_squares() );
    REQUIRE_IS_CLOSE( test_3_gt.sum_of_squares(), test_3_gt_sum_of_sq );
  } // SECTION
}

TEMPLATE_TEST_CASE( "ArrayN Linalg", "[ArrayN][core][linalg][algebra]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  TestType base_y( { 1, 0, 0 } );
  TestType base_x( { 0, 1, 0 } );
  TestType base_z( { 0, 0, 1 } );

  TestType vec_a( { 0.1, 0, 0 } );
  TestType vec_b( { 0, 2, 0 } );
  TestType cprod( { 0, 2, 0 } );

  SECTION( "member, dot-product" )
  {
    REQUIRE_IS_CLOSEX( test_3_gt.dot( test_3_lt ), test_3_lt.dot( test_3_gt ), for_type );
    REQUIRE_IS_CLOSEX( test_3_gt.dot( test_3_gt ), test_3_gt.norm() * test_3_gt.norm(), for_type );
    REQUIRE_IS_CLOSEX( test_3_gt.dot( test_3_lt ), 1.5, for_type );
  } // SECTION

  SECTION( "member, cross-product" )
  {
    REQUIRE( base_y.cross( base_x ) == base_z );
    REQUIRE( base_x.cross( base_y ) == static_cast<for_type>( -1.0 ) * base_z );
    REQUIRE( base_x.cross( base_y * 2 ) == static_cast<for_type>( -2.0 ) * base_z );

    TestType i( { 0, -1, -2 } );
    TestType j( { 0, 1, 0 } );
    TestType k( { 2, 0, 0 } );

    REQUIRE( i.cross( j ) == k );
  } // SECTION

  SECTION( "member, cross-product, orthogonality" )
  {
    TestType i( { 1, 2, 1 } );
    TestType j( { 2, 1, -4 } );
    i.normalize();
    j.normalize();
    auto k = i.cross( j );
    INFO( "i: " << i.repr() << " j: " << j.repr() << " k: " << k.repr() );
    REQUIRE_IS_CLOSEX( ( j.cross( k ) - i ).sum(), 0.0L, for_type );
    REQUIRE_IS_CLOSEX( ( k.cross( i ) - j ).sum(), 0.0L, for_type );
    REQUIRE_IS_CLOSEX( ( j.cross( i ) - k * -1 ).sum(), 0.0L, for_type );
    REQUIRE_IS_CLOSEX( ( k.cross( j ) - i * -1 ).sum(), 0.0L, for_type );
    REQUIRE_IS_CLOSEX( ( i.cross( k ) - j * -1 ).sum(), 0.0L, for_type );
  }
}

TEMPLATE_TEST_CASE( "ArrayN Trig", "[ArrayN][core][linalg][algebra]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  TestType base_x( { 1, 0, 0 } );
  TestType base_y( { 0, 1, 0 } );

  INFO( "Vec3gt cos Vec3lt = " << test_3_gt.cos( test_3_lt ) );
  SECTION( "member, cos" )
  {
    REQUIRE( test_3_gt.cos( test_3_gt ) == 1 );
    REQUIRE( base_y.cos( base_x ) == 0 );
    REQUIRE_IS_CLOSEX( test_3_gt.cos( test_3_lt ), tv::vec3_lt_cos_vec3_gt<for_type>, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt.cos( test_3_gt ), tv::vec3_lt_cos_vec3_gt<for_type>, for_type );
  } // SECTION

} // TEMPLATE_TEST_CASE

TEMPLATE_TEST_CASE(
    "ArrayN Access underlying data", "[ArrayN][core][linalg][data]", FArray3, DArray3, FArray4, DArray4
)
{
  using for_type = typename TestType::elem_type;

  TestType foo{};

  auto const as_arr = static_cast<std::array<for_type, TestType::dimension>>( foo );

  SECTION( "Conversions" )
  {
    REQUIRE_ALL_CLOSEXX( as_arr, foo, for_type, TestType::dimension ); // NOLINT(*-index)
    REQUIRE( typeid( as_arr ) == typeid( foo.cget() ) );
  }

  SECTION( "Printing" )
  {
    std::ostringstream buf;
    REQUIRE( buf.str().empty() );
    buf << foo.repr();
    REQUIRE( buf.str().size() > 3 );
  }


} // TEMPLATE_TEST_CASE

// NOLINTEND(*-magic-numbers,*-pointer-ar*)
