// NOLINTBEGIN(*-magic-numbers)

#include <catch2/catch_template_test_macros.hpp>

#include "khonsu/linalg/linalg.hpp"

#include "testdata/vectors.hpp"

#include "testutils/macros.hpp"

namespace tv = testvals;

using linalg::DArray3;
using linalg::FArray3;

TEMPLATE_TEST_CASE( "ArrayN SUB", "[ArrayN][core][linalg][arithmetic]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;
  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  for_type constexpr foo = 1.3;

  SECTION( "in-place, scalar" )
  {
    test_3_gt -= foo;
    test_3_lt -= foo;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] - foo, for_type );
  } // SECTION

  SECTION( "in-place, element-wise" )
  {
    test_3_gt -= test_3_lt;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] - tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] - tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] - tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "infix, element-wise" )
  {
    auto result = test_3_gt - test_3_lt;

    REQUIRE_IS_CLOSEX( result[ 0 ], tv::vec3_gt<for_type>[ 0 ] - tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 1 ], tv::vec3_gt<for_type>[ 1 ] - tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 2 ], tv::vec3_gt<for_type>[ 2 ] - tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "infix, scalar 1" )
  {
    auto result_lt = test_3_lt - foo;
    auto result_gt = test_3_gt - foo;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] - foo, for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] - foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] - foo, for_type );

  } // SECTION

  SECTION( "infix, scalar 2" )
  {
    auto result_lt = foo - test_3_lt;
    auto result_gt = foo - test_3_gt;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], foo - tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], foo - tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], foo - tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], foo - tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], foo - tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], foo - tv::vec3_gt<for_type>[ 2 ], for_type );

  } // SECTION

} // TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN ADD", "[ArrayN][core][linalg][arithmetic]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;
  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  for_type constexpr bar = 3.13541;

  SECTION( "in-place, scalar" )
  {
    test_3_gt += bar;
    test_3_lt += bar;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] + bar, for_type );

  } // SECTION

  SECTION( "in-place, element-wise" )
  {
    test_3_gt += test_3_lt;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] + tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] + tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] + tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );

  } // SECTION
  SECTION( "infix, element-wise" )
  {
    auto result = test_3_gt + test_3_lt;

    REQUIRE_IS_CLOSEX( result[ 0 ], tv::vec3_gt<for_type>[ 0 ] + tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 1 ], tv::vec3_gt<for_type>[ 1 ] + tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 2 ], tv::vec3_gt<for_type>[ 2 ] + tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "infix, scalar 1" )
  {
    auto result_lt = test_3_lt + bar;
    auto result_gt = test_3_gt + bar;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] + bar, for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] + bar, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] + bar, for_type );

  } // SECTION

  SECTION( "infix, scalar 2" )
  {
    auto result_lt = bar + test_3_lt;
    auto result_gt = bar + test_3_gt;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], bar + tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], bar + tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], bar + tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], bar + tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], bar + tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], bar + tv::vec3_gt<for_type>[ 2 ], for_type );

  } // SECTION
} // TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN MUL", "[ArrayN][core][linalg][arithmetic]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;
  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  for_type constexpr foo = 1.1112;

  SECTION( "in-place, scalar" )
  {
    test_3_gt *= foo;
    test_3_lt *= foo;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] * foo, for_type );

  } // SECTION

  SECTION( "in-place, element-wise" )
  {
    test_3_gt *= test_3_lt;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] * tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] * tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] * tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );

  } // SECTION
  SECTION( "infix, element-wise" )
  {
    auto result = test_3_gt * test_3_lt;

    REQUIRE_IS_CLOSEX( result[ 0 ], tv::vec3_gt<for_type>[ 0 ] * tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 1 ], tv::vec3_gt<for_type>[ 1 ] * tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 2 ], tv::vec3_gt<for_type>[ 2 ] * tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "infix, scalar 1" )
  {
    auto result_lt = test_3_lt * foo;
    auto result_gt = test_3_gt * foo;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] * foo, for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] * foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] * foo, for_type );

  } // SECTION

  SECTION( "infix, scalar 2" )
  {
    auto result_lt = foo * test_3_lt;
    auto result_gt = foo * test_3_gt;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], foo * tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], foo * tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], foo * tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], foo * tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], foo * tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], foo * tv::vec3_gt<for_type>[ 2 ], for_type );

  } // SECTION
} // TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN DIV", "[ArrayN][core][linalg][arithmetic]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  TestType test_3_gt( tv::vec3_gt<for_type> );
  TestType test_3_lt( tv::vec3_lt<for_type> );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  for_type constexpr foo = 1.12358;

  SECTION( "in-place, scalar" )
  {
    test_3_gt /= foo;
    test_3_lt /= foo;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] / foo, for_type );

  } // SECTION

  SECTION( "in-place, element-wise" )
  {
    test_3_gt /= test_3_lt;

    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] / tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] / tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] / tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );

  } // SECTION
  SECTION( "infix, element-wise" )
  {
    auto result = test_3_gt / test_3_lt;

    REQUIRE_IS_CLOSEX( result[ 0 ], tv::vec3_gt<for_type>[ 0 ] / tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 1 ], tv::vec3_gt<for_type>[ 1 ] / tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result[ 2 ], tv::vec3_gt<for_type>[ 2 ] / tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( test_3_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( test_3_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ], for_type );
  } // SECTION

  SECTION( "infix, scalar 1" )
  {
    auto result_lt = test_3_lt / foo;
    auto result_gt = test_3_gt / foo;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], tv::vec3_lt<for_type>[ 0 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], tv::vec3_lt<for_type>[ 1 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], tv::vec3_lt<for_type>[ 2 ] / foo, for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], tv::vec3_gt<for_type>[ 0 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], tv::vec3_gt<for_type>[ 1 ] / foo, for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], tv::vec3_gt<for_type>[ 2 ] / foo, for_type );

  } // SECTION

  SECTION( "infix, scalar 2" )
  {
    auto result_lt = foo / test_3_lt;
    auto result_gt = foo / test_3_gt;

    REQUIRE_IS_CLOSEX( result_lt[ 0 ], foo / tv::vec3_lt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 1 ], foo / tv::vec3_lt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_lt[ 2 ], foo / tv::vec3_lt<for_type>[ 2 ], for_type );

    REQUIRE_IS_CLOSEX( result_gt[ 0 ], foo / tv::vec3_gt<for_type>[ 0 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 1 ], foo / tv::vec3_gt<for_type>[ 1 ], for_type );
    REQUIRE_IS_CLOSEX( result_gt[ 2 ], foo / tv::vec3_gt<for_type>[ 2 ], for_type );

  } // SECTION
} // TEST_CASE

TEMPLATE_TEST_CASE( "ArrayN CMP", "[ArrayN][core][linalg][compare]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  TestType test_3_lt( tv::vec3_lt<for_type> );

  SECTION( "bool, element-wise" )
  {
    REQUIRE_ALL_CLOSE3( test_3_lt, tv::vec3_lt<for_type>, for_type );
    REQUIRE_NOT_ALL_CLOSE3( test_3_lt, tv::vec3_gt<for_type>, for_type );
  } // SECTION

  TestType foobar{};
  foobar[ TestType::dimension - 1 ] = 10.0;

  TestType fizzbuzz{ foobar };

  SECTION( "equal operator" )
  {
    REQUIRE( foobar == fizzbuzz );

    fizzbuzz[ TestType::dimension - 1 ] = 5.0;
    REQUIRE( foobar != fizzbuzz );

  } // SECTION

} // TEST_CASE

// NOLINTEND(*-magic-numbers)
