// NOLINTBEGIN(*-magic-numbers)

#include <catch2/catch_template_test_macros.hpp>

#include "khonsu/linalg/linalg.hpp"

#include "testdata/vectors.hpp"

#include "testutils/macros.hpp"

using linalg::DArray3;
using linalg::FArray3;
using linalg::GenericCoordBase;

using basisvals::arr_depth3;
using basisvals::arr_hor3;
using basisvals::arr_vert3;
using basisvals::arr_zero3;

TEMPLATE_TEST_CASE( "CoordBase Default", "[CoordBase][core][linalg][constructors]", FArray3, DArray3 )
{
  using for_type = typename TestType::elem_type;

  auto zero = GenericCoordBase<TestType>();
  TestType a_pos( { 1, 2, 3 } );

  REQUIRE_ALL_CLOSE3( zero.ydir(), arr_vert3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( zero.xdir(), arr_hor3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( zero.zdir(), arr_depth3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( zero.pos(), arr_zero3<for_type>, for_type );
  REQUIRE( zero.ydir().cross( zero.xdir() ) == zero.zdir() );

  // pos only
  auto moved = GenericCoordBase<TestType>( a_pos );
  REQUIRE( moved.pos() == a_pos );

  // orientation only
  auto oriented = GenericCoordBase<TestType>( TestType( arr_vert3<for_type> ), TestType( arr_depth3<for_type> ) );
  REQUIRE_ALL_CLOSE3( oriented.ydir(), arr_vert3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( oriented.xdir(), arr_hor3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( oriented.zdir(), arr_depth3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( oriented.pos(), arr_zero3<for_type>, for_type );

  // flipped only
  auto flipped = GenericCoordBase<TestType>(
      static_cast<for_type>( -1.0 ) * TestType( arr_vert3<for_type> ), TestType( arr_depth3<for_type> )
  );
  REQUIRE( flipped.ydir() == static_cast<for_type>( -1.0 ) * TestType( arr_vert3<for_type> ) );
  REQUIRE( flipped.xdir() == static_cast<for_type>( -1.0 ) * TestType( arr_hor3<for_type> ) );
  REQUIRE_ALL_CLOSE3( flipped.zdir(), arr_depth3<for_type>, for_type );
  REQUIRE_ALL_CLOSE3( flipped.pos(), arr_zero3<for_type>, for_type );

  // check normalization only
  auto rnd = GenericCoordBase<TestType>( TestType( arr_vert3<for_type> ) * 2, a_pos );
  REQUIRE_IS_CLOSEX( rnd.ydir().norm(), 1, for_type );
  REQUIRE_IS_CLOSEX( rnd.xdir().norm(), 1, for_type );
  REQUIRE_IS_CLOSEX( rnd.zdir().norm(), 1, for_type );

  SECTION( "Flip dir changes sign only" )
  {
    zero.flip_ydir();
    zero.flip_xdir();
    zero.flip_zdir();

    auto flipped_vert = TestType( arr_vert3<for_type> ) * -1.0;
    REQUIRE_ALL_CLOSEX( zero.ydir(), flipped_vert, for_type );

    auto flipped_hor = TestType( arr_hor3<for_type> ) * -1.0;
    REQUIRE_ALL_CLOSEX( zero.xdir(), flipped_hor, for_type );

    auto flipped_depth = TestType( arr_depth3<for_type> ) * -1.0;
    REQUIRE_ALL_CLOSEX( zero.zdir(), flipped_depth, for_type );
  }
}
