// NOLINTBEGIN(*-magic-numbers,*-pointer-ar*)

#include <type_traits>

#include <catch2/catch_template_test_macros.hpp>

#include "khonsu/linalg/material.hpp" // IWYU pragma: keep

using material::GenericColor;
using material::RgbFloat32bpc;
using material::RgbUint16bpc;

TEMPLATE_TEST_CASE( "Color RGB", "[Color][core]", GenericColor<RgbUint16bpc>, GenericColor<RgbFloat32bpc> )
{
  using for_type = typename TestType::elem_type;

  TestType foo{ static_cast<for_type>( 1.0 ) };
  TestType bar{ static_cast<for_type>( 1.0 ) };

  SECTION( "Blending colors is additive" )
  {
    auto foobar = foo.blend( bar );

    REQUIRE( foobar[ 0 ] != foo[ 0 ] );
    REQUIRE( foobar[ 1 ] > foo[ 1 ] );
    REQUIRE( foobar[ 2 ] > foo[ 2 ] );
  }

  SECTION( "Colors from vector" )
  {
    linalg::Vector const as_vec{ { 1, 2, 3 } };

    auto as_col = TestType::from_vector( as_vec );

    REQUIRE( as_col[ 0 ] == 1 );
    REQUIRE( as_col[ 1 ] == 2 );
    REQUIRE( as_col[ 2 ] == 3 );
  }

  SECTION( "Colors from array" )
  {
    std::array<for_type, 3> const as_arr = { 1, 2, 3 };

    auto as_col = TestType{ as_arr };

    REQUIRE( as_col[ 0 ] == 1 );
    REQUIRE( as_col[ 1 ] == 2 );
    REQUIRE( as_col[ 2 ] == 3 );
  }
}
