// NOLINTBEGIN(*-magic-numbers)

#include <cmath>
#include <cstddef>

#include <catch2/catch_template_test_macros.hpp>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"

#include "testutils/macros.hpp"

TEST_CASE( "Ray", "[Ray][core][linalg]" )
{
  using linalg::Ray;
  using linalg::Vector;
  using material::Color;

  Vector at_pos1{ 1 };
  Vector at_pos2{ 2 };
  Vector to_dir1{ { 0, 3, 0 } };
  Vector to_norm1{ { 0, 1, 0 } };
  Vector to_dir2{ { 2, 0, 0 } };
  Vector to_norm2{ { 1, 0, 0 } };

  Vector point1{ { 1, 1, 1 } };
  Vector point2{ { 1, 1, 3 } };
  Vector dir_pt12{ { 0, 0, 1 } };


  constexpr auto red = 0.1;
  constexpr auto green = 0.2;
  constexpr auto blue = 0.3;
  Color color{ { red, green, blue } };

  Ray ray{ at_pos1, to_dir1 };

  SECTION( "Ray dir is normalized and is at pos" )
  {
    REQUIRE_ALL_CLOSE( ray.dir(), to_norm1 );
    REQUIRE_ALL_CLOSE( ray.pos(), at_pos1 );
  }

  SECTION( "Setting/adding color" )
  {
    ray.add_color( color );
    REQUIRE_ALL_CLOSE( ray.color(), color );

    ray.add_color( color );
    REQUIRE_ALL_GT( ray.color(), color );
  }

  SECTION( "Resetting clears color and moves" )
  {
    ray.add_color( color );
    ray.reset_color();
    REQUIRE_ALL_CLOSE( ray.color(), material::Color{ 0 } );
  }

  SECTION( "Moving changes pos or dir and pos" )
  {
    ray.move( at_pos2, to_dir2 );
    REQUIRE_ALL_CLOSE( ray.pos(), at_pos2 );
    REQUIRE_ALL_CLOSE( ray.dir(), to_norm2 );

    ray.move( at_pos1 );
    REQUIRE_ALL_CLOSE( ray.pos(), at_pos1 );
  }

  SECTION( "Pointing at things" )
  {
    ray.move( point1 );
    ray.point_at( point2 );

    REQUIRE_ALL_CLOSE( ray.pos(), point1 );
    REQUIRE_ALL_CLOSE( ray.dir(), dir_pt12 );
  }

  SECTION( "Things pointed at are copied" )
  {
    ray.move( point1 );
    REQUIRE_ALL_CLOSE( ray.dir(), to_norm1 );

    ray.point_at( point2 );

    INFO( "Ref: " << ray.ref_point() << " should: " << point2 );
    REQUIRE_ALL_CLOSE( ray.ref_point(), point2 );
  }

  SECTION( "Setting Image position / initials are 0" )
  {
    std::size_t constexpr some_row = 10;
    std::size_t constexpr some_col = 2000;

    // initialized to 0
    REQUIRE( ray.image_row() == 0 );
    REQUIRE( ray.image_col() == 0 );

    ray.set_image_pos( some_row, some_col );

    REQUIRE( ray.image_row() == some_row );
    REQUIRE( ray.image_col() == some_col );
  }

  SECTION( "At distance from pos() along dir()" )
  {
    linalg::FloatType constexpr to_t = 3.0;

    ray.point_at( Vector( { 2, 2, 2 } ) );

    auto at_dist3 = ray.at_distance( to_t );
    REQUIRE_IS_CLOSE( at_dist3.norm() - std::sqrt( 3 ), to_t );

    Vector quadrup( { 2 + 1, 10 + 1, 11 + 1 } );
    ray.point_at( quadrup );
    auto at_dist_quad = ray.at_distance( 15 );
    REQUIRE_ALL_CLOSE( at_dist_quad, quadrup );
  }
}

// NOLINTEND(*-magic-numbers)
