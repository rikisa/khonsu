// NOLINTBEGIN(*-magic-numbers)

#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"

#include "testutils/macros.hpp"

TEST_CASE( "Intersection", "[unittest][Ray][Intersection][core][linalg]" )
{
  using linalg::FloatType;
  using linalg::Intersection;
  using linalg::Ray;
  using linalg::Vector;

  Intersection intersect{};

  SECTION( "Value t is set from set_at()" )
  {
    FloatType constexpr to_t = 5.0;

    Ray ray{};
    ray.move( Vector{ 0 }, Vector{ { 0, 0, 1 } } );

    intersect.set_at( ray, to_t );

    REQUIRE_IS_CLOSE( intersect.at_t(), to_t );
  }

  SECTION( "Point is set from set_at()" )
  {
    FloatType constexpr to_t = 4.0;
    Vector const ref{ { 0, 0, to_t } };

    Ray ray{};
    ray.move( Vector{ 0 }, Vector{ { 0, 0, 1 } } );

    intersect.set_at( ray, to_t );

    REQUIRE_ALL_CLOSE( intersect.at_point(), ref );
  }

  SECTION( "Setting normal - move" )
  {
    Vector const ref{ { 1, 2, 3 } };

    Vector some_normal{ ref };
    intersect.set_normal( std::move( some_normal ) );

    REQUIRE_ALL_CLOSE( intersect.with_normal(), ref );
  }

  SECTION( "Setting normal - inplace" )
  {
    Vector const ref{ { 3, 2, 1 } };

    intersect.set_normal( Vector{ { 3, 2, 1 } } );

    REQUIRE_ALL_CLOSE( intersect.with_normal(), ref );
  }
}

// NOLINTEND(*-magic-numbers)
