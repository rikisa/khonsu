#include <concepts>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "khonsu/part/basetracer.hpp"
#include "khonsu/part/baseviewer.hpp"
#include "khonsu/part/partconfig.hpp"
#include "khonsu/partregistry.hpp"

#include "imitates/engine/fakecontroller.hpp"
#include "imitates/engine/faketracer.hpp"
#include "imitates/engine/fakeviewer.hpp"

TEST_CASE( "Testing Registry for Engine Parts", "[PartRegistry][Util]" )
{
  using engine::PartConfig;
  using engine::PartRegistry;

  using imitates::engine::FakeController;
  using imitates::engine::FakeTracer;
  using imitates::engine::FakeViewerWithConf;

  PartRegistry reg{};

  SECTION( "Register Parts" )
  {
    constexpr auto foo_name = "FakeViewer";
    constexpr auto bar_name = "FakeTracer";
    constexpr auto unregistered = "unregistered";

    reg.register_part<FakeViewerWithConf>( foo_name );
    reg.register_part<FakeTracer>( bar_name );
    auto keys = reg.registered();

    REQUIRE( keys.find( foo_name ) != keys.end() );
    REQUIRE( keys.find( bar_name ) != keys.end() );
    REQUIRE( keys.find( unregistered ) == keys.end() );

  }; // SECTION

  SECTION( "Register Part and Create Part and Config" )
  {
    constexpr auto foo_name = "FakeViewer";
    constexpr auto unregistered = "unregistered";

    using BaseFoo = engine::part::BaseViewer;
    using ExtFoo = imitates::engine::FakeViewerWithConf;

    reg.register_part<ExtFoo>( foo_name );
    auto keys = reg.registered();

    REQUIRE( keys.find( foo_name ) != keys.end() );
    REQUIRE( keys.find( unregistered ) == keys.end() );

    auto foo_conf = reg.create_config( foo_name );
    REQUIRE( foo_conf.has_value() );
    auto foo_part = reg.create_part<BaseFoo>( foo_name, foo_conf.value() ); // NOLINT(*-access)
    REQUIRE( foo_part.has_value() );

  }; // SECTION

  SECTION( "Get Config of Registered" )
  {
    constexpr auto foo_name = "FakeViewer";

    reg.register_part<FakeViewerWithConf>( foo_name );

    auto default_config = FakeViewerWithConf::create_config();
    auto default_foo = default_config.get_parameter( "foo" ).value(); // NOLINT(*-access)
    REQUIRE_FALSE( default_foo.value.has_value() );

    auto reg_config = reg.create_config( foo_name );
    REQUIRE( reg_config.has_value() );
    auto reg_foo = reg_config.value().get_parameter( "foo" ).value(); // NOLINT(*-access)
    REQUIRE_FALSE( reg_foo.value.has_value() );

  }; // SECTION

  SECTION( "Create part from config" )
  {
    constexpr auto foo_name = "FakeViewer";
    constexpr int foo_val = 42;

    using BaseFoo = engine::part::BaseViewer;
    using ExtFoo = FakeViewerWithConf;

    reg.register_part<ExtFoo>( foo_name );
    auto the_config = FakeViewerWithConf::create_config();
    the_config.set_parameter<int>( "foo", foo_val );

    auto fake_part_ref = reg.create_part<BaseFoo>( foo_name, the_config ).value(); // NOLINT(*-access)
    auto fake_part = std::dynamic_pointer_cast<FakeViewerWithConf>( fake_part_ref );

    REQUIRE( fake_part->get_foo().value() == foo_val ); // NOLINT(*-access)

  }; // SECTION

  SECTION( "Create config for unregistered" )
  {
    constexpr auto foo_name = "INVALID";

    auto maybe_config = reg.create_config( foo_name );

    REQUIRE( maybe_config.has_value() == false );

  }; // SECTION
     //
  SECTION( "Create part for unregistered" )
  {
    constexpr auto foo_name = "INVALID";

    auto maybe_config = reg.create_part<engine::part::BaseViewer>( foo_name, { foo_name } );

    REQUIRE( maybe_config.has_value() == false );

  }; // SECTION

} // TEST_CASE
