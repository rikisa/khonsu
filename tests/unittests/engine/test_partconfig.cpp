#include <catch2/catch_test_macros.hpp>

#include "khonsu/part/partconfig.hpp"

TEST_CASE( "Testing config for BaseParts", "[PartConfig][Util]" )
{
  using BaseConfig = engine::PartConfig;

  using FooType = int;
  auto const foo_key = std::string{ "foo" };
  auto const config_name = std::string{ "dummy" };
  FooType constexpr foo_val = 13;
  std::type_index foo_th = typeid( FooType );

  using BarType = std::string;
  auto const bar_key = std::string{ "bar" };
  BarType const bar_val = "carl";
  std::type_index bar_th = typeid( BarType );

  BaseConfig conf{ config_name };

  SECTION( "Adding Parameter with default and get it" )
  {
    conf.add_parameter( foo_key, foo_val );

    auto param = conf.get_parameter( foo_key ).value();
    REQUIRE( param.name == foo_key );
    REQUIRE( param.type_hint.hash_code() == foo_th.hash_code() );
    REQUIRE( std::any_cast<FooType>( param.value ) == foo_val );

  }; // SECTION

  SECTION( "Adding parameter w/o default and get it" )
  {
    conf.add_parameter<BarType>( bar_key );

    // NOLINTNEXTLINE(*-access)
    auto param = conf.get_parameter( bar_key ).value();
    REQUIRE( param.type_hint.hash_code() == bar_th.hash_code() );
    REQUIRE( param.value.has_value() == false );

  }; // SECTION

  SECTION( "Get not existing parameter" )
  {
    conf.add_parameter<BarType>( bar_key );

    auto param = conf.get_parameter( foo_key );
    REQUIRE( param.has_value() == false );

  }; // SECTION

  SECTION( "Adding parameter w/o default, set it and get it" )
  {
    conf.add_parameter<BarType>( bar_key );

    auto param = conf.get_parameter( bar_key ).value();
    REQUIRE( param.value.has_value() == false );
    param.value = bar_val;

    auto updated_param = conf.get_parameter( bar_key ).value();
    REQUIRE( std::any_cast<BarType>( param.value ) == bar_val );
    REQUIRE( param.type_hint.hash_code() == bar_th.hash_code() );

  }; // SECTION

  SECTION( "Adding parameter w/o default, set it and get it 2" )
  {
    conf.add_parameter<FooType>( foo_key );
    auto init_foo_param = conf.get_parameter( foo_key ).value();
    REQUIRE_FALSE( init_foo_param.value.has_value() );

    conf.set_parameter( foo_key, foo_val );

    auto new_foo_param = conf.get_parameter( foo_key ).value();
    REQUIRE( std::any_cast<FooType>( new_foo_param.value ) == foo_val );

  }; // SECTION

  SECTION( "Setting invalid parameter fails" )
  {
    REQUIRE( conf.set_parameter<double>( "INVALID", 0.0 ) == false );
    REQUIRE( conf.set_parameter<int>( "INVALID", -1 ) == false );
    REQUIRE( conf.set_parameter<std::string>( "INVALID", "hurr" ) == false );
  }; // SECTION

  SECTION( "Set/Change value of existing parameter" )
  {
    std::string another_val = bar_val + bar_val;
    conf.add_parameter<BarType>( bar_key, bar_val );

    REQUIRE( std::any_cast<BarType>( conf.get_parameter( bar_key ).value().value ) == bar_val );

    conf.set_parameter( bar_key, another_val );
    REQUIRE( std::any_cast<BarType>( conf.get_parameter( bar_key ).value().value ) != bar_val );
    REQUIRE( std::any_cast<BarType>( conf.get_parameter( bar_key ).value().value ) == another_val );
  }; // SECTION
} // TEST_CASE

TEST_CASE( "Invalid configs", "[PartConfig][exception][util]" )
{
  engine::PartConfig config{ "invalid" };

  using FooType = int;
  auto const foo_key = std::string{ "foo" };

  SECTION( "Accessing not defined fields throws" )
  {
    REQUIRE_THROWS_AS( config.get_as_or_throw<FooType>( foo_key ), engine::config_error );
  }

  SECTION( "Accessing field with no value throws" )
  {
    config.add_parameter<FooType>( foo_key );
    REQUIRE_THROWS_AS( config.get_as_or_throw<FooType>( foo_key ), engine::config_error );
  }

} // TEST_CASE
