#include <memory>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "khonsu/khonsu.hpp"
#include "khonsu/part/basecontroller.hpp"
#include "khonsu/part/partconfig.hpp"

#include "imitates/engine/fakecontroller.hpp"
#include "imitates/engine/faketracer.hpp"
#include "imitates/engine/fakeviewer.hpp"
#include "imitates/engine/mockcontroller.hpp"
#include "imitates/engine/mockpartregistry.hpp"
#include "imitates/engine/mockviewer.hpp"

TEST_CASE( "Using Khonsu Interface", "[Khonsu][Util]" )
{
  auto reg = std::make_shared<imitates::engine::MockPartRegistry>();

  SECTION( "Unconfigured does not start" )
  {
    auto reg = std::make_shared<imitates::engine::MockPartRegistry>();
    engine::Khonsu khonsu{ reg };
    REQUIRE( khonsu.start() == false );
  }

  SECTION( "Khonsu throws on invalid controller config" )
  {
    std::string const invalid{ "INVALID" };
    REQUIRE_CALL( *reg.get(), _create_part( invalid, engine::PartConfig{ invalid } ) ).RETURN( std::any() );

    engine::Khonsu khonsu{ reg };
    REQUIRE_THROWS( khonsu.from_part_configs( { invalid }, { invalid }, { invalid } ) );
    REQUIRE( khonsu.start() == false );
  }

  SECTION( "Khonsu throws on invalid viewer config" )
  {
    std::string const invalid{ "INVALID" };
    std::string const baz_ct_name{ "BazCt" };


    REQUIRE_CALL( *reg.get(), _create_part( invalid, engine::PartConfig{ invalid } ) ).RETURN( std::any() );
    REQUIRE_CALL( *reg.get(), _create_part( baz_ct_name, engine::PartConfig{ baz_ct_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseController>(
            std::make_shared<imitates::engine::FakeController>()
        ) ) );

    engine::Khonsu khonsu{ reg };
    REQUIRE_THROWS( khonsu.from_part_configs( { baz_ct_name }, { invalid }, { invalid } ) );
    REQUIRE( khonsu.start() == false );
  }

  SECTION( "Khonsu throws on invalid tracer config" )
  {
    std::string const invalid{ "INVALID" };
    std::string const baz_ct_name{ "BazCt" };
    std::string const bar_vi_name{ "BarVi" };


    REQUIRE_CALL( *reg.get(), _create_part( invalid, engine::PartConfig{ invalid } ) ).RETURN( std::any() );
    REQUIRE_CALL( *reg.get(), _create_part( baz_ct_name, engine::PartConfig{ baz_ct_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseController>(
            std::make_shared<imitates::engine::FakeController>()
        ) ) );
    REQUIRE_CALL( *reg.get(), _create_part( bar_vi_name, engine::PartConfig{ bar_vi_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseViewer>(
            std::make_shared<imitates::engine::FakeViewerWithConf>()
        ) ) );

    engine::Khonsu khonsu{ reg };
    REQUIRE_THROWS( khonsu.from_part_configs( { baz_ct_name }, { bar_vi_name }, { invalid } ) );
    REQUIRE( khonsu.start() == false );
  }

  SECTION( "Create throws on wrong type for part config" )
  {
    std::string const foo_tr_name{ "FooTr" };
    std::string const bar_vi_name{ "BarVi" };
    std::string const baz_ct_name{ "BazCt" };


    REQUIRE_CALL( *reg.get(), _create_part( baz_ct_name, engine::PartConfig{ baz_ct_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseController>(
            std::make_shared<imitates::engine::FakeController>()
        ) ) );

    REQUIRE_CALL( *reg.get(), _create_part( foo_tr_name, engine::PartConfig{ foo_tr_name } ) )
        .RETURN( std::any(
            std::dynamic_pointer_cast<engine::part::BaseTracer>( std::make_shared<imitates::engine::FakeTracer>() )
        ) );


    engine::Khonsu khonsu{ reg };
    REQUIRE_THROWS( khonsu.from_part_configs( baz_ct_name, foo_tr_name, bar_vi_name ) );

    REQUIRE( khonsu.start() == false );
  }

  SECTION( "Create from valid PartConfigs" )
  {
    std::string const foo_tr_name{ "FooTr" };
    std::string const bar_vi_name{ "BarVi" };
    std::string const baz_ct_name{ "BazCt" };


    REQUIRE_CALL( *reg.get(), _create_part( baz_ct_name, engine::PartConfig{ baz_ct_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseController>(
            std::make_shared<imitates::engine::FakeController>()
        ) ) );

    REQUIRE_CALL( *reg.get(), _create_part( bar_vi_name, engine::PartConfig{ bar_vi_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseViewer>(
            std::make_shared<imitates::engine::FakeViewerWithConf>()
        ) ) );

    REQUIRE_CALL( *reg.get(), _create_part( foo_tr_name, engine::PartConfig{ foo_tr_name } ) )
        .RETURN( std::any(
            std::dynamic_pointer_cast<engine::part::BaseTracer>( std::make_shared<imitates::engine::FakeTracer>() )
        ) );


    engine::Khonsu khonsu{ reg };
    khonsu.from_part_configs( baz_ct_name, bar_vi_name, foo_tr_name );

    REQUIRE( khonsu.start() == true );
  }

  SECTION( "Start calls init on controller and viewer" )
  {
    using trompeloeil::_;

    std::string const foo_tr_name{ "FooTr" };
    std::string const bar_vi_name{ "BarVi" };
    std::string const baz_ct_name{ "BazCt" };

    auto mock_controller = std::make_shared<imitates::engine::MockPartController>();
    auto mock_viewer = std::make_shared<imitates::engine::MockPartViewer>();

    REQUIRE_CALL( *mock_controller.get(), use_tracer( _ ) ).TIMES( 1 );
    REQUIRE_CALL( *mock_controller.get(), use_viewer( _ ) ).TIMES( 1 );
    REQUIRE_CALL( *mock_controller.get(), init() ).TIMES( 1 );
    REQUIRE_CALL( *mock_viewer.get(), init() ).TIMES( 1 );

    REQUIRE_CALL( *reg.get(), _create_part( baz_ct_name, engine::PartConfig{ baz_ct_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseController>( mock_controller ) ) );

    REQUIRE_CALL( *reg.get(), _create_part( bar_vi_name, engine::PartConfig{ bar_vi_name } ) )
        .RETURN( std::any( std::dynamic_pointer_cast<engine::part::BaseViewer>( mock_viewer ) ) );

    REQUIRE_CALL( *reg.get(), _create_part( foo_tr_name, engine::PartConfig{ foo_tr_name } ) )
        .RETURN( std::any(
            std::dynamic_pointer_cast<engine::part::BaseTracer>( std::make_shared<imitates::engine::FakeTracer>() )
        ) );


    engine::Khonsu khonsu{ reg };
    khonsu.from_part_configs( baz_ct_name, bar_vi_name, foo_tr_name );

    REQUIRE( khonsu.start() == true );
  }
}
