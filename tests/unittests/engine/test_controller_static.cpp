#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "khonsu/controller/staticcontroller.hpp"

#include "imitates/engine/mocktracer.hpp"
#include "imitates/engine/mockviewer.hpp"

TEST_CASE( "Testing StaticController", "[Static][Controller]" )
{
  using trompeloeil::_;

  using controller::StaticControl;
  using imitates::engine::MockPartTracer;
  using imitates::engine::MockPartViewer;

  StaticControl ctrl{};

  auto tracer = std::make_shared<MockPartTracer>();
  auto viewer = std::make_shared<MockPartViewer>();
  ctrl.use_tracer( tracer );
  ctrl.use_viewer( viewer );

  SECTION( "StaticControl throws if viewer/tracer are not set" )
  {
    StaticControl no_tracer{};
    REQUIRE_THROWS( no_tracer.init() );
    no_tracer.use_viewer( viewer );
    REQUIRE_THROWS( no_tracer.init() );

    StaticControl no_viewer{};
    no_viewer.use_tracer( tracer );
    REQUIRE_THROWS( no_viewer.init() );
  }; // SECTION

  SECTION( "StaticControl sets Image for Tracer and Viewer then traces scene" )
  {
    REQUIRE_CALL( *tracer.get(), write_image( _ ) );
    REQUIRE_CALL( *tracer.get(), set_camera( _ ) );
    REQUIRE_CALL( *tracer.get(), trace_scene( _ ) );

    REQUIRE_CALL( *viewer.get(), read_image( _ ) );
    REQUIRE_CALL( *viewer.get(), show() );

    ctrl.init();

  }; // SECTION

} // TEST_CASE
