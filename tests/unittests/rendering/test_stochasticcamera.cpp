#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/rendering/stochasticcamera.hpp"

#include "testutils/macros.hpp"


using linalg::FloatType;
using linalg::Ray;

// NOLINTBEGIN(*-magic-numbers)
TEST_CASE( "StochasticCamera", "[unittest][stochastic][camera][rendering]" )
{
  std::size_t pixel_samples = GENERATE( 3, 4 );

  rendering::StochasticCamera cam{ pixel_samples };

  SECTION( "Size is multiplied by sample" )
  {
    std::size_t img_height = GENERATE( 10, 11 );
    std::size_t img_width = GENERATE( 7, 8 );

    cam.set_shape( img_height, img_width, 1.0 );

    REQUIRE( cam.size() == img_width * img_height * pixel_samples );
  }

  SECTION( "Ray order is kept" )
  {
    std::size_t img_height = GENERATE( 10, 11 );
    std::size_t img_width = GENERATE( 7, 8 );

    auto px_col_dist = static_cast<FloatType>( img_width ) / static_cast<FloatType>( img_height ) /
        static_cast<FloatType>( img_width );

    Ray ray1{};
    Ray ray2{};
    Ray ray3{};

    cam.set_shape( img_height, img_width, 1.0 );

    cam.update_ray( ray1, 1 );
    cam.update_ray( ray2, 2 );
    cam.update_ray( ray3, 2 * img_width + 5 );

    // in same row, within pixel width
    INFO( img_width << " x " << img_height << " " << px_col_dist );
    REQUIRE( std::abs( ray1.dir()[ 0 ] - ray2.dir()[ 0 ] ) <= px_col_dist );
    // ray2 in next column
    REQUIRE( ray1.dir()[ 1 ] < ray2.dir()[ 1 ] );
    // ray3 in next row
    REQUIRE( ray3.dir()[ 0 ] < ray2.dir()[ 0 ] );
  }

  SECTION( "Rays dirs are distributed" )
  {
    std::size_t img_height = GENERATE( 10, 11 );
    std::size_t img_width = GENERATE( 7, 8 );

    Ray ray1{};
    Ray ray2{};

    cam.set_shape( img_height, img_width, 1.0 );

    cam.update_ray( ray1, 4 );
    cam.update_ray( ray2, 4 );

    REQUIRE_ALL_CLOSE( ray1.pos(), ray2.pos() );

    REQUIRE_NONE_CLOSE( ray1.dir(), ray2.dir() );
  }
}

// NOLINTEND(*-magic-numbers)
