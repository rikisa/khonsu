// NOLINTBEGIN(*-magic-numbers,*-pointer-arithmetic)

#include <memory>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/rendering/scene.hpp"

#include "imitates/rendering/mockobject.hpp"

#define _ trompeloeil::_

TEST_CASE( "Scene", "[unittest][scene][rendering]" )
{
  rendering::Scene scene{};

  SECTION( "Added objects are traced" )
  {
    auto obj1 = std::make_shared<imitates::rendering::MockObject>();
    auto obj2 = std::make_shared<imitates::rendering::MockObject>();

    scene.add_object( obj1 );
    scene.add_object( obj2 );

    REQUIRE_CALL( *obj1.get(), get_intersect( _, _, _, _ ) ).RETURN( true );
    REQUIRE_CALL( *obj2.get(), get_intersect( _, _, _, _ ) ).RETURN( true );

    linalg::Ray ray{};
    linalg::Intersection intersect{};
    auto material = std::make_shared<material::BaseMaterial>();
    scene.intersect( ray, intersect );
  }
}

// NOLINTEND(*-magic-numbers,*-pointer-arithmetic)
