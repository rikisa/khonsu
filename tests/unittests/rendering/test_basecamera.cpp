#include <cstddef>
#include <numbers>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/linalg/types.hpp"
#include "khonsu/rendering/basecamera.hpp"

#include "testutils/macros.hpp"

using linalg::FloatType;

// NOLINTBEGIN(*-magic-numbers)
TEST_CASE( "BaseCamera", "[base][camera][rendering]" )
{
  rendering::BaseCamera cam{};

  SECTION( "Normalizes to height" )
  {
    std::size_t pixel_height = GENERATE( 10, 11 );
    std::size_t pixel_width = GENERATE( 7, 8 );

    FloatType constexpr fov = std::numbers::pi_v<FloatType> * 0.5; // 90 deg

    cam.set_shape( pixel_height, pixel_width, fov );

    REQUIRE_IS_CLOSE( cam.height(), 1.0 );
  }

  SECTION( "Width is aspect" )
  {
    std::size_t pixel_height = GENERATE( 10, 11 );
    std::size_t pixel_width = GENERATE( 5, 8 );

    FloatType constexpr fov = std::numbers::pi_v<FloatType> * 0.5; // 90 deg

    cam.set_shape( pixel_height, pixel_width, fov );

    auto aspect = static_cast<FloatType>( pixel_width ) / static_cast<FloatType>( pixel_height );
    REQUIRE_IS_CLOSE( cam.width(), aspect );
  }

  SECTION( "Size is number of pixels" )
  {
    std::size_t constexpr px_height = 2;
    std::size_t constexpr px_width = 3;
    cam.set_shape( px_height, px_width, 1 );

    REQUIRE( cam.size() == px_height * px_width );
  }

  SECTION( "Default orientation - directions" )
  {
    // FOV ~ 53.13 deg ~ 0.9272952180016122 rad
    cam.set_shape( 1, 2, 0.9272952180016122 );

    REQUIRE_ALL_CLOSE( cam.pos(), linalg::Vector( 0 ) );
    REQUIRE_ALL_CLOSE( cam.eye(), linalg::Vector( { 0, 0, -1 } ) );

    REQUIRE_ALL_CLOSE( cam.viewdir(), linalg::Vector( { 0, 0, 1 } ) );
    REQUIRE_ALL_CLOSE( cam.row_dir(), linalg::Vector( { -1, 0, 0 } ) );
    REQUIRE_ALL_CLOSE( cam.col_dir(), linalg::Vector( { 0, 1, 0 } ) );
  }

  SECTION( "Image origins" )
  {
    std::size_t px_rows = GENERATE( 2, 3, 6, 10 );
    std::size_t px_cols = GENERATE( 4, 3, 5, 6, 12 );
    FloatType fov = GENERATE( 0.20, 0.92, 1.11, 3.14 );

    cam.set_shape( px_rows, px_cols, fov );

    INFO( "With px_cols: " << px_cols << ", px_rows: " << px_rows << " - aspect " << cam.height() );

    // x is constant -> width are normalized
    // z is constant -> always in same plane of origin xdir X ydir
    REQUIRE_IS_CLOSE( cam.image_origin().cget().at( 0 ), 0.5 );
    // REQUIRE_IS_CLOSE( cam.image_origin().cget().at( 1 ), -0.5 );
    REQUIRE_IS_CLOSE( cam.image_origin().cget().at( 2 ), 0.0 );

    // hight is scaled and centered at 0.0
    if ( px_cols >= px_rows ) {
      REQUIRE( cam.image_origin().cget().at( 1 ) <= -0.5 );
    } else {
      REQUIRE( cam.image_origin().cget().at( 1 ) > -0.5 );
    }
  }

  SECTION( "Pixel dimensions" )
  {
    std::size_t constexpr rows = 4;
    std::size_t constexpr cols = 5;
    FloatType constexpr fov = 0.9272952180016122;

    cam.set_shape( rows, cols, fov );

    REQUIRE( cam.px_height() == 0.25 );
    REQUIRE( cam.px_width() == 0.25 );
  }

  SECTION( "Pixel dimensions 2" )
  {
    std::size_t rows = GENERATE( 1, 2, 3, 4, 5 );
    std::size_t cols = GENERATE( 1, 2, 3, 4, 5 );
    FloatType constexpr fov = 0.9272952180016122;

    cam.set_shape( rows, cols, fov );

    REQUIRE_IS_CLOSE( cam.px_height(), cam.px_width() );
  }
}

TEST_CASE( "BaseCameraRays", "[iteration][ray][camera][rendering]" )
{
  // Image Coords: top left corner of pixel O in image
  //
  //    +---col_dir-->
  //    |             0               1              2              4              5
  // row_dir   O--<i=0>-------+--<i=1>-------+--<i=2>-------+--<i=3>-------+--<i=4>-------+
  //    |      | (          ) | (            | (          ) | (          ) | (          ) |   0
  //    V      +--<i=5>-------+--<i=6>-------+--<i=7>-------+--<i=8>-------+--<i=9>-------+
  //           | (          ) | (            | (          ) | (          ) | (          ) |   1
  //           +--<i=10>------+--<i=11>------+--<i=12>------+--<i=13>------+--<i=14>------+
  //           | (          ) | (            | (          ) | (          ) | (          ) |   2
  //           +--<i=15>------+--<i=16>------+--<i=17>------+--<i=18>------+--<i=19>------+
  //           | (          ) | (            | (          ) | (          ) | (          ) |   3
  //           +--------------+--------------+--------------+--------------+--------------+
  //
  //           O := image origin: < 0.5, -0.625>
  //

  std::size_t constexpr rows = 4;
  std::size_t constexpr cols = 5;
  FloatType constexpr fov = 0.9272952180016122;

  FloatType constexpr img_ori_row = 0.5;
  FloatType constexpr img_ori_col = -0.625;
  FloatType constexpr px_col_off = 0.125;
  FloatType constexpr px_row_off = -0.125;

  rendering::BaseCamera cam{};
  cam.set_shape( rows, cols, fov );

  linalg::Ray ray{};

  SECTION( "First Ray points into pixel at image origin" )
  {
    // Image origin coordinates in the world
    linalg::Vector should_img_ori( { 0, 0, 0 } );

    cam.update_ray( ray, 0.0 );

    REQUIRE( ray.image_col() == 0 );
    REQUIRE( ray.image_row() == 0 );

    INFO(
        "Image Ori: " << cam.image_origin().repr() << " (ori_row, ori_cpl): " << '(' << img_ori_row << ", "
                      << img_ori_col << ')' << "\nCamWidth: " << cam.width() << " CamHeight: " << cam.height()
    );

    auto should_pos = linalg::Vector( { 0, 0, -1 } );
    REQUIRE_ALL_CLOSE( ray.pos(), should_pos );

    auto should_px_center = linalg::Vector( { ( img_ori_row + px_row_off ), ( img_ori_col + px_col_off ), 0 } );
    auto eye_to_should_px_center = should_px_center - cam.eye();
    INFO( "PixelCenter: " << should_px_center.repr() );
    INFO( "EyeToPxCenter: " << eye_to_should_px_center.repr() );
    eye_to_should_px_center.normalize();
    INFO( "|EyeToPxCenter|: " << eye_to_should_px_center.repr() );

    REQUIRE_ALL_CLOSE( ray.dir(), eye_to_should_px_center );
  }

  SECTION( "Pixel are iterated along rows" )
  {
    cam.update_ray( ray, 1.0 );

    linalg::Vector should_look_at( { 0.375, -0.25, 0 } );

    auto shoudl_dir = should_look_at - cam.eye();
    INFO( "EyeToPxCenter: " << shoudl_dir.repr() );
    shoudl_dir.normalize();

    REQUIRE_ALL_CLOSE( ray.dir(), shoudl_dir );
  }

  SECTION( "Last Ray points into last pixel of image" )
  {
    cam.update_ray( ray, 19.0 );

    auto last_px_center = linalg::Vector( { -0.5 - px_row_off, 0.625 - px_col_off, 0.0 } );
    INFO( "LastPxCenter" << last_px_center.repr() );
    auto last_px_dir = last_px_center - linalg::Vector( { 0, 0, -1 } );
    last_px_dir.normalize();

    REQUIRE_ALL_CLOSE( ray.dir(), last_px_dir );
  }
  SECTION( "Row and col getter" )
  {
    std::size_t constexpr rows = 4;
    std::size_t constexpr cols = 5;

    rendering::BaseCamera cam{};
    cam.set_shape( rows, cols, 1.0 );

    REQUIRE( cam.cols() == cols );
    REQUIRE( cam.rows() == rows );
  }
}

// NOLINTEND(*-magic-numbers)
