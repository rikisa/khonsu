// NOLINTBEGIN(*-magic-numbers,*-pointer-arithmetic)

#include <cstddef>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/material/color.hpp"
#include "khonsu/rendering/image.hpp"

#include "testutils/macros.hpp"

using material::RgbFloat32bpc;
using material::RgbUint16bpc;

TEMPLATE_TEST_CASE( "RGBImage", "[Image][rendering]", RgbUint16bpc, RgbFloat32bpc )
{
  using chan_type = typename TestType::channel_type;
  using material::Color;

  std::size_t constexpr height = 130;
  std::size_t constexpr width = 200;
  std::size_t constexpr at_row = 3;
  std::size_t constexpr at_col = 20;
  std::size_t constexpr at_channel = 1;
  std::size_t constexpr at_flat_index = ( at_row * width + at_col ) * TestType::channel_num + at_channel;
  static_assert( at_row < height && at_col < width && at_channel < TestType::channel_num );


  chan_type some_val = 42;

  rendering::GenericImage<TestType> img( height, width );

  SECTION( "Initialized to zero" )
  {
    chan_type cumsum = 0;
    for ( auto val : img.container() ) {
      cumsum += val;
    }
    REQUIRE( cumsum == 0 );
  }

  SECTION( "Access container" ) { REQUIRE( img.container().size() == width * height * TestType::channel_num ); }

  SECTION( "Access data" ) { REQUIRE( typeid( img.data() ) == typeid( typename TestType::channel_type * ) ); }

  SECTION( "Height first, set" )
  {
    img.set_value( at_row, at_col, at_channel, some_val );
    REQUIRE( img.container()[ at_flat_index ] == some_val );
  }

  SECTION( "Height first, get" )
  {
    img.container()[ at_flat_index ] = some_val;
    REQUIRE( img.get_value( at_row, at_col, at_channel ) == some_val );
  }

  SECTION( "Set and add color to pixel" )
  {
    Color::elem_type constexpr red = 1;
    Color::elem_type constexpr blue = 2;
    Color::elem_type constexpr green = 3;

    Color the_color{ { red, green, blue } };
    img.set_color( at_row, at_col, the_color );

    REQUIRE( img.get_value( at_row, at_col, 0 ) == red );
    REQUIRE( img.get_value( at_row, at_col, 1 ) == green );
    REQUIRE( img.get_value( at_row, at_col, 2 ) == blue );

    img.add_color( at_row, at_col, the_color );

    REQUIRE( img.get_value( at_row, at_col, 0 ) == red * 2 );
    REQUIRE( img.get_value( at_row, at_col, 1 ) == green * 2 );
    REQUIRE( img.get_value( at_row, at_col, 2 ) == blue * 2 );
  }
}

TEMPLATE_TEST_CASE( "RGBImage Iterator", "[Image][pixel_iterator][rendering]", RgbUint16bpc, RgbFloat32bpc )
{
  using chan_type = typename TestType::channel_type;

  std::size_t constexpr height = 5;
  std::size_t constexpr width = 10;
  std::size_t constexpr at_row = 3;
  std::size_t constexpr at_col = 5;
  static_assert( at_row < height && at_col < width );

  rendering::GenericImage<TestType> img( height, width );

  SECTION( "Access pointer and pixel count" )
  {
    for ( auto pix_it : img ) {
      pix_it[ 0 ] += 1;
    };

    chan_type cumsum = 0;
    for ( auto val : img.container() ) {
      cumsum += val;
    }
    REQUIRE( cumsum == height * width );
  }
}

// NOLINTEND(*-magic-numbers,*-pointer-arithmetic)
