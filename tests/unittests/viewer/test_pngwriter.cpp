#include <filesystem>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "khonsu/part/partconfig.hpp"
#include "khonsu/viewer/pngwriter.hpp"

#include "testutils/utils.hpp"

static std::string_view constexpr tempfile_prefix = "test_pngviewer";

// NOLINTBEGIN(*-magic-numbers)
TEST_CASE( "PNGWriter", "[part][viewer]" )
{
  auto tempdir = std::filesystem::temp_directory_path() / "khonsu";
  std::filesystem::create_directory( tempdir );

  CHECK( std::filesystem::is_directory( tempdir ) );

  SECTION( "without image, everything is a NOOP" )
  {
    auto fpath = testutils::get_temp_file( tempdir, tempfile_prefix );
    (void)std::filesystem::remove( fpath.c_str() );

    viewer::PNGWriter pngwriter{ fpath };

    // no image creation on instanciation
    REQUIRE( std::filesystem::exists( fpath ) == false );

    pngwriter.show();
    REQUIRE( std::filesystem::exists( fpath ) == false );

    pngwriter.hide();
    REQUIRE( std::filesystem::exists( fpath ) == false );
  }

  SECTION( "Adds file extension *.png, if not present" )
  {
    auto fpath = testutils::get_temp_file( tempdir, tempfile_prefix );
    CHECK( fpath.extension() != ".png" );

    viewer::PNGWriter pngwriter{ fpath };
    auto is_path =
        pngwriter.get_part_config().get_as_or_throw<std::string>( viewer::PNGWriter::ConfigOpts::file_out() );

    // extension is added
    REQUIRE( is_path == fpath.string() + ".png" );
  }

  SECTION( "Keeps existing *.png extension, if present" )
  {
    auto fpath = testutils::get_temp_file( tempdir, tempfile_prefix ).string() + ".png";

    viewer::PNGWriter pngwriter{ fpath };
    auto is_path =
        pngwriter.get_part_config().get_as_or_throw<std::string>( viewer::PNGWriter::ConfigOpts::file_out() );

    // no change
    REQUIRE( is_path == fpath );
  }
}

// NOLINTEND(*-magic-numbers)
