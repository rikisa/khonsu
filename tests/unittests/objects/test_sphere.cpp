// NOLINTBEGIN(*-magic-numbers,*-pointer-arithmetic)

#include <memory>
#include <type_traits>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "khonsu/linalg/intersection.hpp"
#include "khonsu/linalg/linalg.hpp"
#include "khonsu/linalg/material.hpp"
#include "khonsu/linalg/ray.hpp"
#include "khonsu/objects/objects.hpp"

#include "testutils/macros.hpp" // IWYU pragma: keep

using objects::DebugSphere;
using objects::Sphere;

TEMPLATE_TEST_CASE( "Sphere", "[unittest][scene][rendering]", Sphere, DebugSphere )
{
  auto mat = std::make_shared<material::BaseMaterial>( material::Color{ 1 } );

  TestType sphere = [ & ]() {
    if constexpr ( std::is_same_v<TestType, DebugSphere> ) {
      auto mode = GENERATE(
          DebugSphere::ColorMode::Normals, DebugSphere::ColorMode::CheckBoard, DebugSphere::ColorMode::Distance,
          DebugSphere::ColorMode::DistanceInverse, DebugSphere::ColorMode::DistanceMix
      );
      return TestType{ linalg::CoordBase{}, mat, 0.1, mode };
    } else {
      return TestType{ linalg::CoordBase{}, mat, 0.1 };
    }
  }();

  linalg::Ray hitting_ray{};
  hitting_ray.move( linalg::Vector{ { 0, 0, -2 } }, linalg::Vector{ { 0, 0, 1 } } );

  linalg::Ray nonhitting_ray{};
  nonhitting_ray.move( linalg::Vector{ { 0, 0, -2 } }, linalg::Vector{ { 1 } } );

  SECTION( "Intersect with ray" )
  {
    linalg::Intersection intersect{};

    auto hits = sphere.get_intersect( hitting_ray, intersect, 0, 100.0 );

    INFO( hitting_ray.pos() << " " << hitting_ray.dir() << " " << sphere.get_origin().pos() );
    REQUIRE( hits );
    REQUIRE( intersect.at_t() > 0.0 );
    REQUIRE( intersect.at_point().norm() > 0.0 );
    REQUIRE_ALL_CLOSE( intersect.with_material().get_color(), sphere.get_material()->get_color() );
  }

  SECTION( "Does not intersect with ray - not in max_dist" )
  {
    linalg::Intersection intersect{};
    auto material = std::make_shared<material::BaseMaterial>();

    auto hits = sphere.get_intersect( hitting_ray, intersect, 0.0, 0.0 );

    REQUIRE_FALSE( hits );
  }

  SECTION( "Does not intersect with ray - not in min_dist" )
  {
    linalg::Intersection intersect{};
    auto material = std::make_shared<material::BaseMaterial>();

    auto hits = sphere.get_intersect( hitting_ray, intersect, 100.0, 100.0 );

    REQUIRE_FALSE( hits );
  }

  SECTION( "Does not intersect withn non-hitting ray - geometry" )
  {
    linalg::Intersection intersect{};
    auto material = std::make_shared<material::BaseMaterial>();

    auto hits = sphere.get_intersect( nonhitting_ray, intersect, 0.0, 100.0 );

    REQUIRE_FALSE( hits );
  }
}

// NOLINTEND(*-magic-numbers,*-pointer-arithmetic)
