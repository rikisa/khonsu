// NOLINTBEGIN(*-magic-numbers,*-pointer-arithmetic)

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/linalg.hpp"

#include "imitates/rendering/mockobject.hpp"

#include "testutils/macros.hpp"

TEST_CASE( "Base Object", "[unittest][scene][rendering]" )
{
  imitates::rendering::MockObject base_obj{};

  SECTION( "Default Material and pos is all zeros" )
  {
    auto obj_mat = base_obj.get_material();
    REQUIRE_ALL_CLOSE( obj_mat->get_color(), material::Color{ 0 } );

    auto obj_origin = base_obj.get_origin();
    REQUIRE_ALL_CLOSE( obj_origin.pos(), linalg::Vector{ 0 } );
  }

  SECTION( "Set origin" )
  {
    linalg::Vector the_pos{ 4 };
    linalg::Vector the_fwd{ 1 };
    linalg::Vector the_upward{ 2 };
    the_fwd.normalize();
    the_upward.normalize();

    linalg::CoordBase new_ori{ the_upward, the_fwd, the_pos };
    base_obj.set_origin( std::move( new_ori ) );

    REQUIRE_ALL_CLOSE( base_obj.get_origin().pos(), the_pos );
    REQUIRE_ALL_CLOSE( base_obj.get_origin().ydir(), the_upward );
    REQUIRE_ALL_CLOSE( base_obj.get_origin().zdir(), the_fwd );
  }
}

// NOLINTEND(*-magic-numbers,*-pointer-arithmetic)
