khonsu_add_test(
  TestObject
  SOURCES test_object.cpp
  LIBRARIES Khonsu::LinAlg Khonsu::Objects Khonsu::ImitatesRendering Khonsu::Testdata)

khonsu_add_test(
  TestSphere
  SOURCES test_sphere.cpp
  LIBRARIES Khonsu::Objects Khonsu::TestUtils)
