#include <cstddef>
#include <filesystem>

#include <catch2/catch_test_macros.hpp>

#include "khonsu/linalg/material/color.hpp"
#include "khonsu/rendering/image.hpp"
#include "khonsu/viewer/pngwriter.hpp"

#include "testutils/pngwalker.hpp"
#include "testutils/utils.hpp"
#include <png++/image.hpp>

static std::string_view constexpr tempfile_prefix = "test_pngviewer+image";

using material::RgbFloat32bpc;
using material::RgbUint16bpc;

// NOLINTBEGIN(*-magic-numbers)
TEST_CASE( "PNGWriter", "[part][viewer][image][png]" )
{
  auto tempdir = std::filesystem::temp_directory_path() / "khonsu";
  std::filesystem::create_directory( tempdir );

  std::size_t constexpr height = 3;
  std::size_t constexpr width = 5;

  auto img = std::make_shared<rendering::Image>( height, width );

  SECTION( "Reads from refernced image upon show(), writes content into file" )
  {
    auto fpath = testutils::get_temp_file( tempdir, tempfile_prefix, ".png" );

    viewer::PNGWriter pngwriter{ fpath };

    // write empty image
    pngwriter.read_image( img );
    pngwriter.show();

    INFO( std::format( "Writing {}x{} image to {}", img->width(), img->height(), fpath.string() ) );

    testutils::PngWalker png_out( fpath );
    auto ihdr = png_out.read_ihdr();

    REQUIRE( ihdr.width == width );
    REQUIRE( ihdr.height == height );
  }

  SECTION( "Data from image written to png" )
  {
    auto fpath = testutils::get_temp_file( tempdir, tempfile_prefix, ".png" );

    std::size_t constexpr larger_height = 30;
    std::size_t constexpr larger_width = 50;
    auto larger_img = std::make_shared<rendering::Image>( larger_height, larger_width );

    for ( std::size_t cur_col = 0; cur_col < larger_width; ++cur_col ) {
      larger_img->set_color( 0, cur_col, material::Color( { 1.0, 0.0, 0.0 } ) );
      larger_img->set_color( larger_height - 1, cur_col, material::Color( { 0.0, 1.0, 0.0 } ) );
    }

    for ( std::size_t cur_row = 0; cur_row < larger_height; ++cur_row ) {
      larger_img->set_color( cur_row, 0, material::Color( { 1.0, 0.0, 1.0 } ) );
      larger_img->set_color( cur_row, larger_width - 1, material::Color( { 0.0, 1.0, 1.0 } ) );
    }

    viewer::PNGWriter pngwriter{ fpath };
    pngwriter.read_image( larger_img );

    // diagonal added after read
    for ( std::size_t cur_row = 0; cur_row < larger_height; ++cur_row ) {
      for ( std::size_t cur_col = 0; cur_col < larger_width; ++cur_col ) {
        if ( cur_col == cur_row ) {
          larger_img->set_color( cur_row, cur_col, material::Color( { 1.0, 1.0, 1.0 } ) );
        }
      }
    }

    pngwriter.show();

    png::image<png::rgb_pixel_16> png_image( larger_img->width(), larger_img->height() );
    png_image.read( fpath.string() );

    CHECK( png_image.get_width() == larger_width );
    CHECK( png_image.get_height() == larger_height );

    INFO( std::format(
        "Writing {}x{} image with diagonal line to {}", larger_img->width(), larger_img->height(), fpath.string()
    ) );

    auto constexpr max_value = std::numeric_limits<std::uint16_t>::max();
    for ( std::size_t cur_row = 1; cur_row < ( larger_height - 1 ); ++cur_row ) {
      for ( std::size_t cur_col = 1; cur_col < ( larger_width - 1 ); ++cur_col ) {
        auto pixel_diag = png_image.get_pixel( cur_col, cur_row );
        if ( cur_col == cur_row ) {
          REQUIRE( pixel_diag.blue == max_value );
          REQUIRE( pixel_diag.red == max_value );
          REQUIRE( pixel_diag.green == max_value );
        } else {
          REQUIRE( pixel_diag.blue == 0 );
          REQUIRE( pixel_diag.red == 0 );
          REQUIRE( pixel_diag.green == 0 );
        }
      }
    }
  }
}

// NOLINTEND(*-magic-numbers)
