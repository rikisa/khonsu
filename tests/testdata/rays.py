# Calculate Rays for a rotated basis and generate

import numpy as np
from pathlib import Path
from typing import Tuple, List, Union
from numpy.typing import ArrayLike
from jinja2 import Template
import sys


def transform(vec: ArrayLike, mat: ArrayLike) -> ArrayLike:
    """ Sequentially performes rotation matrices in mat on vec
    """
    ret = []
    for v in vec:
        for M in mat:
            v = v @ M
        ret.append(v)
    return ret


def plot(origins: ArrayLike, dirs: ArrayLike, focus: ArrayLike, pos: ArrayLike):
    """ Plots origin points with their respective dirs,
    and pos (center) and focus
    """
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    start = np.stack(origins)
    stop = start + np.stack(dirs)

    Y = np.stack([start[:,0], stop[:,0]]).T
    X = np.stack([start[:,1], stop[:,1]]).T
    Z = np.stack([start[:,2], stop[:,2]]).T

    for yi, xi, zi in zip(Y, X, Z):
        # plot the dirs
        ax.plot(yi, xi, zi)

        # extension from focus to ray tip
        ye = [yi[1], focus[0]]
        xe = [xi[1], focus[1]]
        ze = [zi[1], focus[2]]
        ax.plot(ye, xe, ze, linestyle=':', color=[0, 0, 0, .3])

    # origin dots
    for oi in origins:
        yi = [oi[0]]
        xi = [oi[1]]
        zi = [oi[2]]
        ax.plot(yi, xi, zi, marker='o')

    # focus and center
    cy = pos[0], focus[0]
    cx = pos[1], focus[1]
    cz = pos[2], focus[2]
    ax.plot(cy, cx, cz, marker='x')

    ax.set_xlim(-1, 1)
    ax.set_xlabel('X')
    ax.set_ylim(-1, 1)
    ax.set_ylabel('Y')
    ax.set_zlim(-1, 1)
    ax.set_zlabel('Z')

    plt.show()


def calculate(tz: float, ty: float, focal_length: float, image_width: float, translation: np.ndarray) -> Tuple[ArrayLike]:
    """ Creates a 3 x 4 image plane with dirs and origins, rotated around tz, ty and
    moved by translation
    """
    # definition of original
    e1 = np.array([1, 0, 0])
    e2 = np.array([0, 1, 0])
    e3 = np.array([0, 0, 1])
    pos = np.array([0, 0, 0])
    focus = np.array([0, 0, -1]) * focal_length
    translation = np.array(translation)

    rows, cols = 5, 5
    image_height = image_width * (rows / cols)
    row_off = 1 / (rows*2)
    col_off = 1 / (cols*2)
    row_grid = np.linspace(0.5 - row_off, row_off - 0.5, rows)
    col_grid = np.linspace(col_off - 0.5, 0.5 - col_off, cols)
    row_steps = row_grid * image_width
    col_steps = col_grid * image_height

    # rotation matrices
    Rz = np.array(
      [[np.cos(tz), -np.sin(tz), 0],
       [np.sin(tz),  np.cos(tz), 0],
       [       0,         0, 1],
    ])

    Ry = np.array(
      [[1,        0,         0],
       [0, np.cos(ty), -np.sin(ty)],
       [0, np.sin(ty),  np.cos(ty)],
    ])

    # transformation matrix
    M = np.stack(transform([e1, e2, e3], [Rz, Ry]))

    origins = []
    dirs = []
    for i in row_steps:  # rows
        for j in col_steps:  # cols
            ray_origin = e1 * i + e2 * j
            ray_dir = ray_origin - focus
            ray_dir /= np.linalg.norm(ray_dir)
            origins.append(ray_origin)
            dirs.append(ray_dir)

    # apply transformations to all absolute points and not on dirs
    origins = [oi @ M + translation for oi in origins]
    dirs = [di @ M for di in dirs]
    pos = translation
    focus = focus @ M + translation

    # sanity checks
    for en in M:
        assert np.isclose(np.linalg.norm(en), 1), f'Base {en} not normalized!'
    e1, e2, e3 = M

    assert np.isclose(np.linalg.norm( focus - pos ), focal_length), f'Focal point not correct!'

    return origins, dirs, pos, focus, (e1, e2, e3), len(row_steps), len(col_steps)

def generate_header(origins: ArrayLike, dirs: ArrayLike, to_dir: Union[Path, str],
                    rows: int, cols: int, pos: ArrayLike, focus: float, width: float,
                    e1: ArrayLike, e2: ArrayLike, e3: ArrayLike):

    focal_length = np.linalg.norm( focus - pos )

    to_dir = Path(to_dir)
    if not to_dir.is_dir():
        raise ValueError(f"'{to_dir}' must be a path")

    # template rendering
    this_file = Path(__file__).absolute()
    with this_file.with_suffix('.hpp.j2').open('r') as src:
        template = Template(src.read())

    content = template.render(origins=origins, dirs=dirs, rows=rows, cols=cols,
                              pos=pos, base=(e1, e2, e3), focal_length=focal_length, focus=focus,
                              width=width)

    # write the output
    out_file = (to_dir / this_file.name).with_suffix('.hpp')
    with out_file.open('w') as dst:
        dst.write(content)


if __name__ == '__main__':

    # plot(origins, dirs, focus, pos)
    focal_length = 1.8
    image_width = 1.456
    origins, dirs, pos, focus, base, rows, cols = calculate(
        tz=np.pi/4, ty=np.pi/8, focal_length=focal_length, image_width=image_width, translation=[0, 0, 0])

    e1, e2, e3 = base

    if 'plot' in sys.argv:
        plot(origins, dirs, focus, pos)
    
    to_dir = Path(__file__).absolute().parent / 'include' / 'testdata'
    to_dir.mkdir(exist_ok=True, parents=True)
    generate_header(
        origins, dirs, to_dir=to_dir,
        rows=rows, cols=cols, pos=pos, focus=focus,
        e1=e1, e2=e2, e3=e3, width=image_width
    )
