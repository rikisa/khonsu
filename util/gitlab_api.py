#!/bin/python

"""Utility script with some incomplete binding of the girlab api"""

import IPython as ip
from urllib.request import Request, urlopen
from urllib.parse import urljoin
from urllib.error import HTTPError
from dataclasses import dataclass
from pathlib import Path
import json


@dataclass
class GitlabProject:
    """Global Attributes"""
    project_id: int = 45619137
    pipeline_api_url: str = f'https://gitlab.com/api/v4/projects/{project_id}/pipelines'


@dataclass
class Pipeline:
    """Representation of GitLab pipeline"""
    pipeline_id: int
    sha: str


def append_url(url: str, appendix: str):
    if not url.endswith('/'):
        url = url + '/'
    return urljoin(url, appendix)


def get_pipelines(token: str):
    """Gets a list of 20 piplines"""
    req = Request(
        url=GitlabProject.pipeline_api_url,
        headers={
            "PRIVATE-TOKEN": f'{token}'
        },
        method='GET'
    )
    resp = urlopen(req)

    ret = []
    for pipeline_data in json.load(resp):
        ret.append(Pipeline(pipeline_data['id'], pipeline_data['sha']))

    return ret


def erase_pipeline(token: str, pipline: Pipeline):
    """Erase pipeline and all artifacts/logs/jobs from project"""
    print(f'Deleting pipeline #{pipline.pipeline_id}', end=' -> ')
    req = Request(
        url=append_url(GitlabProject.pipeline_api_url,
                       f'{pipline.pipeline_id}'),
        headers={
            "PRIVATE-TOKEN": f'{token}'
        },
        method='DELETE'
    )
    try:
        resp = urlopen(req)
        print(resp.getcode())
    except HTTPError as err:
        print(err)


def delete_all_pipelines(token: str):
    while to_delete := get_pipelines(token):
        for pline in to_delete:
            erase_pipeline(token, pline)


def get_token(fname: str | Path):
    """read token from file"""
    with Path(fname).expanduser().open('r') as tkn:
        return tkn.read()


ip.embed()
