#!/bin/bash

function show_help(){
    echo $0
    echo "  -a, --all       Add everything under source control, but not staged files"
    echo "  -n, --noadd     Only format, do git add the formatted files"
    echo "  -v, --verbose   Set verbosity of clang-format"
    echo "  -h, --help      Show this help and exit"
}


# Change cwd to repository root if needed (aka not run as hook)
REPO_ROOT=$(git rev-parse --show-toplevel)
pushd ${REPO_ROOT} > /dev/null

### Default behaviour
# perform git add
GIT_ADD=1
# regex for matching files with grep
GREP_EX='\.(h|hpp|cpp)$'
# Currently staged files
declare -a FILES=()
FILES+=$(git diff --cached --name-only --diff-filter=ACM | grep -E --color=never ${GREP_EX})
# Do not format everything under source control
ALL_FILES=0
# Non-verbose clang format
VERBOSE=""

# parse arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    -a|--all) # everything under source control
      ALL_FILES=1
      shift
      ;;
    -n|--noadd) # do not add after formating
      GIT_ADD=0
      shift
      ;;
    -h|--help) # help
      show_help
      exit 0
      ;;
    -v|--verbose) # Set verbosity of clang-format
      VERBOSE="--verbose"
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      show_help
      exit 22
      ;;
  esac
done


# Adding all files under source control
if [ ${ALL_FILES} -ne 0 ];
then
  FILES+=$(git ls-files | grep -E --color=never ${GREP_EX})
fi
printf "%s\n" "${FILES[@]}"
# Trim to unique ones (removed already under source controll AND staged changes)
FILES=( $(printf "%s\n" "${FILES[@]}" | sort -u) )

if [ -z "${FILES}" ]; then
    echo "No files, nothing to do"
    exit 0
fi

# formatting in place
clang-format      \
    -style=file   \
    ${VERBOSE}    \
    -i "${FILES[@]}"

# adding changed files to commit
if [ ${GIT_ADD} -ne 0 ];
then
    echo "Adding changes to current commit"
    git add ${FILES[@]}
fi

popd > /dev/null
