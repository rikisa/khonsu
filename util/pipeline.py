#!/usr/bin/env python3
"""Defines pipeline stages for CI/CD independently from used CI/CD tooling.

Wrapper around cmake invocations and possible cleanup and moving around artefacts
after cmake steps.
"""

import argparse
from pathlib import Path
import subprocess as sub
import sys
import os
import re


def do_call(*args, **kwargs):
    kwargs.setdefault('stdout', sys.stdout)
    print(' '.join(args), flush=True)
    job = sub.Popen(args, **kwargs)
    job.wait()
    return job


def build_stage(args):
    """Wrapps cmake commands for building already configured repositories"""
    extra = []

    for do_type in args.type:
        cmake_call = [
            'cmake', '--build', f'{args.bindir}', '--config', f'{do_type}']
        cmake_call += [f'-D{ex}' for ex in extra]

        job = do_call(*cmake_call)
        if job.returncode != 0:
            return job.returncode
    return 0


def docs_stage(args):
    """Wrapps cmake commands generating reports and documentation"""
    extra = []

    if args.cov:
        cmake_call = [
            'cmake', '--build', f'{args.bindir}', '--config', 'Coverage', '--target', 'CoverageReport', '-j', '1']
        cmake_call += [f'-D{ex}' for ex in extra]
        ret = do_call(*cmake_call)
        if ret.returncode:
            return ret.returncode
    if args.proj:
        cmake_call = [
            'cmake', '--build', f'{args.bindir}', '--config', 'Coverage', '--target', 'SphinxDocsGen', '-j', '1']
        cmake_call += [f'-D{ex}' for ex in extra]
        ret = do_call(*cmake_call)
        if ret.returncode:
            return ret.returncode
    return 0


def config_stage(args):
    """Wrapps cmake commands for configuring the build dir"""
    extra = []

    # do only compile commands
    if args.compile_cmd != 'None':
        extra.append('CMAKE_EXPORT_COMPILE_COMMANDS=ON')
        cmake_call = ['cmake', str(args.srcdir), "-G", "Ninja",
                      f'--preset={args.compile_cmd}', '-B', f'{args.bindir}']
        cmake_call += [f'-D{ex}' for ex in extra]
        ret = do_call(*cmake_call)
        return ret.returncode

    cmake_call = ['cmake', str(args.srcdir),
                  '--preset=pipeline', '-B', f'{args.bindir}']
    cmake_call += [f'-D{ex}' for ex in extra]

    ret = do_call(*cmake_call)
    return ret.returncode


def test_stage(args):
    """Wrapps cmake commands doing tests"""
    extra = []

    for do_type in args.type:
        cmake_call = ['ctest', f'--build-config {do_type}',
                      '--test-dir', f'{args.bindir}',
                      '--output-junit', 'test-report.xml',
                      ]
        cmake_call += [f'-D{ex}' for ex in extra]

        ret = do_call(*cmake_call)
        if ret.returncode != 0:
            return 1


def analysis_stage(args):
    """Perform static and dynamic analysis on deployed project

    Allows to parse the output of clang-tidy and returns an error code
    """

    analyse_dir = Path(args.srcdir) / 'src'
    log_dir = Path(args.bindir) / 'analysis'
    log_dir.mkdir(parents=True, exist_ok=True)

    if args.static_analysis:
        # ClangTidy version 15
        tidy_call = [
            'run-clang-tidy',
            f'-j{os.cpu_count()}',
            f'-p={args.bindir}',
            f'-config-file={args.srcdir / ".clang-tidy"}']

        if args.apply_fix:
            tidy_call.extend(['-fix', '-format'])

        tidy_call.append(f'{str(analyse_dir / ".*")}')

        ret = do_call(*tidy_call, stdout=sub.PIPE, stderr=sub.PIPE)
        tidy_out = ret.stdout.read().decode('utf-8')  # type: ignore
        tidy_out += ret.stderr.read().decode('utf-8')  # type: ignore
        tidy_log = log_dir / 'clang-tidy.log'

        with tidy_log.open('w') as dst:
            dst.write(tidy_out)

        findings = list(re.findall(f'^ *{analyse_dir}.*warning:.*', tidy_out, re.MULTILINE)) + \
            list(re.findall(f'^ *{analyse_dir}.*error:.*',
                 tidy_out, re.MULTILINE))

        print('\n'.join(findings), flush=True)
        if len(findings) > 0:
            print(
                f'Clang tidy found {len(findings)} issues, see full log: {tidy_log}', file=sys.stderr, flush=True)
            return 1

        return ret.returncode


def get_args_parser():
    parser = argparse.ArgumentParser(
        description='Run pipeline stages for CI/CD')

    default_bindir = Path(os.getcwd()) / 'build'
    default_sourcedir = Path(__file__).parents[1]

    parser.add_argument(
        '-b --builddir', help=f'CMake binary dir to use (default={default_bindir})', dest='bindir', type=Path, default=default_bindir)

    parser.add_argument(
        '-s --sourcedir', help=f'CMake source dir to use (default={default_sourcedir})', dest='srcdir',
        default=default_sourcedir, type=Path)

    stage_parsers = parser.add_subparsers(
        title='stage', help='Stage to perform', required=True, dest='stage')

    config_cmd = stage_parsers.add_parser('config', help='Configure build')
    config_cmd.add_argument(
        '-c --compile-db', help='Create compile_command.json only, but nothing else',
        dest='compile_cmd', type=str, default='None', choices=['linux-dev', 'linux-release', 'linux-cov'])

    build_cmd = stage_parsers.add_parser('build', help='Build targets')
    build_cmd.add_argument(
        'type', choices=['Debug', 'Release', 'Coverage'], nargs='+')

    test_cmd = stage_parsers.add_parser('test', help='Run tests')
    test_cmd.add_argument(
        'type', choices=['Debug', 'Release', 'Coverage'], nargs='+')

    docu_cmd = stage_parsers.add_parser('docs', help='Generate docu')
    docu_cmd.add_argument('-c --coverage', dest='cov',
                          help='Generate test coverage report', action='store_true')
    docu_cmd.add_argument('-p --project', dest='proj',
                          help='Generate project documentation', action='store_true')

    analysis_cmd = stage_parsers.add_parser(
        'analysis', help='Do variuos analysis steps')
    analysis_cmd.add_argument(
        '--fix', help='Apply fixes and format the code', action='store_true', dest='apply_fix')
    analysis_cmd.add_argument('--no-static', help='Do static code analysis',
                              action='store_false', dest='static_analysis')

    return parser


def main():
    parser = get_args_parser()
    args = parser.parse_args()

    match args.stage:
        case 'build':
            sys.exit(build_stage(args))
        case 'config':
            sys.exit(config_stage(args))
        case 'test':
            sys.exit(test_stage(args))
        case 'docs':
            sys.exit(docs_stage(args))
        case 'analysis':
            sys.exit(analysis_stage(args))


if __name__ == '__main__':
    main()
