#!/bin/bash

INVALID_VALUE=22
INVALID_DIR=20

# Used binaries
FORMAT_CMD=clang-format
GIT_CMD=git

REPO_ROOT=$(git rev-parse --show-toplevel)

# Check binary dependencies
${FORMAT_CMD} --version
if [ $? -ne 0 ] ; then
    echo "Format command '${FORMAT_CMD}' not found. Please install it!"
    exit ${INVALID_VALUE}
fi

${GIT_CMD} --version
if [ $? -ne 0 ] ; then
    echo "Git command '${GIT_CMD}' not found. Please install it!"
    exit ${INVALID_VALUE}
fi

# Check target directory
if [ ! -d ${HOOKS_DIR} ]; then
    echo "Hooks directory '${HOOKS_DIR}' does not exists! Did you move/copy the script file?"
    exit ${INVALID_DIR}
fi

# Used directories
SCRIPT_DIR=$(realpath $(dirname -- "$0"; ))
REPO_DIR=$( dirname ${SCRIPT_DIR} )
HOOKS_DIR=${REPO_DIR%/}/.git/hooks

ln -s ${SCRIPT_DIR}/format-commit.sh ${HOOKS_DIR}/pre-commit
