#!/bin/bash

set -e

SRC_DEPS_DIR="${1:-/tmp/repos}"
INSTALL_PREFIX="${2:-/usr/local}"

# download, build and install dependencies into base debian system
mkdir -p ${SRC_DEPS_DIR}

git clone --depth=1 --branch=v3.3.2 https://github.com/catchorg/Catch2.git ${SRC_DEPS_DIR}/Catch2 || echo "Assuming Catch2 already fetched"
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -S ${SRC_DEPS_DIR}/Catch2 -G Ninja -B ${SRC_DEPS_DIR}/Catch2-build \
      -DBUILD_TESTING=OFF \
      -DCATCH_INSTALL_DOCS=OFF \
      -DCATCH_INSTALL_EXTRAS=ON

git clone --depth=1 --branch=v3.3.0 https://github.com/ThePhD/sol2.git ${SRC_DEPS_DIR}/sol2 || echo "Assuming sol2 already fetched"
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -S ${SRC_DEPS_DIR}/sol2 -G Ninja -B ${SRC_DEPS_DIR}/sol2-build \
      -DBUILD_TESTING=OFF \
      -DSOL2_BUILD_LUA=OFF \
      -DSOL2_DOCS=OFF \
      -DSOL2_TESTS=OFF \
      -DSOL2_EXAMPLES=OFF 

git clone --depth=1 --branch=v3.0.9 https://github.com/dankamongmen/notcurses.git ${SRC_DEPS_DIR}/notcurses || echo "Assuming notcurses already fetched"
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -S ${SRC_DEPS_DIR}/notcurses -G Ninja -B ${SRC_DEPS_DIR}/notcurses-build \
      -DUSE_COVERAGE=OFF \
      -DUSE_CXX=ON \
      -DUSE_DOCTEST=OFF \
      -DUSE_DOXYGEN=OFF \
      -DUSE_PANDOC=OFF \
      -DBUILD_EXECUTABLES=OFF \
      -DBUILD_TESTING=OFF \
      -DUSE_POC=ON \
      -DUSE_STATIC=ON \
      -DUSE_MULTIMEDIA=none 
      

cmake --build ${SRC_DEPS_DIR}/Catch2-build --target install
cmake --build ${SRC_DEPS_DIR}/sol2-build --target install
cmake --build ${SRC_DEPS_DIR}/notcurses-build --target install

rm -rf ${SRC_DEPS_DIR}
