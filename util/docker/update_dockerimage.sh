#!/bin/bash

if [[ -z ${KHONSU_CI_USERNAME} ]]; then
    echo "Please set the KHONSU_CI_USERNAME environment variable!"
    exit -1
fi

if [[ -z ${KHONSU_CI_TOKEN} ]]; then
    echo "Please set the KHONSU_CI_TOKEN environment variable!"
    exit -1
fi

echo "${KHONSU_CI_TOKEN}" | docker login -u ${KHONSU_CI_USERNAME} --password-stdin registry.gitlab.com || exit -1

docker buildx build . -f debian-gcc.Dockerfile -t registry.gitlab.com/rikisa/khonsu/debian-gcc \
  && docker push registry.gitlab.com/rikisa/khonsu/debian-gcc

docker logout
