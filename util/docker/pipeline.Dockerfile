FROM registry.gitlab.com/rikisa/khonsu/debian-gcc as Configure
WORKDIR /khonsu/debian-gcc/source
ADD . /khonsu/debian-gcc/source

RUN  ./util/pipeline.py config


FROM registry.gitlab.com/rikisa/khonsu/debian-gcc as Build
WORKDIR /khonsu/debian-gcc/source
ADD . /khonsu/debian-gcc/source
COPY --from=Configure /khonsu/debian-gcc/source/build /khonsu/debian-gcc/source/build

RUN  ./util/pipeline.py build Coverage


FROM registry.gitlab.com/rikisa/khonsu/debian-gcc as Testing
WORKDIR /khonsu/debian-gcc/source
ADD . /khonsu/debian-gcc/source
COPY --from=Build /khonsu/debian-gcc/source/build /khonsu/debian-gcc/source/build

RUN  ./util/pipeline.py test Coverage


FROM registry.gitlab.com/rikisa/khonsu/debian-gcc as DocsCov
WORKDIR /khonsu/debian-gcc/source
ADD . /khonsu/debian-gcc/source
COPY --from=Testing /khonsu/debian-gcc/source/build /khonsu/debian-gcc/source/build

RUN  ./util/pipeline.py docs -c -p
