FROM debian:trixie-slim as staging

WORKDIR /khonsu/debian-gcc/staging

# Install everything for bootstrapping the system
ADD ./deps/apt/staging /khonsu/debian-gcc/staging/deps-staging
RUN apt-get update -y \
  && echo "$(cat ./deps-staging) gpg gpg-agent" \
  | xargs apt-get install -y --no-install-recommends \
  && apt-get clean

# LLVM PPA setup
ADD ./deps/apt/sources/llvm.list /etc/apt/sources.list.d/llvm.list
RUN curl -o /usr/share/keyrings/llvm-snapshot.gpg.key  https://apt.llvm.org/llvm-snapshot.gpg.key \
  && gpg --no-default-keyring --keyring ./temp-keyring.gpg --import /usr/share/keyrings/llvm-snapshot.gpg.key \
  && gpg --no-default-keyring --keyring ./temp-keyring.gpg --export --output /usr/share/keyrings/llvm-snapshot.gpg \
  && apt-get purge -y gpg gpg-agent \
  && apt-get clean

# Build and project deps
ADD ./deps/apt/ /khonsu/debian-gcc/staging/deps/apt/
RUN apt-get update && echo $(cat ./deps/apt/build ./deps/apt/coverage ./deps/apt/docs ./deps/apt/analysis ./deps/apt/testing) \
  | xargs apt-get install -y --no-install-recommends \
  && apt-get clean

# Build source deps
ADD ./scripts /khonsu/debian-gcc/staging/sbin
RUN ./sbin/fetch_and_buils_source_deps.sh

# setup clang-tidy
RUN update-alternatives --install /usr/bin/run-clang-tidy run-clang-tidy /usr/bin/run-clang-tidy-17 100 \
 && update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-17 100 \
 && update-alternatives --install /usr/bin/clang-apply-replacements clang-apply-replacements /usr/bin/clang-apply-replacements-17 100

# Get pip only dependencies
ADD ./deps/pip/docs.txt /khonsu/debian-gcc/staging/deps/pip/
RUN pip install -r ./deps/pip/docs.txt --break-system-packages \
 && pip cache purge \
 && apt-get purge -y pip

# curl deps, patching plantuml to a recent version
RUN curl -L https://github.com/plantuml/plantuml/releases/download/v1.2023.12/plantuml-mit-1.2023.12.jar -o /usr/share/plantuml/plantuml.jar

# Remove staging dependencies
RUN apt-get clean

# Remove any artifacts and apt
WORKDIR /
RUN rm -rf /khonsu /usr/local/share/doc

FROM scratch
COPY --from=staging / /
