"""Gathers profile data from binary dir and creates coverage report"""

import glob
import argparse
from pathlib import Path
import subprocess
import sys
import re
from typing import TextIO
import logging

logging.basicConfig(
    level=logging.INFO,
    handlers=[logging.StreamHandler()]
)


def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--cov', required=True,
                        dest='llvm_cov', help='LLVM-Cov command')
    parser.add_argument('--pdat', required=True,
                        dest='llvm_profdata', help='LLVM-Profdata command')
    parser.add_argument('--dir', required=True,
                        dest='working_dir', help='Path to working directory. All artifacts will be created there')
    parser.add_argument('--prof', required=True,
                        dest='profraws_dir', help='Path that will be globbed for .profraw files')
    # parser.add_argument('--bin',
    #                     dest='binaries_dir', help='Path that will be globbed for .so and .a files')
    parser.add_argument('--binmanifest',
                        dest='binaries_manifest', help='File that contains all test binaries')
    parser.add_argument('--profpat', default='.*.profraw',
                        dest='prof_pat', help='Only *.profraw filenames matching this regex pattern are used')
    parser.add_argument('--ignore', default='*',
                        dest='ignore_pattern', help='Only source files matching this pattern are reported')
    return parser.parse_args()


def run_sub(cli_args, working_dir, outstream: TextIO | int = sys.stdout):
    log = logging.getLogger()
    log.debug(' '.join([str(ca) for ca in cli_args]))
    try:
        subprocess.run(cli_args, cwd=working_dir, check=True, stdout=outstream,
                       stderr=sys.stderr)
    except subprocess.CalledProcessError as err:
        log.warning(f' -> {err.returncode}\n')
        sys.exit(err.returncode)


def main():
    args = get_args()

    cov_cmd = args.llvm_cov
    profdat_cmd = args.llvm_profdata

    working_dir = Path(args.working_dir)
    working_dir.mkdir(parents=True, exist_ok=True)

    html_dir = working_dir / 'html'
    html_dir.mkdir(parents=True, exist_ok=True)

    lcov_path = working_dir / 'lcov.dat'
    profdat_path = str(working_dir / 'all.profdata')

    profraws_dir = Path(args.profraws_dir)
    # binaries_dir = Path(args.binaries_dir)
    binaries_manifest = Path(args.binaries_manifest)

    ignore_pattern = args.ignore_pattern
    prof_pat = args.prof_pat

    if not profraws_dir.is_absolute():
        profraws_dir = working_dir / profraws_dir
    profraws_dir = profraws_dir.expanduser().resolve()

    # if not binaries_dir.is_absolute():
    #     binaries_dir = working_dir / binaries_dir
    # binaries_dir = binaries_dir.expanduser().resolve()
    if not binaries_manifest.is_absolute():
        binaries_manifest = working_dir / binaries_manifest
    binaries_manifest = binaries_manifest.expanduser().resolve()

    # Merge profiles
    prof_match = re.compile(prof_pat)
    all_profraws = [profraws_dir / fi for fi in glob.glob(
        '**/*.profraw', root_dir=profraws_dir, recursive=True)]
    filtered_profraws = [
        str(pr) for pr in all_profraws if prof_match.match(pr.name) is not None]

    cli_args = ([profdat_cmd, 'merge', '-sparse']
                + filtered_profraws
                + ['-o', profdat_path])
    run_sub(cli_args, working_dir, outstream=subprocess.DEVNULL)

    binaries = [Path(tbin.strip())
                for tbin in binaries_manifest.open('r').readlines()]

    # binaries_list = [str(binaries[0])]
    binaries_list = []
    for obj in binaries:
        binaries_list.extend(['-object', str(obj)])

    # create html report
    cli_args = ([cov_cmd, 'show', '-instr-profile', profdat_path]
                + binaries_list
                + [f'-ignore-filename-regex={ignore_pattern}']
                + ['-format', 'html']
                + ['-o', str(html_dir)]
                + ['-show-line-counts-or-regions', '-Xdemangler', 'c++filt']
                )
    run_sub(cli_args, working_dir, outstream=subprocess.DEVNULL)

    # export to lcov
    cli_args = ([cov_cmd, 'export', '-instr-profile', profdat_path]
                + binaries_list
                + [f'-ignore-filename-regex={ignore_pattern}']
                + ['-format', 'lcov']
                )
    with lcov_path.open('w') as dest:
        run_sub(cli_args, working_dir, outstream=dest)

    # summary text
    cli_args = ([cov_cmd, 'report', '-instr-profile', profdat_path]
                + binaries_list
                + [f'-ignore-filename-regex={ignore_pattern}']
                + ['-use-color=0', '-show-region-summary=0', '-show-branch-summary=0',
                   '-show-functions=0']
                )
    run_sub(cli_args, working_dir)


if __name__ == '__main__':
    main()
